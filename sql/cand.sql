create table reqmcan 
(
 canid integer not null,
 reqid integer not null,
 canfecing date, 
 cannombre1 varchar(25) not null,
 cannombre2 varchar(25),
 cannombre3 varchar(25),
 canapellido1 varchar(25) not null,
 canapellido2 varchar(25),
 canapellidoc varchar(25),
 cantipiden char(5) 
 canidentif varchar(25),
 canestado char(1) not null,
 canobserva varchar(255),
 canmotrech char(5), 
 canmotrechfec date, 
 canmotrechob varchar(255), 
 canemail varchar(100),
 canfecnac date not null,
 cantelefono varchar(20),
 canreligion char(2),
 canestadocivil char(1),
 candireccion varchar(150),
 canfindesemana char(1),
 canturnorota char(1),
 cannivelacademico char(3),
 cancarrera varchar(60),
 
 canprocesant char(1),
 canfamilialab char(1),
 canfamiliapub char(1),
 
 cantrasporte char(3),
 trcid smallint,
 cansalud varchar(255),
 canfecprocita date,
 canfecprochora timestamp without time zone,
 cangenero char(1),
 CONSTRAINT reqmcan_pkey PRIMARY KEY (canid),
 CONSTRAINT reqmcan_reqid_fkey FOREIGN KEY (reqid)
    REFERENCES public.reqmreq (reqid) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
);
