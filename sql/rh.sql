--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: andevarlog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.andevarlog (
    andesesid character(25) NOT NULL,
    parnombre character varying(25),
    parvalor character varying(50),
    fecha date
);


ALTER TABLE public.andevarlog OWNER TO postgres;

--
-- Name: commemp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.commemp (
    id_commemp integer NOT NULL,
    nombre character varying(255) NOT NULL,
    nit character varying(15),
    telefono character varying(20),
    direccion character varying(100),
    imagen character varying(100),
    estado integer,
    dbname character varying(8),
    espropia boolean,
    cajasprestamosaldo smallint,
    codigo character varying(15),
    iniciales character varying(8)
);


ALTER TABLE public.commemp OWNER TO postgres;

--
-- Name: commemp_id_commemp_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.commemp_id_commemp_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.commemp_id_commemp_seq OWNER TO postgres;

--
-- Name: commemp_id_commemp_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.commemp_id_commemp_seq OWNED BY public.commemp.id_commemp;


--
-- Name: cust; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cust (
    cod integer,
    nom character varying(30)
);


ALTER TABLE public.cust OWNER TO postgres;

--
-- Name: empresa; Type: TABLE; Schema: public; Owner: sistemas
--

CREATE TABLE public.empresa (
    codigo character(10) NOT NULL,
    nombre character varying(200),
    pais integer
);


ALTER TABLE public.empresa OWNER TO sistemas;

--
-- Name: grupo; Type: TABLE; Schema: public; Owner: sistemas
--

CREATE TABLE public.grupo (
    grpid integer NOT NULL,
    grpnombre character varying(255)
);


ALTER TABLE public.grupo OWNER TO sistemas;

--
-- Name: grupo_grpid_seq; Type: SEQUENCE; Schema: public; Owner: sistemas
--

CREATE SEQUENCE public.grupo_grpid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grupo_grpid_seq OWNER TO sistemas;

--
-- Name: grupo_grpid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sistemas
--

ALTER SEQUENCE public.grupo_grpid_seq OWNED BY public.grupo.grpid;


--
-- Name: menu; Type: TABLE; Schema: public; Owner: sistemas
--

CREATE TABLE public.menu (
    menid integer NOT NULL,
    mennombre character varying(255),
    mencmd character(1000),
    mentipo smallint,
    menpadre integer
);


ALTER TABLE public.menu OWNER TO sistemas;

--
-- Name: menu_menid_seq; Type: SEQUENCE; Schema: public; Owner: sistemas
--

CREATE SEQUENCE public.menu_menid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menu_menid_seq OWNER TO sistemas;

--
-- Name: menu_menid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sistemas
--

ALTER SEQUENCE public.menu_menid_seq OWNED BY public.menu.menid;


--
-- Name: permiso; Type: TABLE; Schema: public; Owner: sistemas
--

CREATE TABLE public.permiso (
    pergrpid integer NOT NULL,
    permenid integer NOT NULL
);


ALTER TABLE public.permiso OWNER TO sistemas;

--
-- Name: usuario; Type: TABLE; Schema: public; Owner: sistemas
--

CREATE TABLE public.usuario (
    usuid integer NOT NULL,
    usulogin character varying(255),
    usupwd character varying(255),
    usunombre character varying(255),
    usugrpid integer,
    usuempresa character(10)
);


ALTER TABLE public.usuario OWNER TO sistemas;

--
-- Name: usuario_usuid_seq; Type: SEQUENCE; Schema: public; Owner: sistemas
--

CREATE SEQUENCE public.usuario_usuid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_usuid_seq OWNER TO sistemas;

--
-- Name: usuario_usuid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sistemas
--

ALTER SEQUENCE public.usuario_usuid_seq OWNED BY public.usuario.usuid;


--
-- Name: commemp id_commemp; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commemp ALTER COLUMN id_commemp SET DEFAULT nextval('public.commemp_id_commemp_seq'::regclass);


--
-- Name: grupo grpid; Type: DEFAULT; Schema: public; Owner: sistemas
--

ALTER TABLE ONLY public.grupo ALTER COLUMN grpid SET DEFAULT nextval('public.grupo_grpid_seq'::regclass);


--
-- Name: menu menid; Type: DEFAULT; Schema: public; Owner: sistemas
--

ALTER TABLE ONLY public.menu ALTER COLUMN menid SET DEFAULT nextval('public.menu_menid_seq'::regclass);


--
-- Name: usuario usuid; Type: DEFAULT; Schema: public; Owner: sistemas
--

ALTER TABLE ONLY public.usuario ALTER COLUMN usuid SET DEFAULT nextval('public.usuario_usuid_seq'::regclass);


--
-- Name: andevarlog andevarlog_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.andevarlog
    ADD CONSTRAINT andevarlog_pkey PRIMARY KEY (andesesid);


--
-- Name: commemp commemp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commemp
    ADD CONSTRAINT commemp_pkey PRIMARY KEY (id_commemp);


--
-- Name: empresa empresa_pkey; Type: CONSTRAINT; Schema: public; Owner: sistemas
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT empresa_pkey PRIMARY KEY (codigo);


--
-- Name: grupo grupo_pkey; Type: CONSTRAINT; Schema: public; Owner: sistemas
--

ALTER TABLE ONLY public.grupo
    ADD CONSTRAINT grupo_pkey PRIMARY KEY (grpid);


--
-- Name: menu menu_pkey; Type: CONSTRAINT; Schema: public; Owner: sistemas
--

ALTER TABLE ONLY public.menu
    ADD CONSTRAINT menu_pkey PRIMARY KEY (menid);


--
-- Name: permiso permiso_pkey; Type: CONSTRAINT; Schema: public; Owner: sistemas
--

ALTER TABLE ONLY public.permiso
    ADD CONSTRAINT permiso_pkey PRIMARY KEY (pergrpid, permenid);


--
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: sistemas
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (usuid);


--
-- Name: menu menu_menpadre_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sistemas
--

ALTER TABLE ONLY public.menu
    ADD CONSTRAINT menu_menpadre_fkey FOREIGN KEY (menpadre) REFERENCES public.menu(menid);


--
-- Name: permiso permiso_pergrpid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sistemas
--

ALTER TABLE ONLY public.permiso
    ADD CONSTRAINT permiso_pergrpid_fkey FOREIGN KEY (pergrpid) REFERENCES public.grupo(grpid);


--
-- Name: permiso permiso_permenid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sistemas
--

ALTER TABLE ONLY public.permiso
    ADD CONSTRAINT permiso_permenid_fkey FOREIGN KEY (permenid) REFERENCES public.menu(menid);


--
-- Name: usuario usuario_usuempresa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sistemas
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_usuempresa_fkey FOREIGN KEY (usuempresa) REFERENCES public.empresa(codigo) NOT VALID;


--
-- Name: usuario usuario_usugrpid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: sistemas
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_usugrpid_fkey FOREIGN KEY (usugrpid) REFERENCES public.grupo(grpid);


--
-- Name: TABLE andevarlog; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.andevarlog TO PUBLIC;


--
-- Name: TABLE commemp; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.commemp TO PUBLIC;


--
-- Name: TABLE cust; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.cust TO sistemas;
GRANT ALL ON TABLE public.cust TO carlos;
GRANT ALL ON TABLE public.cust TO PUBLIC;


--
-- Name: TABLE empresa; Type: ACL; Schema: public; Owner: sistemas
--

GRANT ALL ON TABLE public.empresa TO PUBLIC;


--
-- Name: TABLE grupo; Type: ACL; Schema: public; Owner: sistemas
--

GRANT ALL ON TABLE public.grupo TO PUBLIC;


--
-- Name: TABLE menu; Type: ACL; Schema: public; Owner: sistemas
--

GRANT ALL ON TABLE public.menu TO PUBLIC;


--
-- Name: TABLE permiso; Type: ACL; Schema: public; Owner: sistemas
--

GRANT ALL ON TABLE public.permiso TO PUBLIC;


--
-- Name: TABLE usuario; Type: ACL; Schema: public; Owner: sistemas
--

GRANT ALL ON TABLE public.usuario TO PUBLIC;


--
-- PostgreSQL database dump complete
--

