create view vmmot as
select r.reqid, r.reqmotivo, m.motnombre,
  case
  	when (reqmotivo = 1) or (reqmotivo = 2) then 1
	else 0 
  end as vacante,
  case
  	when (reqmotivo = 3) or (reqmotivo = 4) then 1
	else 0 
  end as nueva,
  case
  	when (reqmotivo = 2) or (reqmotivo = 4) then 1
	else 0 
  end as temporal,
  case
  	when (reqmotivo = 5) then 1
	else 0 
  end as pasante

from reqmreq r, glbmmot m
where r.reqmotivo = m.motid;


select * from vmmot
