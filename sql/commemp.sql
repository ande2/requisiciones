






{ TABLE "informix".commemp row size = 541 number of columns = 12 index size = 9 }

create table "informix".commemp 
  (
    id_commemp serial not null ,
    nombre varchar(255) not null ,
    nit varchar(15),
    telefono varchar(20),
    direccion varchar(100),
    imagen varchar(100),
    estado integer,
    dbname varchar(8),
    espropia "informix".boolean,
    cajasprestamosaldo smallint,
    codigo varchar(15),
    iniciales varchar(8),
    primary key (id_commemp)  constraint "informix".pk_empresa
  );

revoke all on "informix".commemp from "public" as "informix";




