






{ TABLE "informix".commempl row size = 987 number of columns = 15 index size = 0 }

create table "informix".commempl 
  (
    id_commempl serial not null ,
    nombre varchar(255),
    apellido varchar(255),
    telefono varchar(20),
    direccion varchar(100),
    codigo varchar(20),
    jefe integer,
    comision smallint,
    estado smallint,
    id_commpue integer,
    id_commdep integer,
    numcuenta varchar(25),
    usulogin varchar(25),
    usupwd varchar(255),
    usugrpid integer
  );

revoke all on "informix".commempl from "public" as "informix";




