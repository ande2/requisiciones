GLOBALS "menuGlobals.4gl"

#########################################################################
## Function  : MAIN
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Funcion principal del programa
##             202410142002 Cambio 1 JA
#########################################################################
MAIN
   DEFINE aui  om.DomNode
   DEFINE sm   om.DomNode
   DEFINE ok   SMALLINT
   DEFINE tit  STRING
   DEFINE runcmd STRING 
   
   OPTIONS
      INPUT NO WRAP,
      ON CLOSE APPLICATION STOP
   WHENEVER ERROR CONTINUE

  -- CALL ui.interface.loadStyles("styles_sc")

   CLOSE WINDOW SCREEN 
   OPEN WINDOW menuBlank AT 1,1 WITH FORM "menuBlank" ATTRIBUTES (TEXT = tit)
   
   
   CALL ui.Interface.loadActionDefaults("actiondefaultsLogin")
   CALL ui.Interface.loadStyles("style")
   CALL ui.interface.loadToolBar("opt1") 
   
   LET ok = login()
   IF (ok) THEN
      IF esUsuario = 0 THEN 
         LET tit = "Bienvenido al Sistema :: ", gUsuario.nombre CLIPPED, ' ', gUsuario.apellido CLIPPED, " (", gUsuario.usuLogin CLIPPED, ")"
         CALL ui.Window.getCurrent().setText("ANDE - Menú General - "||nomUsuario(gUsuario.id_commempl))
         CALL ui.Interface.setText(tit)
         LET aui = ui.Interface.getRootNode()
         LET sm = aui.createChild("StartMenu")
         CALL sm.setAttribute("text","Menu Principal")
         CALL men(cRaiz, sm)
         MENU ""
            ON IDLE 30
               DISPLAY "Activo"
            ON ACTION CLOSE 
            --call confirm_Save(dialog)
               EXIT MENU 
            --COMMAND "Salir"
            --   EXIT MENU
            {COMMAND KEY(INTERRUPT)
               EXIT MENU}
         END MENU
      ELSE 
         IF esUsuario = 1 THEN --USUARIOS
            LET runcmd = "fglrun Flujo.42r ", ' ', gUsuario.id_commempl CLIPPED
            DISPLAY "RUNCMD ", runcmd
            RUN runcmd
         ELSE --CLINICA MEDICA / esUsuario=2
            LET runcmd = "fglrun Clinica.42r ", ' ', gUsuario.id_commempl CLIPPED
            DISPLAY "RUNCMD ", runcmd
            RUN runcmd
         END IF    
      END IF 
      CLOSE WINDOW menuBlank
   END IF
END MAIN

#########################################################################
## Function  : men()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Arma la estructura de menu
#########################################################################
FUNCTION men(p,sm)
   DEFINE arr    DYNAMIC ARRAY OF
      RECORD
         menId    LIKE menu.menId,
         menNombre   LIKE menu.menNombre,
         menTipo  LIKE menu.menTipo,
         menCmd   LIKE menu.menCmd,
         node     om.DomNode
      END RECORD
   DEFINE p       LIKE menu.menId
   DEFINE i, n, cnt  SMALLINT
   DEFINE qry     STRING
   DEFINE nom     LIKE menu.menNombre
   DEFINE sm      om.DomNode
   DEFINE smg     om.DomNode
   DEFINE smc     om.DomNode
   DEFINE runCmd  STRING 
   
   LET qry = "SELECT a.menId, a.menNombre, a.menTipo, a.menCmd FROM menu a, permiso b WHERE a.menId <> ", cRaiz, 
                " AND a.menPadre = ", p, " AND b.perMenId = a.menId AND b.perGrpId = ", gUsuario.usuGrpId,
                " order by a.menId "
   PREPARE prpM1 FROM qry
   DECLARE curM1 CURSOR FOR prpM1
   LET i = 1
   FOREACH curM1 INTO arr[i].menId, arr[i].menNombre, arr[i].menTipo, arr[i].menCmd
      LET i = i + 1
   END FOREACH
   LET i = i - 1
   IF i > 0 THEN
      FOR n = 1 TO i
         IF arr[n].menTipo = 0 THEN    -- Crea un submenu
            LET arr[n].node = createStartMenuGroup(sm,arr[n].menNombre)
         ELSE  
         -- Crea un comando
            LET runCmd = arr[n].menCmd CLIPPED, ' ', gUsuario.id_commempl CLIPPED
            LET arr[n].node = createStartMenuCommand(sm,arr[n].menNombre CLIPPED, runCmd CLIPPED, "circle.png")
         END IF
         CALL men(arr[n].menId,arr[n].node)
      END FOR
   END IF
END FUNCTION

#########################################################################
## Function  : login()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Firma del sistema
#########################################################################
FUNCTION login()
   DEFINE ok,c,s  SMALLINT
   DEFINE rec RECORD
      usuEmpresa  LIKE usuario.usuEmpresa,
      usuLogin    LIKE usuario.usuLogin,
      usuPwd      LIKE usuario.usuPwd
      END RECORD
   DEFINE qry     STRING
   LET ok = FALSE
   LET s = TRUE
   OPEN WINDOW menLogin AT 1,1 WITH FORM "menLogin" ATTRIBUTES (TEXT = "Inicio de sesion...")
   CALL lib_cleanTinyScreen()
   CALL combo_init()
   CALL ui.Interface.setText("Inicio de sesion...")
   WHILE s
      LET int_flag = FALSE
      --INPUT BY NAME rec.*
      INPUT rec.usuEmpresa, rec.usuLogin, rec.usuPwd
         FROM fusuempresa, usuLogin, usuPwd
         AFTER INPUT
            IF (NOT int_flag) THEN
               LET c = 0
               LET qry = "SELECT COUNT(*) FROM commempl WHERE usuLogin = ? AND usuPwd = ?"
               PREPARE prpL1 FROM qry
               EXECUTE prpL1 USING rec.usuLogin, rec.usuPwd INTO c
               IF STATUS = 0 THEN
                  IF c = 1 THEN
                     LET qry = "SELECT * FROM commempl WHERE usuLogin = ? AND usuPwd = ?"
                      
                     PREPARE prpL2 FROM qry
                     EXECUTE prpL2 USING rec.usuLogin, rec.usuPwd INTO gUsuario.*
                     IF STATUS = 0 THEN
                        LET s = FALSE
                        LET ok = TRUE
                     END IF
                  ELSE
                     DISPLAY "Var C/Usuario/Password ", c, ' - ', qry, ' - ', rec.usuLogin, ' - ', rec.usuPwd
                     CALL msg("Usuario/Password incorrecto")
                     NEXT FIELD usuLogin
                  END IF
               END IF
            ELSE
               LET int_flag = TRUE
               LET s = FALSE
            END IF
      END INPUT
   END WHILE
   CLOSE WINDOW menLogin
   IF gUsuario.usugrpid = 6 THEN --USUARIOS
      LET esUsuario = 1
   ELSE 
      IF gUsuario.usugrpid = 10 THEN --CLINICA MEDICA
         LET esUsuario = 2
      ELSE 
         LET esUsuario = 0
      END IF    
   END IF 
   RETURN ok
END FUNCTION

#########################################################################
## Function  : nm()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Obtiene el nombre del menu
#########################################################################
FUNCTION nm(id)
   DEFINE id   LIKE menu.menId
   DEFINE nomb LIKE menu.menNombre
   DEFINE qry  STRING
   LET qry = "SELECT menNombre FROM menu WHERE menId = ?"
   PREPARE prpD1 FROM qry
   EXECUTE prpD1 USING id INTO nomb
   RETURN nomb
END FUNCTION

FUNCTION combo_init()
   CALL combo_din2("fusuempresa", "SELECT codigo, nombre FROM empresa ")
END FUNCTION