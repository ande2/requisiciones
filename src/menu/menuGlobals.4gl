DATABASE rh

#########################################################################
## Function  : GLOBALS
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Declaracion de funciones globales
#########################################################################

GLOBALS 
DEFINE gUsuario   RECORD LIKE commempl.*
  { nombre   LIKE empresa.nombre,
   usuId    LIKE usuario.usuid,
   usuLogin LIKE usuario.usulogin,
   usuPwd   LIKE usuario.usupwd,
   usuNombre   LIKE usuario.usunombre,
   usuGrpId LIKE usuario.usugrpid
END RECORD }
CONSTANT cRaiz = -1
CONSTANT cErr = "Error. La operaci�n no fue procesada."
CONSTANT cAddOK = "Registro agregado exitosamente"
CONSTANT cDelOK = "Registro eliminado"
CONSTANT cUpdOK = "Registro actualizado"
CONSTANT cPerOK = "Permisos actualizados"
DEFINE   esUsuario SMALLINT 
END GLOBALS 