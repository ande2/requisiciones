GLOBALS "reqr0323_glob.4gl"

FUNCTION run_report_to_handler(HANDLER)
DEFINE   pDet   rep_det,
         pfecha, smes, stit, pfechaing, pgerente STRING,
         nmes  SMALLINT 

DEFINE HANDLER om.SaxDocumentHandler

   LET nmes = MONTH(TODAY) 
   CASE 
      WHEN nmes = 1 LET smes = 'enero'
      WHEN nmes = 2 LET smes = 'febrero'
      WHEN nmes = 3 LET smes = 'marzo'
      WHEN nmes = 4 LET smes = 'abril'
      WHEN nmes = 5 LET smes = 'mayo'
      WHEN nmes = 6 LET smes = 'junio'
      WHEN nmes = 7 LET smes = 'julio'
      WHEN nmes = 8 LET smes = 'agosto'
      WHEN nmes = 9 LET smes = 'septiembre'
      WHEN nmes = 10 LET smes = 'octubre'
      WHEN nmes = 11 LET smes = 'noviembre'
      WHEN nmes = 12 LET smes = 'diciembre'
      OTHERWISE LET smes = ''
   END CASE 
   LET pfecha = 'Guatemala, '||DAY(TODAY)||' de '||smes CLIPPED || ' de '||YEAR(TODAY) 
   
   TRY
       START REPORT report1_report TO XML HANDLER HANDLER
  
         --Cargar los Datos de la requisicion
         SELECT e.nombre, e.direccion, e.telefono,
            CASE 
               WHEN (c.cantrato = 'S' ) THEN 'Señor'
               WHEN (c.cantrato = 'A' ) THEN 'Señora'
               WHEN (c.cantrato = 'I' ) THEN 'Señorita'
               ELSE ''
            END AS trato,
            coalesce(trim(initcap(c.canNombre1)),'')|| ' '|| coalesce(trim(initcap(c.canNombre2)), '') || ' ' || coalesce(trim(initcap(c.canApellido1)), '') || ' ' || coalesce(trim(initcap(c.canApellido2)), ''),
            coalesce(trim(initcap(c.canApellido1)), '') || ' ' || coalesce(trim(initcap(c.canApellido2)), ''),
            r.fechaing, p.puenombre, uornombre, r.jefenom, l.locnombre, r.reqsalario, 0.0
         INTO pDet.*   
         FROM reqmcan c, reqmreq r, commemp e, glbmpue p, glbuniorga u, glbmloc l
         WHERE c.reqid 			   = r.reqid
            AND r.reqempcon 	   = e.id_commemp	
            AND r.pueposicion 	= p.pueid
            AND r.pueuniorg		= u.uorid
            AND r.pueloc		   = l.locid
            AND canid 			   = lCanId

         LET stit = pDet.cantrato CLIPPED, ' ', pDet.canApe

         IF pDet.fecIng IS NOT NULL THEN
            LET nmes = MONTH(pDet.fecIng) 
            CASE 
               WHEN nmes = 1 LET smes = 'enero'
               WHEN nmes = 2 LET smes = 'febreo'
               WHEN nmes = 3 LET smes = 'marzo'
               WHEN nmes = 4 LET smes = 'abril'
               WHEN nmes = 5 LET smes = 'mayo'
               WHEN nmes = 6 LET smes = 'junio'
               WHEN nmes = 7 LET smes = 'julio'
               WHEN nmes = 8 LET smes = 'agosto'
               WHEN nmes = 9 LET smes = 'septiembre'
               WHEN nmes = 10 LET smes = 'octubre'
               WHEN nmes = 11 LET smes = 'noviembre'
               WHEN nmes = 12 LET smes = 'diciembre'
               OTHERWISE LET smes = ''
            END CASE
            LET pfechaing = 'Guatemala, '||DAY(pDet.fecIng)||' de '||smes CLIPPED || ' de '||YEAR(pDet.fecIng) 
         ELSE 
            LET pfechaing = 'Pendiente'
         END IF 

         SELECT valchr INTO pDet.salBoni FROM glb_paramtrs WHERE numpar = 3 AND tippar = 1 
         LET pDet.salTot = pdet.salOrd + pdet.salBoni

         SELECT valchr INTO pgerente FROM glb_paramtrs WHERE numpar = 3 AND tippar = 2 

         OUTPUT TO REPORT report1_report ( pDet.*, pfecha, stit, pfechaing, pgerente)
       
       FINISH REPORT report1_report
   END TRY
END FUNCTION

REPORT report1_report( rDet, rfecha, rtit, rfechaing, rgerente)
DEFINE  rDet      rep_det,
   rfecha, rtit, rfechaing, rgerente   STRING 

   FORMAT
      ON EVERY ROW
         
         PRINTX rDet.*, rfecha, rtit, rfechaing, rgerente
         
END REPORT
