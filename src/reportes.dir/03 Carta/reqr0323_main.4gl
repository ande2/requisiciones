GLOBALS "reqr0323_glob.4gl"  

MAIN
      
    LET lCanId = arg_val(1)
    --LET lCanId = 34
    DISPLAY "ID ", lCanId
    CALL print_report (report_name)
    
END MAIN 

FUNCTION print_report (lReportName STRING)
DEFINE pSalida     STRING
DEFINE HANDLER     om.SaxDocumentHandler

  -- LET pSalida = "PDF"
   LET pSalida = "RTF"
   
   TRY
      DISPLAY "antes del load"
      IF fgl_report_loadCurrentSettings(lReportName||".4rp") THEN
         DISPLAY "antes del selectde"
         CALL fgl_report_selectDevice(pSalida)
         DISPLAY  "antes del if"
         IF pSalida = "XLSX" THEN
            CALL fgl_report_configureXLSXDevice (
               NULL,  #fromPage INTEGER,
               NULL,  #toPage INTEGER,
               NULL,  #removeWhitespace INTEGER,
               NULL,  #ignoreRowAlignment INTEGER,
               NULL,  #ignoreColumnAlignment INTEGER,
               NULL,  #removeBackgroundImages INTEGER,
               TRUE ) #mergePages INTEGER
         END IF 
         DISPLAY "antes del prev"
         CALL fgl_report_selectPreview(TRUE)
         DISPLAY "antes del comm"
         LET handler = fgl_report_commitCurrentSettings()
         DISPLAY "despues del com"
      END IF
      DISPLAY "antes del 2 if"
      IF handler IS NOT NULL THEN
         CALL run_report_to_handler(HANDLER)
         DISPLAY "despues del had"
      END IF
   CATCH 
      ERROR "No se encuentra el formato "||lReportName CLIPPED ||".4rp"
   END TRY 

END FUNCTION 