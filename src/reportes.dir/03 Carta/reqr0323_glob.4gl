DATABASE rh

GLOBALS
TYPE 
   rep_det  RECORD 
      empNom         LIKE commemp.nombre,
      empDir         LIKE commemp.direccion,
      empTel         LIKE commemp.telefono,

      cantrato       STRING,
      canNom         STRING,
      canApe         STRING,

      fecIng         LIKE reqmreq.fechaing,
      puesto         LIKE glbmpue.puenombre,
      unidad         LIKE glbuniorga.uornombre,
      jefe           LIKE reqmreq.jefenom,
      locacion       LIKE glbmloc.locnombre,
      salOrd         LIKE reqmreq.reqsalario,
      salBoni        DECIMAL(7,2),
      salTot         DECIMAL(14,2)
   END RECORD 
   DEFINE      lCanId      LIKE reqmreq.reqid 
   CONSTANT    prog_name = "reqr0323"
   CONSTANT    report_name = "reqr0323_rep01"
END GLOBALS 