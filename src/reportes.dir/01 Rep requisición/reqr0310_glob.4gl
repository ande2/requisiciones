################################################################################
# Funcion     : %M%
# Descripcion : Catalogo de Empresas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
DATABASE rh

GLOBALS
TYPE 
   rep_det  RECORD 
        reqFecRec          DATE
      , reqDia             SMALLINT
      , reqMes             SMALLINT
      , reqAnio            SMALLINT
      , reqPlazaVacante    SMALLINT
      , reqPlazaNueva      SMALLINT 
      , reqPlazaTemporal   SMALLINT
      , reqPlazaPasante    SMALLINT
      , reqEmpresa         LIKE commemp.iniciales
      , reqMotivo          LIKE glbMMot.motnombre
      , reqVacante         LIKE reqMReq.reqvacant
      , reqTipo            LIKE reqMReq.reqtipo
      , reqAfuNombre       LIKE glbmareaf.afunombre
      , reqUorNombre       LIKE glbuniorga.uornombre
      , reqPueNombre       LIKE glbmpue.puenombre
      , reqCcoNombre       LIKE glbCco.cconombre
      , reqLocNombre       LIKE glbMLoc.locnombre
      , reqPueNecVeh       LIKE reqMReq.puenecveh
      , reqPueHorario      LIKE reqMreq.puehorario
      , reqJefeNom         LIKE reqMreq.jefenom
      , reqJefePueNom      LIKE glbmpue.puenombre
      , reqususol          STRING
      , reqSolPueNom       STRING 
      , requsuaut          STRING
      , reqAutPueNom       STRING
      , gerFunAut          STRING 
      , gerFunPueNom       STRING    
      , reqsalario         LIKE reqmreq.reqsalario
      , reqcondcomp        LIKE reqmreq.reqcondcomp
      , reqhrsext          LIKE reqmreq.reqhrsext
      , reqObs             LIKE reqmReq.reqobs
      , reqfechoringreso   LIKE reqmreq.fechoringreso
      , codPosicion        LIKE reqmreq.codposicion
   END RECORD 
   DEFINE      pReqID      LIKE reqmreq.reqid 
   CONSTANT    prog_name = "reqr0310"
   CONSTANT    report_name = "reqr0310_rep01"
END GLOBALS 