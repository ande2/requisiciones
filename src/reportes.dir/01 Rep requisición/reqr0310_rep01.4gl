################################################################################
# Funcion     : %M%
# Descripcion : Modulo para reporte
# Funciones   
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
IMPORT util 

GLOBALS "reqr0310_glob.4gl"
 

FUNCTION run_report_to_handler(HANDLER)
DEFINE  pDet         rep_det
DEFINE lgerfunaut    INT 


DEFINE HANDLER       om.SaxDocumentHandler
 
   TRY
       START REPORT report1_report TO XML HANDLER HANDLER
  
         --Cargar los Datos de la requisicion
         SELECT    a.reqFecRec
                 , 0,0,0, o.vacante, o.nueva, o.temporal, o.pasante
                 , b.iniciales
                 , c.motNombre, a.reqvacant
                 , a.reqTipo
                 , e.afunombre, d.uornombre, f.puenombre, g.cconombre, l.locnombre
                 , a.puenecveh, a.puehorario
                 , a.jefenom
                 , j.puenombre as puenombrejefe
                 , trim(m.nombre) || ' ' || trim(m.apellido) as ususol
                 , trim(n.nombre) || ' ' || trim(n.apellido) as usuaut
                 , a.reqsalario, a.reqcondcomp, a.reqhrsext, a.reqobs
                 , a.fechoringreso, a.codposicion
          INTO     pDet.reqFecRec, 
                   pDet.reqDia, pDet.reqMes, pDet.reqAnio, 
                   pDet.reqPlazaVacante, pDet.reqPlazaNueva, pDet.reqPlazaTemporal, pDet.reqPlazaPasante,   
                   pDet.reqEmpresa, 
                   pDet.reqMotivo, pDet.reqVacante, 
                   pDet.reqTipo,           
                   pDet.reqAfuNombre, pDet.reqUorNombre, pDet.reqPueNombre,      
                   pDet.reqCcoNombre, pDet.reqLocNombre, 
                   pDet.reqPueNecVeh, pDet.reqPueHorario,     
                   pDet.reqJefeNom, 
                   pDet.reqJefePueNom, 
                   pDet.reqususol, --pDet.reqSolPueNom,      
                   pDet.requsuaut, 
                   pDet.reqsalario, pDet.reqcondcomp, pDet.reqhrsext, pDet.reqObs, 
                   pDet.reqfechoringreso, pDet.codPosicion       
          FROM     reqMReq a, commemp b, glbMMot c
                , glbuniorga d, glbmareaf e, glbmpue f
                , glbcco g
                , glbuniorga h, glbmareaf i, glbmpue j
                , glbmloc l
                , commempl m, commempl n, vmmot o 
          WHERE    a.reqId = pReqID
          AND      b.id_commemp = a.reqEmpCon
          AND      c.motId = a.reqmotivo
          AND      d.uorid = a.pueUniOrg
          AND      e.afuid = d.uorareaf
          AND      f.pueid = a.pueposicion
          AND      g.ccoid = pueCco 
          AND      h.uorid = a.jefeUniOrg
          AND      i.afuid = h.uorareaf
          AND      j.pueid = a.jefePue
          AND      l.locid = a.pueloc
          AND      m.id_commempl = a.reqususol
          AND      n.id_commempl = a.requsuaut
          AND      o.reqid = a.reqid 
          AND      o.reqmotivo = a.reqmotivo

         SELECT puenombre INTO pDet.reqSolPueNom FROM glbmpue WHERE pueid = 
            (SELECT pueid FROM commempl WHERE id_commempl = 
               (SELECT reqususol FROM public.reqmreq WHERE reqid = pReqID)
            ) 

         SELECT puenombre INTO pDet.reqAutPueNom FROM glbmpue WHERE pueid = 
            (SELECT pueid FROM commempl WHERE id_commempl = 
               (SELECT requsuaut FROM public.reqmreq WHERE reqid = pReqID)
            ) 

         LET lgerfunaut = NULL 
         SELECT gerFunAut INTO lgerfunaut FROM reqmreq WHERE reqid = pReqID
         IF lgerfunaut IS NOT NULL THEN 
            SELECT trim(nombre)||' '||trim(apellido) INTO pDet.gerFunAut FROM commempl, reqmreq WHERE id_commempl = gerfunaut AND reqid = pReqID
               SELECT puenombre INTO pDet.gerFunPueNom FROM glbmpue WHERE pueid = 
                  (SELECT pueid FROM commempl WHERE id_commempl = 
                     (SELECT gerfunaut FROM public.reqmreq where reqid = pReqID)
                  )  
         ELSE 
            LET pDet.gerFunAut = NULL 
         END IF 

         OUTPUT TO REPORT report1_report ( pDet.* )
       
       FINISH REPORT report1_report
   END TRY
END FUNCTION

REPORT report1_report( rDet)
DEFINE  rDet      rep_det

   FORMAT
      ON EVERY ROW
         LET rDet.reqDia = DAY(rDet.reqFecRec)
         
         LET rDet.reqMes = MONTH (rDet.reqFecRec)
         DISPLAY 'rDet.reqMes ', rDet.reqMes
         LET rDet.reqAnio = YEAR(rDet.reqFecRec)
         PRINTX rDet.*
END REPORT
