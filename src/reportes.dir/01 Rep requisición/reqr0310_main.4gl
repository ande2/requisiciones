################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal para umdores 
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
IMPORT util 

GLOBALS "reqr0310_glob.4gl"
   
DATABASE rh 

FUNCTION main(  )
      CONNECT TO "rh"
      
      LET pReqID  = ARG_VAL(1)
      DISPLAY "ID ", pReqID
      CALL print_Report ( report_name)
END FUNCTION 

FUNCTION print_report (pReportName)
DEFINE pReportName STRING
DEFINE pSalida     STRING
DEFINE HANDLER     om.SaxDocumentHandler

   LET pSalida = "PDF"
   
   TRY
   DISPLAY "antes del load"
            IF fgl_report_loadCurrentSettings(pReportName||".4rp") THEN
            DISPLAY "antes del selectde"
            CALL fgl_report_selectDevice(pSalida)
            DISPLAY  "antes del if"
            IF pSalida = "XLSX" THEN
               CALL fgl_report_configureXLSXDevice (
                                                      NULL,  #fromPage INTEGER,
                                                      NULL,  #toPage INTEGER,
                                                      NULL,  #removeWhitespace INTEGER,
                                                      NULL,  #ignoreRowAlignment INTEGER,
                                                      NULL,  #ignoreColumnAlignment INTEGER,
                                                      NULL,  #removeBackgroundImages INTEGER,
                                                      TRUE ) #mergePages INTEGER
            END IF 
            DISPLAY "antes del prev"
            CALL fgl_report_selectPreview(TRUE)
            DISPLAY "antes del comm"
            LET handler = fgl_report_commitCurrentSettings()
            DISPLAY "despues del com"
         END IF
         DISPLAY "antes del 2 if"
         IF handler IS NOT NULL THEN
            CALL run_report_to_handler(HANDLER)
            DISPLAY "despues del had"
         END IF
      CATCH 
         ERROR "No se encuentra el formato "||pReportName CLIPPED ||".4rp"
      END TRY 

END FUNCTION 

