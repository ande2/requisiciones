
GLOBALS "reqr0320_glob.4gl"

MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING

	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	LET n_param = num_args()
    IF n_param > 0 THEN
        LET requi = arg_val(1)
        LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
        
        CALL STARTLOG(prog_name2)

        CALL main_init()
    END IF
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form

    CALL ui.Interface.loadActionDefaults("actiondefaults_rh1")

--    CLOSE WINDOW SCREEN 
--
--    LET nom_forma = "reqm0318_form"
--	OPEN WINDOW w1 WITH FORM nom_forma
--    CALL fgl_settitle("ANDE - Reporte Requisiciones")
--
--	LET w = ui.WINDOW.getcurrent()
--	LET f = w.getForm()

    CALL main_menu()
END FUNCTION

FUNCTION main_menu()
DEFINE eta    RECORD LIKE reqmeta.*,
 bita   RECORD LIKE reqmbit.*,
 i, j   SMALLINT
 DEFINE suma      LIKE reqmeta.etatiempo 
 DEFINE myHandler om.SaxDocumentHandler 
 
    LET i = 0
    CALL reg_det.clear()
    
    DECLARE cur_etapas CURSOR FOR
        SELECT *
          FROM reqmeta
         WHERE etatipo > 0
        ORDER BY etatipo, etaorden
        
    FOREACH cur_etapas INTO eta.*
        INITIALIZE bita.* TO NULL
        
        SELECT * INTO bita.*
        from reqmbit
        where reqid = requi
        and bitid in (
            select max(bitid)
            from reqmbit b2
            where b2.reqid = requi
              and b2.etaidfin = eta.etaid
         )

         IF sqlca.sqlcode <> NOTFOUND THEN
            LET i = i + 1
            IF eta.etatiempo IS NULL THEN LET eta.etatiempo = ' 0 00' END IF 
            LET reg_det[i].etapa.* = eta.*
            LET reg_det[i].bitacora.* = bita.*

            IF i = 1 THEN
                LET reg_det[i].tiemporeal = ' 0 00'
                LET reg_det[i].tiempo     = ' 0 00'
                LET reg_det[i].diferencia = ' 0 00'
                LET reg_det[i].tiempoeta  = eta.etatiempo
            ELSE
                LET reg_det[i-1].fecinicio  = bita.bitfecoper
                LET reg_det[i].tiempoeta    = eta.etatiempo
                LET reg_det[i].tiemporeal = ' 0 00'
                LET reg_det[i].tiempo     = ' 0 00'
                LET reg_det[i].diferencia = ' 0 00'
            END IF
         END IF
    END FOREACH
    
    LET j = i
    LET suma = ' 0 00'

    IF i > 0 THEN

        FOR i = 1 TO j
            LET reg_det[i].tiemporeal = (reg_det[i].fecinicio - reg_det[i].bitacora.bitfecoper) + suma
            LET reg_det[i].tiempo     = reg_det[i].tiemporeal - findesemana(reg_det[i].bitacora.bitfecoper,reg_det[i].fecinicio)
            LET reg_det[i].diferencia = reg_det[i].tiempo - reg_det[i].etapa.etatiempo 
        END FOR
    
        --LET myHandler = gral_reporte("carta","vertial","PDF",132,"reqm0318")
        --START REPORT reqr0320_rep TO "reqm0318.txt"
        IF fgl_report_loadCurrentSettings("reqr0320_repor.4rp") THEN
           LET myHandler = fgl_report_commitCurrentSettings()
        ELSE
           EXIT PROGRAM
        END IF

        START REPORT reqr0320_rep TO XML HANDLER myHandler
        --om.XmlWriter.createFileWriter("reqr0320_repor.xml")

        FOR i = 1 TO j
            OUTPUT TO REPORT reqr0320_rep(reg_det[i].*) 
        END FOR

        FINISH REPORT reqr0320_rep
        CALL fgl_report_stopGraphicalCompatibilityMode()
    END IF
END FUNCTION

FUNCTION findesemana(tini, tfin)
DEFINE tini, tfin  LIKE reqmbit.bitfecoper
DEFINE fini, ffin  DATE
DEFINE dias        SMALLINT
DEFINE i           SMALLINT
DEFINE restar      LIKE reqmeta.etatiempo
DEFINE fecha       DATE

    LET restar = '0 00'
    
    LET fini = findesemana_fecha(tini)
    LET ffin = findesemana_fecha(tfin)

    LET dias = ffin - fini + 1
    LET fecha = fini - 1
    
    FOR i = 1 TO dias
        LET fecha = fecha + 1
        IF WEEKDAY(fecha) = 0 OR  WEEKDAY(fecha) = 6 THEN
            LET restar = restar + '1 00'
        ELSE
            LET restar = restar + findesemana_feriado(fecha)
        END IF
    END FOR
    
RETURN restar
END FUNCTION

FUNCTION findesemana_feriado(fecha)
DEFINE fecha   DATE
DEFINE feriado RECORD LIKE glbmfer.*
DEFINE tiempo  LIKE reqmeta.etatiempo

    LET tiempo = '0 00'
    SELECT * INTO feriado.*
      FROM glbmfer
     WHERE feriaid = fecha

    IF SQLCA.sqlcode <> NOTFOUND THEN
        CASE feriado.feriatiempo
            WHEN 1  LET tiempo = '1 00'
            WHEN 12 LET tiempo = '0 12'
        END CASE
    END IF
    
RETURN tiempo
END FUNCTION

FUNCTION findesemana_fecha(texto)
DEFINE texto   CHAR(25)
DEFINE xanio,
       xmes,
       xdia  CHAR(4)
DEFINE anio, mes, dia SMALLINT
DEFINE fecha  DATE

    LET xanio = texto[1,4]
    LET xmes  = texto[6,7]
    LET xdia  = texto[9,10]

    LET anio = xanio
    LET mes  = xmes
    LET dia  = xdia

    LET fecha = MDY(mes, dia, anio)

RETURN fecha
END FUNCTION
