IMPORT util
IMPORT FGL req_combos

GLOBALS "reqr0320_glob.4gl"

REPORT reqr0320_rep(r)
DEFINE r    tDG
DEFINE req  RECORD LIKE reqmreq.*
DEFINE fijo, total, difer LIKE reqmeta.etatiempo
DEFINE uor     RECORD LIKE glbuniorga.*
DEFINE afu     RECORD LIKE glbmareaf.*
DEFINE pue     RECORD LIKE glbmpue.*
DEFINE usu     RECORD LIKE commempl.*
DEFINE t1      LIKE reqmeta.etatiempo
DEFINE usolic  CHAR(30)
DEFINE bitfecoper CHAR(20)
DEFINE etatiempo CHAR(15)
DEFINE tiempo CHAR(15)
DEFINE diferencia CHAR(15)
DEFINE t_etatiempo CHAR(15)
DEFINE t_tiempo CHAR(15)
DEFINE t_diferencia CHAR(15)
DEFINE cmb_dat tComboTex
DEFINE i SMALLINT
DEFINE estado   CHAR(20)
FORMAT
    FIRST PAGE HEADER

        LET t1 = ' 0 00'
        
        SELECT * INTO req.*
          FROM reqmreq
         WHERE reqid = requi

        SELECT * INTO usu.*
          FROM comMEmpl
         WHERE id_commempl = req.reqususol

        LET usolic = usu.nombre CLIPPED, ' ', usu.apellido CLIPPED

        CALL cmb_reqestado_init() RETURNING cmb_dat.*
            
        FOR i = 1 TO cmb_dat.cantidad
            IF req.reqestado = cmb_dat.datos[i].id THEN 
               LET estado = cmb_dat.datos[i].nombre
            END IF
        END FOR

         SELECT *
           INTO uor.*
           FROM glbuniorga
          WHERE uorid = req.pueuniorg

         SELECT * 
           INTO afu.*
           FROM glbmareaf
          WHERE afuid = uor.uorareaf

         SELECT *
           INTO pue.*
           FROM glbmpue
          WHERE pueid = req.pueposicion

        PRINT COLUMN 01, 'No Requisicion: ', req.reqid USING "########&",
              COLUMN 55, 'Solicita: ', usolic
        PRINT COLUMN 01, 'Fecha Crea: ', req.reqfecrec USING "DD/MM/YYYY",
              COLUMN 55, 'Unidad Organizacional: ', uor.uornombre
        PRINT COLUMN 01, 'Area Funcional: ', afu.afunombre,
              COLUMN 55, 'Nombre Puesto:', pue.puenombre
        PRINT COLUMN 01, 'Estado Actual: ', req.reqestado, estado,
              COLUMN 55, 'Nombre Jefe: ', req.jefenom

        SKIP 2 LINE

        PRINT COLUMN 01, 'Etapa de la requisición',
              COLUMN 30, 'Fecha y Hora Proceso',
              COLUMN 60, 'Asignado',
              COLUMN 70, 'Utilizado',
              COLUMN 80, 'Diferencia'

         SKIP 1 LINE 
              
    ON EVERY ROW

        LET bitfecoper = NULL
        LET etatiempo = NULL
        LET tiempo = NULL
        LET diferencia = NULL

        LET bitfecoper = util.Datetime.format(r.bitacora.bitfecoper, "%d/%m/%Y %H:%M")
        LET etatiempo  = util.Interval.format(r.etapa.etatiempo,"%d días")
        LET tiempo     = util.Interval.format(r.tiempo,"%d días")
        LET diferencia = util.Interval.format(r.diferencia,"%d días")

        --IF diferencia > t1 THEN
        --    LET diferencia = diferencia CLIPPED, '(+)'
        --ELSE
        --    LET diferencia = diferencia CLIPPED, '(-)'
        --END IF

        PRINT COLUMN 1, r.etapa.etanombre,
              COLUMN 30, bitfecoper,
              COLUMN 60, etatiempo,  
              COLUMN 70, tiempo,  
              COLUMN 80, diferencia  

    ON LAST ROW
        LET fijo  = SUM(r.etapa.etatiempo)
        LET total = SUM(r.tiempo)
        LET difer = SUM(r.diferencia)

        LET t_etatiempo  = util.Interval.format(fijo,"%d días")
        LET t_tiempo     = util.Interval.format(total,"%d días")
        LET t_diferencia = util.Interval.format(difer,"%d días")
        
        SKIP 1 LINE

        PRINT COLUMN 30, 'TOTAL:',
              COLUMN 60, t_etatiempo,
              COLUMN 70, t_tiempo,
              COLUMN 80, t_diferencia
    
END REPORT
