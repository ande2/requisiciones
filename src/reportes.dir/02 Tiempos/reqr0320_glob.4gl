DATABASE rh

GLOBALS
TYPE tDG RECORD
    etapa      RECORD LIKE reqmeta.*,
    bitacora   RECORD LIKE reqmbit.*,
    tiempoeta  LIKE reqmeta.etatiempo,
    tiempo     LIKE reqmeta.etatiempo,
    fecinicio  LIKE reqmbit.bitfecoper,
    diferencia LIKE reqmeta.etatiempo,
    tiemporeal LIKE reqmeta.etatiempo
END RECORD

    DEFINE dbname STRING
    CONSTANT    prog_name = "reqr0320"

    DEFINE reg_det DYNAMIC ARRAY OF tDG
    DEFINE u_reg, g_reg             tDG
    DEFINE condicion STRING

    DEFINE requi LIKE reqmreq.reqid
    DEFINE etapa LIKE reqmeta.etaid

DEFINE
    w           ui.Window,
    f           ui.Form
END GLOBALS