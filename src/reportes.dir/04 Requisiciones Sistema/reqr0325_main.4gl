################################################################################
# Usa la plantilla "Requisiciones_SISTEMA_t.xlsx"
# Al final hace un "Guardar como " usando  CALL fgl_excel.workbook_writeToFile(workbook, filename)
# Y lo guarda como "Requisiciones_SISTEMA_fecha.xlsx" 
# Carlos Santizo - Julio/2024
#
################################################################################

IMPORT FGL fgl_excel

DATABASE rh

{DEFINE luser   LIKE users.user_id
DEFINE lgrupo  LIKE grupo.grpnombre
DEFINE lsede   LIKE city.city_num}
 
MAIN
DEFINE g_reg RECORD --LIKE city.*
   reqempcon   LIKE reqmreq.reqempcon,
   pueareaf    LIKE reqmreq.pueareaf,
   reqfecreq   LIKE reqmreq.reqfecrec,
   reqtipo     LIKE reqmreq.reqtipo,
   pueuniorg   LIKE reqmreq.pueuniorg,
   pueposicion LIKE reqmreq.pueposicion,
   pueloc      LIKE reqmreq.pueloc,
   reqususol   LIKE reqmreq.reqususol,
   requsuaut   LIKE reqmreq.requsuaut
END RECORD 
DEFINE sql STRING
DEFINE filename STRING
DEFINE header BOOLEAN
DEFINE preview BOOLEAN
DEFINE result BOOLEAN
DEFINE fecha DATE 
DEFINE fechahora STRING 

DEFINE sql_stmt   STRING
DEFINE where_clause  STRING   

    DEFER INTERRUPT
    DEFER QUIT
    OPTIONS INPUT WRAP
    OPTIONS FIELD ORDER FORM

    -- Create and populate test database
    --CONNECT TO ":memory:+driver='dbmsqt'"
    --CONNECT TO "aprojusa"
    --CALL populate()
    CALL fgl_setenv("DBDATE","dmy2-")
    --OPEN WINDOW w WITH FORM "apror0704_form"
    OPEN WINDOW w WITH FORM "reqr0325_form"

    CALL fgl_settitle("Listado de Requisiciones")

    CALL combo_init()
    
    --LET luser  = arg_val(1)
    --LET lgrupo = arg_val(2)
    --SELECT city_num INTO lsede FROM users WHERE user_id = luser
    --DISPLAY "usuario lleva ", luser

    --LET where_clause = " city_num <> 1000 "
    {IF lgrupo = "SEDES" OR lgrupo = "OFICINA" OR lgrupo = "SUPERVISORES" THEN  
       LET where_clause = where_clause CLIPPED, " AND city_num = ", lsede
            --" AND contact_user = '", usuario, "'"
    END IF}  
    LET sql_stmt     = "SELECT * FROM reqmreq WHERE ", where_clause CLIPPED 
    DISPLAY "sql_stmt ", sql_stmt
    
    --CALL combo_din2("city_num", sql_stmt)

    
    -- Default values
    LET sql = "SELECT * FROM contnote"

     -- Creando nombre archivo de errores a copiar a pc cliente 
    LET fechahora      = "_"||TODAY||"_"||CURRENT HOUR TO MINUTE||"." 
    LET fechahora      = librut001_replace(fechahora,":","",40)  
    LET fechahora      = librut001_replace(fechahora,"/","",40)  
    LET fechahora      = librut001_replace(fechahora,"-","",40)  
    
    --LET filename = "fgl_excel_generic_test.xlsx"
    --LET filename = "REPORTE-DIARIO",fechahora,".xls"
    LET filename = "Requisiciones_SISTEMA.xlsx"
    
    --LET header = TRUE
    LET preview = TRUE
    
    --INPUT BY NAME sql, filename, header, preview ATTRIBUTES(UNBUFFERED, WITHOUT DEFAULTS=TRUE, ACCEPT=FALSE, CANCEL=FALSE)
    LET int_flag = FALSE 
    {CONSTRUCT where_clause 
      ON reqempcon, pueareaf, reqfecrec, reqtipo, pueuniorg, pueposicion, pueloc, reqususol, requsuaut }      --g_reg.city_num --sql, filename, header, preview
      CONSTRUCT BY NAME where_clause 
         ON reqmreq.reqempcon, reqmreq.pueareaf, reqmreq.reqfecrec, reqmreq.reqtipo, reqmreq.pueuniorg, 
            reqmreq.pueposicion, reqmreq.pueloc, reqmreq.reqususol, reqmreq.requsuaut 
      --ATTRIBUTES(UNBUFFERED, WITHOUT DEFAULTS=TRUE, ACCEPT=FALSE, CANCEL=FALSE)

         {BEFORE INPUT 
            LET sql_stmt = "SELECT FIRST 1 city_num FROM city WHERE ", where_clause CLIPPED, " ORDER BY 1 " 
            PREPARE ex_stmt FROM sql_stmt
            EXECUTE ex_stmt INTO g_reg.city_num 
            DISPLAY BY NAME g_reg.city_num}
      
        {ON ACTION excel ATTRIBUTES(TEXT="Generate Excel", IMAGE="mars_excel")
        
            LET sql = prepsql(where_clause)
            DISPLAY "SQL ==========> ", sql
            IF sql_to_excel(sql, filename, header) THEN
                IF preview THEN
                    CALL fgl_putfile(filename, filename)
                    CALL ui.Interface.frontCall("standard","shellExec", filename, result)
                ELSE
                    MESSAGE "Spreadsheet created"
                END IF
            ELSE
                ERROR "Something went wrong"
            END IF
            EXIT CONSTRUCT} 
        {ON ACTION salir
            EXIT CONSTRUCT } 
        ON ACTION close
            EXIT CONSTRUCT 

    END CONSTRUCT 
    
    IF int_flag THEN
      LET int_flag = FALSE
      ERROR "Reporte cancelado por el usuario"
      CLEAR SCREEN 
      RETURN 
    END IF 

    LET sql = prepsql(where_clause)
    DISPLAY "SQL ==========> ", SQL
    IF sql_to_excel(sql, filename, header) THEN
        IF preview THEN
            CALL fgl_putfile(filename, filename)
            CALL ui.Interface.frontCall("standard","shellExec", filename, result)
        ELSE
            MESSAGE "Spreadsheet created"
        END IF
    ELSE
        ERROR "Something went wrong"
    END IF
END MAIN


FUNCTION sql_to_excel(sql, filename, header)
DEFINE hdl base.SqlHandle
DEFINE sql STRING
DEFINE filename STRING
DEFINE header BOOLEAN
DEFINE row_idx, col_idx INTEGER 

DEFINE workbook     fgl_excel.workbookType 
DEFINE sheet        fgl_excel.sheetType  
DEFINE row          fgl_excel.rowType  
DEFINE cell         fgl_excel.cellType 
DEFINE header_style fgl_excel.cellStyleType
DEFINE header_font  fgl_excel.fontType

DEFINE datatype STRING
    
    LET hdl = base.SqlHandle.create()
    TRY
        CALL hdl.prepare(sql)
        CALL hdl.open()
    CATCH
        RETURN FALSE
    END TRY

    --CALL fgl_excel.workbook_create() RETURNING workbook
    CALL fgl_excel.workbook_open("Requisiciones_SISTEMA_t.xlsx") RETURNING workbook
    -- create a worksheet
    --CS CALL fgl_excel.workbook_createsheet(workbook) RETURNING sheet
    CALL fgl_excel.workbook_opensheet(workbook) RETURNING sheet
    
    -- create data rows
    --cs LET row_idx = 0 
    LET row_idx = 9 
    
    WHILE TRUE
        CALL hdl.fetch()
        IF STATUS=NOTFOUND THEN
            EXIT WHILE
        END IF
        LET row_idx = row_idx + 1

        IF row_idx = 1 AND header THEN
            -- create a font, will be used in header
            CALL fgl_excel.font_create(workbook) RETURNING header_font
            CALL fgl_excel.font_set(header_font, "weight", "bold")

            -- create a style, will be used in header
            CALL fgl_excel.style_create(workbook) RETURNING header_style
            CALL fgl_excel.style_set(header_style, "alignment","center")
            CALL fgl_excel.style_font_set(header_style, header_font)
   
            -- Add column headers
            CALL fgl_excel.sheet_createrow(sheet, 0) RETURNING row
            FOR col_idx = 1 TO hdl.getResultCount()
                CALL fgl_excel.row_createcell(row, col_idx-1) RETURNING cell
                CALL fgl_excel.cell_value_set(cell, hdl.getResultName(col_idx))
                CALL fgl_excel.cell_style_set(cell, header_style)
            END FOR
        END IF
        CALL fgl_excel.sheet_createrow(sheet, IIF(header,row_idx, row_idx-1)) RETURNING row

        FOR col_idx = 1 TO hdl.getResultCount()
            CALL fgl_excel.row_createcell(row, col_idx-1) RETURNING cell
            LET datatype = hdl.getResultType(col_idx) 
            CASE 
                WHEN datatype =  "INTEGER" -- TODO check logic
                  OR datatype MATCHES "DECIMAL*"
                  OR datatype MATCHES "FLOAT*"
                  OR datatype MATCHES "*INT*"
                    CALL fgl_excel.cell_number_set(cell, hdl.getResultValue(col_idx))
                OTHERWISE
                    CALL fgl_excel.cell_value_set(cell, hdl.getResultValue(col_idx))
            END CASE
        END FOR
    END WHILE

    -- TODO this code should automatically size the columns
    -- However it is very very slow for reasons I can't determine
    -- Uncomment and test at your leisure
    {IF hdl IS NOT NULL THEN
        FOR col_idx = 1 TO hdl.getResultCount()
            CALL fgl_excel.sheet_autosizecolumn(sheet, col_idx-1)
        END FOR
    END IF}

    -- Write to File
    CALL fgl_excel.workbook_writeToFile(workbook, filename)

    RETURN TRUE   
END FUNCTION

FUNCTION populate()
DEFINE idx INTEGER
DEFINE rec RECORD
    integer_type INTEGER,
    date_type DATE,
    char_type CHAR(20),
    float_type FLOAT
END RECORD

    CREATE TEMP TABLE test_data 
        (integer_type INTEGER,
         date_type DATE,
         char_type VARCHAR(20),
         float_type FLOAT)

    FOR idx = 1 TO 26
        LET rec.integer_type = idx
        LET rec.date_type = TODAY+idx
        LET rec.char_type = ASCII(64+idx)
        LET rec.float_type = 1/idx
        
        INSERT INTO test_data VALUES(rec.*)
    END FOR
END FUNCTION

FUNCTION prepsql(w_clause STRING )
   --DEFINE idSede LIKE city.city_num
   DEFINE lfec DATE 
   DEFINE sql_stmt STRING 
   
   --CALL creatmp()
   --CALL carga_datos(idSede)
   LET sql_stmt = " SELECT reqmreq.reqid, ",       --ID
                         " e.iniciales, ",   --Empresa, 
                         " a.afunombre, ",   --gerencia_funcional
                         " reqmreq.reqfecrec, ",   --fecha
                         " CASE WHEN (reqmreq.reqtipo = 'I') THEN 'Interno' ",
                         "      WHEN (reqmreq.reqtipo = 'E') THEN 'Exerno' ",
                         "      WHEN (reqmreq.reqtipo = 'A') THEN 'Interno y Externo' ",
                         " END, ",           --AS Tipo,
                         " u.uornombre, ",   --unidad_organizativa
                         " p.puenombre, ",   --posicion
                         " l.locnombre, ",   --localidad, 
                         " reqmreq.reqvacant, ",   --susituye_a
                         " reqmreq.jefenom, ",     --jefe_inmediato_superior,
                         " TRIM(em.nombre)||' '||TRIM(em.apellido), ", --Solicitante
                         " TRIM(au.nombre)||' '||trim(au.apellido), ", --autoriza
                         " et.etanombre, ",  --estado
                         " reqmreq.reqobs ", --observaciones
                         " FROM reqmreq, commemp e, glbmareaf a, glbuniorga u, glbmpue p, glbmloc l, commempl em, commempl au, reqmeta et ",
                         " WHERE reqmreq.reqempcon = e.id_commemp ",
                         " AND reqmreq.pueareaf = a.afuid ",
                         " AND reqmreq.pueuniorg = u.uorid ",
                         " AND reqmreq.pueposicion = p.pueid ",
                         " AND reqmreq.pueloc = l.locid ",
                         " AND reqmreq.reqususol = em.id_commempl ",
                         " AND reqmreq.requsuaut = au.id_commempl ",
                         " AND reqmreq.reqestado = et.etaid::character varying ",
                         " AND ", w_clause CLIPPED,
                         " ORDER BY 1 "
      
      DISPLAY "qry tmp ", sql_stmt
      --DISPLAY "where   ", w_clause
   RETURN sql_stmt
END FUNCTION

FUNCTION creatmp()
   CREATE TEMP TABLE tmp_rep_excel(
      contact_num          INTEGER, 
      contact_estado_caso  CHAR(18), 
      contact_city         INTEGER, --region 
      contact_operacion    CHAR(20), 
      contact_producto     CHAR(18), 
      contact_cuotas_mora  INT, --CHAR(20),     
      contact_fec_mora     CHAR(20),
      contact_saldo_cap    DECIMAL, --CHAR(20),
      contact_cap_vencido  DECIMAL,
      contact_int_vencido  DECIMAL,
      contact_saldo_imo    DECIMAL,
      contact_otros        DECIMAL,
      contact_total        DECIMAL,
      contact_porc_serv    DECIMAL,
      contact_total_cobrar DECIMAL,
      contact_estado_op    CHAR(20),
      contact_name         CHAR(100),
      contact_street       CHAR(240),
      contact_garantia     CHAR(40),
      contact_tel_cliente  CHAR(240),
      contact_fec_ult_pago CHAR(20),
      contact_dpi_cliente  CHAR(20),
      contact_nom_fia1     CHAR(240),
      contact_dir_fia1     CHAR(240),
      contact_nom_fia2     CHAR(240),
      contact_dir_fia2     CHAR(240),
      contact_nom_fia3     CHAR(240),
      contact_dir_fia3     CHAR(240)
   )
   
END FUNCTION  

--FUNCTION carga_datos(sede)

  { DEFINE sede LIKE city.city_num
   DEFINE lfecha DATE 
   
   TYPE t_reg RECORD
      contact_num          INTEGER, 
      contact_estado_caso  CHAR(18), 
      contact_city         INTEGER, --region 
      contact_operacion    CHAR(20), 
      contact_producto     CHAR(18), 
      contact_cuotas_mora  INT,     
      contact_fec_mora     CHAR(20),
      contact_saldo_cap    DECIMAL, --CHAR(20),
      contact_cap_vencido  DECIMAL,
      contact_int_vencido  DECIMAL,
      contact_saldo_imo    DECIMAL,
      contact_otros        DECIMAL,
      contact_total        DECIMAL,
      contact_porc_serv    DECIMAL,
      contact_total_cobrar DECIMAL,
      contact_estado_op    CHAR(20),
      contact_name         CHAR(100),
      contact_street       CHAR(240),
      contact_garantia     CHAR(40),
      contact_tel_cliente  CHAR(240),
      contact_fec_ult_pago CHAR(20),
      contact_dpi_cliente  CHAR(20),
      contact_nom_fia1     CHAR(240),
      contact_dir_fia1     CHAR(240),
      contact_nom_fia2     CHAR(240),
      contact_dir_fia2     CHAR(240),
      contact_nom_fia3     CHAR(240),
      contact_dir_fia3     CHAR(240)
   END RECORD
   DEFINE comentario          CHAR(2000)
   DEFINE loperacion          LIKE contact.contact_operacion
   DEFINE g_reg, g_new t_reg
   DEFINE g_ord RECORD
      contnote_num            LIKE contnote.contnote_contact, 
      contnote_cod_tipologia  LIKE contnote.contnote_cod_tipologia, 
      orden                   LIKE tipologia.orden
   END RECORD 
   DEFINE ga_reg DYNAMIC ARRAY OF t_reg
   DEFINE i    SMALLINT 
   DEFINE where_clause, sql_stmt STRING 
   DEFINE des_estado STRING 

   IF sede = 1 THEN --informe completo
      LET where_clause = ""
   ELSE
      LET where_clause = " AND c.contact_city = ", sede
      IF lgrupo = "SEDES" OR lgrupo = "OFICINA" THEN 
         LET where_clause = " AND c.contact_user = '", luser CLIPPED, "'"
      END IF 
   END IF 
   
   LET sql_stmt = "SELECT c.contact_num, c.contact_estado_caso, c.contact_city, ",
      " c.contact_operacion, c.contact_producto, CAST(c.contact_cuotas_mora AS INT), c.contact_fec_mora, ",
      " CAST(c.contact_saldo_cap AS DECIMAL(10,2)), CAST(c.contact_cap_vencido AS DECIMAL), ",
      " CAST(c.contact_int_vencido AS DECIMAL), CAST(c.contact_saldo_imo AS DECIMAL), ",
      " CAST(c.contact_otros AS DECIMAL), CAST(c.contact_total AS DECIMAL), ",
      " CAST(c.contact_porc_serv AS DECIMAL), CAST(c.contact_total_cobrar AS DECIMAL), ",
      " c.contact_estado_op, c.contact_name, c.contact_street, ",
      " c.contact_garantia, c.contact_tel_cliente, c.contact_fec_ult_pago, c.contact_dpi_cliente, ",
      " c.contact_nom_fia1, c.contact_dir_fia1, c.contact_nom_fia2, c.contact_dir_fia2, ",
      " c.contact_nom_fia3, c.contact_dir_fia3 ",
      " FROM contact c ", 
      --" WHERE n.contnote_contact = c.contact_num ",
      --" AND n.contnote_cod_tipologia = t.cod_tipologia ",
      --" WHERE DATE(contact_rec_mtime) = TODAY - 21 ", where_clause CLIPPED 
      " WHERE 1=1 ", where_clause CLIPPED 
      --where_clause CLIPPED 
      DISPLAY "sql_stmt -> ", sql_stmt
   PREPARE ex_stmt FROM sql_stmt
   DECLARE ccur CURSOR FOR ex_stmt}
      {SELECT n.contnote_contact, n.contnote_num, 
         c.contact_operacion, n.contnote_text, 
         n.contnote_cod_tipologia, t.des_tipologia, 
         n.contnote_fecha_promesa, n.contnote_monto_promesa
         --n.contnote_rec_mtime, day(n.contnote_rec_mtime)||'/'|| month(n.contnote_rec_mtime)||'/'|| year(n.contnote_rec_mtime)
      FROM contnote n, contact c,tipologia t 
      WHERE n.contnote_contact = c.contact_num
      AND n.contnote_cod_tipologia = t.cod_tipologia
      AND DATE(contnote_rec_mtime) = TODAY 
      AND c.contact_city = sede}

   {FOREACH ccur INTO g_reg.contact_num, des_estado, --g_reg.contact_estado_caso, 
      g_reg.contact_city, 
      g_reg.contact_operacion, g_reg.contact_producto, g_reg.contact_cuotas_mora, g_reg.contact_fec_mora,
      g_reg.contact_saldo_cap, g_reg.contact_cap_vencido, g_reg.contact_int_vencido,
      g_reg.contact_saldo_imo, g_reg.contact_otros, g_reg.contact_total, g_reg.contact_porc_serv,
      g_reg.contact_total_cobrar, g_reg.contact_estado_op, g_reg.contact_name, g_reg.contact_street, 
      g_reg.contact_garantia, g_reg.contact_tel_cliente, g_reg.contact_fec_ult_pago, g_reg.contact_dpi_cliente, 
      g_reg.contact_nom_fia1, g_reg.contact_dir_fia1, g_reg.contact_nom_fia2, g_reg.contact_dir_fia2, 
      g_reg.contact_nom_fia3, g_reg.contact_dir_fia3 

      LET g_reg.contact_saldo_cap = formatoNum(g_reg.contact_saldo_cap)

      CASE des_estado
         WHEN "RECUPERADO"    LET g_reg.contact_estado_caso = "RECUPERADO TOTAL"
         WHEN "REC-PARCIAL"   LET g_reg.contact_estado_caso = "RECUPERADO PARCIAL"
         OTHERWISE            LET g_reg.contact_estado_caso = des_estado
      END CASE 
      
      INSERT INTO tmp_rep_excel ( contact_num, contact_estado_caso, contact_city, 
      contact_operacion, contact_producto, contact_cuotas_mora, contact_fec_mora, 
      contact_saldo_cap, contact_cap_vencido, contact_int_vencido, 
      contact_saldo_imo, contact_otros, contact_total, contact_porc_serv, 
      contact_total_cobrar, contact_estado_op, contact_name, contact_street, 
      contact_garantia, contact_tel_cliente, contact_fec_ult_pago, contact_dpi_cliente, 
      contact_nom_fia1, contact_dir_fia1, contact_nom_fia2, contact_dir_fia2, 
      contact_nom_fia3, contact_dir_fia3 )
         
        VALUES (g_reg.contact_num, g_reg.contact_estado_caso, g_reg.contact_city, 
      g_reg.contact_operacion, g_reg.contact_producto, g_reg.contact_cuotas_mora, g_reg.contact_fec_mora,
      g_reg.contact_saldo_cap, g_reg.contact_cap_vencido, g_reg.contact_int_vencido,
      g_reg.contact_saldo_imo, g_reg.contact_otros, g_reg.contact_total, g_reg.contact_porc_serv,
      g_reg.contact_total_cobrar, g_reg.contact_estado_op, g_reg.contact_name, g_reg.contact_street, 
      g_reg.contact_garantia, g_reg.contact_tel_cliente, g_reg.contact_fec_ult_pago, g_reg.contact_dpi_cliente, 
      g_reg.contact_nom_fia1, g_reg.contact_dir_fia1, g_reg.contact_nom_fia2, g_reg.contact_dir_fia2, 
      g_reg.contact_nom_fia3, g_reg.contact_dir_fia3)
   
   END FOREACH }
   --SELECT COUNT(*) INTO i FROM tmp_rep_excel WHERE operacion = '7063266326'
   --DISPLAY "Cuando carga ", i 
   --Consolidando
   {DECLARE ccur2 CURSOR FOR 
      SELECT operacion
      FROM tmp_rep_excel
      GROUP BY operacion
      HAVING COUNT(*) > 1

   FOREACH ccur2 INTO loperacion

      DECLARE ccur3 CURSOR FOR 
         SELECT r.contnote_num, t.orden
            FROM tmp_rep_excel r, tipologia t
            WHERE r.cod_tipologia = t.cod_tipologia 
            AND r.operacion = loperacion
            ORDER BY 2

      LET i = 0
      FOREACH ccur3 INTO g_ord.contnote_num, g_ord.orden
      
         SELECT t.contnote_contact, t.contnote_num, t.operacion, t.comentario, 
            t.cod_tipologia, t.des_tipologia, t.fecha_promesa, t.monto_promesa
         INTO g_reg.contnote_contact, g_reg.contnote_num, 
            g_reg.contact_operacion, g_reg.contnote_text, 
            g_reg.contnote_cod_tipologia, g_reg.des_tipologia, 
            g_reg.contnote_fecha_promesa, g_reg.contnote_monto_promesa
         FROM tmp_rep_excel t
         WHERE t.contnote_num = g_ord.contnote_num
         
         IF i = 0 THEN
            LET i = 1
            LET g_new.contnote_contact       = g_reg.contnote_contact
            LET g_new.contnote_num           = g_reg.contnote_num 
            LET g_new.contact_operacion      = g_reg.contact_operacion 
            LET g_new.contnote_text          = g_reg.contnote_text
            LET g_new.contnote_cod_tipologia = g_reg.contnote_cod_tipologia
            LET g_new.des_tipologia          = g_reg.des_tipologia 
            LET g_new.contnote_fecha_promesa = g_reg.contnote_fecha_promesa
            LET g_new.contnote_monto_promesa = g_reg.contnote_monto_promesa
         ELSE 
            LET g_new.contnote_text          = g_new.contnote_text CLIPPED, "\n", g_reg.contnote_text
            LET g_new.contnote_text          = g_new.contnote_text CLIPPED, "\n", g_reg.contnote_cod_tipologia CLIPPED, ")"
            LET g_new.contnote_text          = g_new.contnote_text CLIPPED, " ", g_reg.des_tipologia CLIPPED, "\n"
         END IF 
      END FOREACH
      DELETE FROM tmp_rep_excel WHERE operacion = loperacion
      --SELECT COUNT(*) INTO i FROM tmp_rep_excel WHERE operacion = '7063266326'
      --DISPLAY "Despues del delete ", i
      INSERT INTO tmp_rep_excel ( contnote_contact, contnote_num,
         operacion, comentario, cod_tipologia, des_tipologia,
         fecha_promesa, monto_promesa )
         
        VALUES (g_new.contnote_contact, g_new.contnote_num, 
         g_new.contact_operacion, g_new.contnote_text, 
         g_new.contnote_cod_tipologia, g_new.des_tipologia, 
         g_new.contnote_fecha_promesa, g_new.contnote_monto_promesa) 
   END FOREACH} 
   --SELECT COUNT(*) INTO i FROM tmp_rep_excel WHERE operacion = '7063266326'
      --DISPLAY "Al terminar ", i
--END FUNCTION 

FUNCTION combo_init()
   --Empresa contratante
   CALL combo_din2("reqempcon","select id_commemp, trim(iniciales) from commemp WHERE estado=1 ORDER BY 2 ")
   --Locación / Ubicación
   CALL combo_din2("pueLoc","select locId, trim(locNombre) from glbMLoc ORDER BY 2")
   --Motivo
   CALL combo_din2("reqMotivo","select motId, trim(motNombre) from glbMMot ORDER BY 2")
   --Area Funcional del Puesto
   CALL combo_din2("pueAreaF", "SELECT afuId, trim(afuNombre) FROM glbMAreaF ORDER BY 2")
   --Unidad Organizativa
   CALL combo_din2("pueUniOrg","select uorId, trim(uorNombre) from glbUniOrga ORDER BY 2")
   --Posición / Puesto
   CALL combo_din2("puePosicion","select pueId, trim(pueNombre) from glbmpue ORDER BY 2")
   --Centro de Costo
   CALL combo_din2("pueCco","select ccoId, trim(ccoNombre) from glbCco ORDER BY 2")
   --Area Funcional del Puesto
   CALL combo_din2("jefeAreaF", "SELECT afuId, trim(afuNombre) FROM glbMAreaF ORDER BY 2")
   --Digitador
   CALL combo_din2("reqDigitador", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl ORDER BY 2")
   --Solicitante
   --CALL combo_din2("reqUsuSol", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esSolicitante = '1' ORDER BY 2" )
   CALL combo_din2("reqUsuSol", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE essolicitante = '1' ORDER BY 2" )
   --Autorizante
   --CALL combo_din2("reqUsuAut", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esAutorizante = '1' ORDER BY 2" )
   CALL combo_din2("reqUsuAut", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esAutorizante = '1' ORDER BY 2" )
   --Gerencia funcional que autoriza
   --CALL combo_din2("gerFunAut", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esGerAreaF = '1' ORDER BY 2")
   CALL combo_din2("gerFunAut", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esGerAreaF = '1' ORDER BY 2")
   --Unidad Organizativa Jefe
   CALL combo_din2("jefeUniOrg","select uorId, trim(uorNombre) from glbUniOrga ORDER BY 2")
   --Posición / Puesto Jefe
   CALL combo_din2("jefePue","select pueId, trim(pueNombre) from glbmpue ORDER BY 2")
   --CALL combo_din2("cmbccoUniOrga","select uorId, trim(uorNombre) from glbUniOrga")
   CALL combo_din2("reqanalista","select id_commempl, trim(nombre)||' '||trim(apellido) from commempl")
END FUNCTION 