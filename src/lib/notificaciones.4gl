IMPORT util 

DATABASE rh

   DEFINE
      g_reg RECORD
         reqId          LIKE reqMReq.reqid, --Id de la Requi --7
         
         destinatarios  STRING,
         
         empresa        LIKE commemp.iniciales,
         localidad      LIKE glbMLoc.locnombre,
         motivo         LIKE glbMMot.motnombre,
         area           LIKE glbMAreaF.afunombre,
         unidad         LIKE glbUniOrga.uornombre,
         posicion       LIKE glbMPue.puenombre,

         usuario        STRING,
         --rol            STRING,
         etapa          LIKE reqMEta.etanombre,
         fechaope       LIKE reqMBit.bitfecoper
      END RECORD
     

--Parámetros 
--  Id_requi = 7
--  Etapa    = 1 - Pendiente de Autorizar
--  Accion   = 1 - Autorizar
--  Etapa Res= 4 - Autorizada
FUNCTION notifica(rrequi INT, retapaini INT, raccion INT, retapares INT)   
   
   {LET cidrequi  = 7
   LET cetapa    = 1
   LET caccion   = 1
   LET cetapares = 4}
   
   IF destinatarios(rrequi, retapaini, raccion) THEN 
      --CALL datos(7,1,1,4)
      CALL datos(rrequi, retapaini, raccion, retapares)
      CALL crea_mail()
   ELSE 
      DISPLAY "No hay destinatarios"
   END IF 
END FUNCTION  

FUNCTION destinatarios(reqid INT, etapa INT, accion INT) 
   DEFINE res SMALLINT 
   DEFINE destinatarios    DYNAMIC ARRAY OF STRING
   DEFINE i                SMALLINT
   DEFINE lmot, lfluid     INTEGER 
   DEFINE lnotiDigi, lnotiSoli, lnotiAut, lnotiAutGF, lnotiCompe, lnotiCoord, lnotiAnal, lnotiCli CHAR(1)
   DEFINE lmail            STRING 
   
   LET g_reg.reqId = reqid 

   ----Roles
   IF etapa IS NULL THEN 
      SELECT FIRST 1 notiDigi, notiSoli, notiAut, notiAutGF, notiCompe, notiCoord, notiAnal, notiCli
         INTO lnotiDigi, lnotiSoli, lnotiAut, lnotiAutGF, lnotiCompe, lnotiCoord, lnotiAnal, lnotiCli 
         FROM reqDNot 
         WHERE reqDNot.etaid IS NULL 
           AND reqDNot.accid IS NULL 
   ELSE 
      SELECT FIRST 1 notiDigi, notiSoli, notiAut, notiAutGF, notiCompe, notiCoord, notiAnal, notiCli
         INTO lnotiDigi, lnotiSoli, lnotiAut, lnotiAutGF, lnotiCompe, lnotiCoord, lnotiAnal, lnotiCli 
         FROM reqDNot 
         WHERE reqDNot.etaid = etapa
           AND reqDNot.accid = accion
   END IF 
   LET i = 0
   
   --Digitador
   IF lnotiDigi = 'Y' THEN
      SELECT email INTO lmail FROM commempl, reqmreq WHERE id_commempl = reqMReq.reqDigitador AND reqMReq.reqId = reqid
      IF esEmail(lmail) THEN
         LET i = i + 1
         LET destinatarios[i] = lmail CLIPPED 
      END IF 
   END IF

   --Solicitante
   IF lnotiSoli = 'Y' THEN
      SELECT email INTO lmail FROM commempl, reqmreq WHERE id_commempl = reqMReq.reqUsuSol AND reqMReq.reqId = reqid
      IF sqlca.sqlcode = 0 THEN   
         IF esEmail(lmail) THEN
            LET i = i + 1
            LET destinatarios[i] = lmail CLIPPED 
         END IF 
      END IF 
   END IF

   --Autorizante
   IF lnotiAut = 'Y' THEN
      SELECT email INTO lmail FROM commempl, reqmreq WHERE id_commempl = reqMReq.reqUsuAut AND reqMReq.reqId = reqid
      IF sqlca.sqlcode = 0 THEN 
         IF esEmail(lmail) THEN
            LET i = i + 1
            LET destinatarios[i] = lmail CLIPPED 
         END IF 
      END IF 
   END IF

   --Autorizante GF
   IF lnotiAutGF = 'Y' THEN
      SELECT email INTO lmail FROM commempl, reqmreq WHERE id_commempl = reqMReq.gerFunAut AND reqMReq.reqId = reqid
      IF sqlca.sqlcode = 0 THEN 
         IF esEmail(lmail) THEN
            LET i = i + 1
            LET destinatarios[i] = lmail CLIPPED 
         END IF 
      END IF 
   END IF

   --Compensaciones
   IF lnotiCompe = 'Y' THEN
      --SELECT email INTO lmail FROM commempl, reqmbit WHERE id_commempl = reqMbit.bitusuoper AND reqMbit.reqId = reqid and reqmbit.accid = 4
      DECLARE curno CURSOR FOR SELECT email INTO lmail FROM commempl WHERE esNomina = '1'
      FOREACH curno INTO lmail
         IF esEmail(lmail) THEN
            LET i = i + 1
            LET destinatarios[i] = lmail CLIPPED 
         END IF 
      END FOREACH 
   END IF

   --Coord RRHH
   IF lnotiCoord = 'Y' THEN
      --SELECT email INTO lmail FROM commempl, reqmbit WHERE id_commempl = reqMbit.bitusuoper AND reqMbit.reqId = reqid and reqmbit.accid = 4
      DECLARE curcr CURSOR FOR SELECT email INTO lmail FROM commempl WHERE escoorrhh = '1'
      FOREACH curcr INTO lmail
         IF esEmail(lmail) THEN
            LET i = i + 1
            LET destinatarios[i] = lmail CLIPPED 
         END IF 
      END FOREACH 
   END IF

   --Analista RRHH
   IF lnotiAnal = 'Y' THEN
      SELECT email INTO lmail FROM commempl, reqmbit 
         WHERE id_commempl = reqMbit.bitusuoper AND reqMbit.reqId = reqid AND reqmbit.accid = 7
      IF sqlca.sqlcode = 0 THEN 
         IF esEmail(lmail) THEN
            LET i = i + 1
            LET destinatarios[i] = lmail CLIPPED 
         END IF 
      END IF  
   END IF

   --Cli Med
   IF lnotiCli = 'Y' THEN
      DECLARE curcm CURSOR FOR SELECT email INTO lmail FROM commempl WHERE esclimed = 1
      FOREACH curcm INTO lmail 
         IF esEmail(lmail) THEN
            LET i = i + 1
            LET destinatarios[i] = lmail CLIPPED 
         END IF 
      END FOREACH 
   END IF

   --Armando el to: con los destinatarios
   IF destinatarios.getLength() > 0 THEN
      IF destinatarios[1] = destinatarios[2] THEN
         --Cuando el digitador y el solicitante son la misma persona 
         CALL destinatarios.deleteElement(1)
      END IF 
      FOR i = 1 TO destinatarios.getLength()
         IF i > 1 THEN
            LET g_reg.destinatarios = g_reg.destinatarios CLIPPED, ',', destinatarios[i]
         ELSE    
            LET g_reg.destinatarios = destinatarios[1]
         END IF    
      END FOR 
      RETURN TRUE 
   ELSE 
      RETURN FALSE 
   END IF 

{
   IF accion = 1 THEN --autorizar
   END IF 
   DECLARE des CURSOR FOR select * from reqdnot
   LET destinatarios[1] = "carlos@solc.com.gt"
   LET destinatarios[2] = "contabilidad@solc.com.gt"

   FOR i = 1 TO destinatarios.getLength()
      IF i > 1 THEN 
         LET g_reg.destinatarios = g_reg.destinatarios CLIPPED, ',', destinatarios[i]
      ELSE    
         LET g_reg.destinatarios = destinatarios[1]
      END IF    
   END FOR 
   RETURN res}
END FUNCTION

FUNCTION datos(dreqid INT, detapa INT, daccion INT, detapares INT)   

   --Para empresa
   --LET g_reg.empresa          = "IPSA"
   SELECT e.iniciales INTO g_reg.empresa FROM commemp e, reqmreq r
      WHERE e.id_commemp = r.reqempcon AND r.reqid = dreqid

   --Para localidad
   --LET g_reg.localidad        = "ECOPET Producción"
   SELECT l.locNombre INTO g_reg.localidad FROM glbMLoc l, reqmreq r
      WHERE l.locId = r.pueloc AND r.reqid = dreqid

   --Para motivo
   --LET g_reg.motivo           = "Vacante"
   SELECT m.motNombre INTO g_reg.motivo FROM glbMMot m, reqmreq r
      WHERE m.motId = r.reqmotivo AND r.reqid = dreqid

   --Para area
   --LET g_reg.area             = "Operaciones"
   SELECT a.afuNombre INTO g_reg.area FROM glbMAreaF a, reqmreq r
      WHERE a.afuId = r.pueAreaF AND r.reqid = dreqid

   --Para unidad   
   --LET g_reg.unidad           = "Producción Recicla"
   SELECT u.uorNombre INTO g_reg.unidad FROM glbUniOrga u, reqmreq r
      WHERE u.uorId = r.pueUniOrg AND r.reqid = dreqid

   --Para posición
   --LET g_reg.posicion         = "Supervisor Producción"
   SELECT p.pueNombre INTO g_reg.posicion FROM glbMPue p, reqmreq r
      WHERE p.pueId = r.puePosicion AND r.reqid = dreqid

   --Para Usuario
   --LET g_reg.usuario          = "RENE IVAN FUENTES MIRANDA"
   SELECT FIRST 1 trim(nombre)||' '|| trim(apellido) INTO g_reg.usuario FROM commempl, reqmbit 
         WHERE id_commempl = reqMbit.bitusuoper AND reqMbit.reqId = dreqid AND reqmbit.accid = daccion

   --Para rol
   --LET g_reg.rol              = "Autorizante"

   --Para etapa
   --LET g_reg.etapa            = "AUTORIZADA"
   SELECT upper(trim(etanombre)) INTO g_reg.etapa FROM reqMEta WHERE etaId = detapares

   --Para fechaope
   --LET g_reg.fechaope         = "2024-06-20 10:46:14"
   SELECT FIRST 1 bitfecoper INTO g_reg.fechaope FROM reqmbit 
         WHERE reqMbit.reqId = dreqid AND reqmbit.accid = daccion
   
END FUNCTION 

--Valida que tenga al menos @ y .
FUNCTION esEmail(fmail STRING)

    IF fmail.matches('@') THEN 
       IF fmail.matches('\\.') THEN 
         RETURN TRUE 
      ELSE 
         RETURN FALSE 
      END IF 
    ELSE 
      RETURN FALSE 
    END IF

END FUNCTION 

FUNCTION crea_mail()
   DEFINE dt DATETIME YEAR TO SECOND
   DEFINE fn, filen, p_username, runcmd STRING 
   
   LET dt = CURRENT 
   LET fn = util.Datetime.format(dt, "%Y%m%d%H%M%S")
   
   PREPARE stmt FROM "SELECT CURRENT_USER"
   EXECUTE stmt INTO p_username

   LET filen = p_username CLIPPED, "_", fn CLIPPED, ".sh"
   DISPLAY "filen ", filen
   START REPORT rmail TO FILE filen
      OUTPUT TO REPORT rmail()
   FINISH REPORT rmail 

   LET runcmd = "/usr/bin/sh ", filen CLIPPED
   --DISPLAY "runcmd lleva ", runcmd  
   RUN runcmd
   LET runcmd = "rm -f ", filen CLIPPED 
   --RUN runcmd
   --DISPLAY "runcmd lleva ", runcmd  
END FUNCTION 

REPORT rmail()
OUTPUT
   LEFT MARGIN 0
   RIGHT MARGIN 0
   TOP MARGIN 0
   BOTTOM MARGIN 0
   PAGE LENGTH 72
   
   
   FORMAT 
      ON EVERY ROW
         PRINT 'to="', g_reg.destinatarios CLIPPED, '"' --'to="carlos@solc.com.gt"'
         PRINT 'from="Sistema de Requisiciones <notificaciones@rrhhingrup.com>"'
         PRINT 'boundary=$(uuidgen -t)'
         PRINT 'now=$(date +"%a, %d %b %Y %T %z")'
         PRINT ''
         PRINT 'BODY=$(mktemp)'
         PRINT 'ENVELOPE=$(mktemp)'
         PRINT ''
         PRINT 'cat > ${BODY} <<EOF3'
         PRINT ''
         PRINT '<!DOCTYPE html>'
         PRINT '<head>'
         PRINT '  <style>'
         PRINT '    table, th, td {'
         PRINT '      border: 1px solid black;'
         PRINT '      text-align: left;'
         PRINT '      border-collapse: collapse;'
         PRINT '    }'
         PRINT '  </style>'
         PRINT '</head>'
         PRINT '<body>'
         PRINT '  <img src="https://rrhhingrup.com/imagenes/logo-ingrup.png" width="300" height="95">'
         PRINT "  <font face='verdana' color='green'>"
         PRINT '    <h2>INGRUP - RRHH - Gestión de Talento</h2>'
         PRINT '  </font>'
         PRINT '  <p>La requisición con los siguientes datos:</p>'
         PRINT '  <table style="width:50%">'
         PRINT '    <tr><th>No.</th><td>',                  g_reg.reqId USING "<<<<<", '</td></tr>'  --'    <tr><th>No.</th><td>2</td></tr>'
         PRINT '    <tr><th>Empresa</th><td>',              g_reg.empresa CLIPPED,     '</td></tr>'  --'    <tr><th>Empresa</th><td>IPSA</td></tr>'
         PRINT '    <tr><th>Localidad</th><td>',            g_reg.localidad CLIPPED,   '</td></tr>'  --'    <tr><th>Localidad</th><td>Avenida Petapa Producción IPSA</td></tr>'
         PRINT '    <tr><th>Motivo</th><td>',               g_reg.motivo CLIPPED,      '</td></tr>'  --'    <tr><th>Motivo</th><td>Vacante</td></tr>'
         PRINT '    <tr><th>Area Funcional</th><td>',       g_reg.area CLIPPED,        '</td></tr>'  --'    <tr><th>Area Funcional</th><td>Operaciones</td></tr>'
         PRINT '    <tr><th>Unidad Organizativa</th><td>',  g_reg.unidad CLIPPED,      '</td></tr>'  --'    <tr><th>Unidad Organizativa</th><td>Soplado</td></tr>'
         PRINT '    <tr><th>Posición</th><td>',             g_reg.posicion CLIPPED,    '</td> </tr>' --'    <tr><th>Posición</th><td>Ayudante de Producción</td> </tr>'
         PRINT '  </table>'
         --PRINT '  <p>Habiéndose realizado la validación correspondiente por el usuario <b>Luis Barrientos (Autorizante)</b></p>'
         PRINT '  <p>Habiéndose realizado la validación correspondiente por el usuario <b>', g_reg.usuario CLIPPED, '</b></p>'
         PRINT '  <p style="color:blue">La misma fue <strong>', g_reg.etapa CLIPPED, '</strong></p>'  --'  <p style="color:blue">La misma fue <strong>AUTORIZADA</strong></p>'
         PRINT '  <p>Fecha de operación', g_reg.fechaope,'</p>'  --'  <p>Fecha de operación 2024-06-23 10:44</p>'
         PRINT '  <p>Ésta es una notificación automática enviada por el sistema, por favor no la responda.</p>'
         PRINT '  <p>Para mayor información puede ingresar a https://rrhhingrup.com </p>'
         PRINT '  <p>Si usted no es usuario del sistema de requisiciones o no está relacionado con esta requisición, por favor comuníquese con el área de gestión de talento del departamento de Recurso Humanos</p>'
         PRINT '</body>'
         PRINT ''
         PRINT 'EOF3'
         PRINT ''
         PRINT 'SUBJECT="Sistema de Requisiciones de Personal - La solicitud No.', g_reg.reqId USING '<<<<<', ' fue ', g_reg.etapa CLIPPED, '"'   --'SUBJECT="Sistema de Requisiciones de Personal - La solicitud No. 2 fue AUTORIZADA"' 
         PRINT ''
         PRINT 'cat > ${ENVELOPE} <<EOF2'
         PRINT 'From: ${from}'
         PRINT 'To: ${to}'
         PRINT 'Date: ${now}'
         PRINT 'Subject: ${SUBJECT}'
         PRINT 'MIME-Version: 1.0'
         PRINT 'Content-Type: multipart/alternative;boundary=${boundary}'
         PRINT ''
         PRINT '--${boundary}'
         PRINT 'Content-type: text/html;charset=utf-8'
         PRINT 'Content-Transfer-Encoding: base64'
         PRINT ''
         PRINT '$(cat ${BODY} | openssl base64 )' 
         PRINT ''
         PRINT '--${boundary}--'
         PRINT 'EOF2'
         PRINT '/usr/bin/mail -t < ${ENVELOPE}'
         PRINT 'sc=$?'
         PRINT 'echo "sedmail rc=${rc}"'
         PRINT 'if [[ $sc -eq 0 ]]; then'
         PRINT '    rm -f ${BODY} ${ENVELOPE}' 
         PRINT '    echo "Success"'
         PRINT 'else'
         PRINT '    echo "Fail"'
         PRINT 'fi'

END REPORT
