IMPORT os
--SCHEMA argoss
GLOBALS 
   --CONSTANT    WWW_LOCAL_URL = "http://192.168.0.50:6394/archivos/"
   CONSTANT    WWW_LOCAL_URL = "http://10.7.9.67:6394/archivos/"
   -- $FGLASDIR/web/archivos (la carpeta archivos hay que crearla vacia)
   --CS CONSTANT    WWW_LOCAL_DIR = "/opt/4js/g240r/gas/web/archivos/"
   CONSTANT    WWW_LOCAL_DIR = "/app/test/portal.dir/"
END GLOBALS
#########################################################################
## Function  : trae_usuario()
## Parametros: Ninguno
## Retorna: usuario del sistema, id de usuario y nombre de usuario
##
## Comments  : 
#########################################################################
{FUNCTION trae_usu_doc( lid )
DEFINE lid INTEGER 
DEFINE reg RECORD
   usu_id      LIKE doc_usu.usu_id,
   usu_cod     LIKE doc_usu.usu_login,
   usu_nom     LIKE doc_usu.usu_nom,
   email       LIKE doc_usu.email,
   --empr_id     LIKE doc_usu.empr_id,
   rol_id      LIKE doc_usu.rol_id
END RECORD 

   TRY
      SELECT usu_id, usu_login, usu_nom, email,  rol_id 
      INTO reg.usu_id, reg.usu_cod, reg.usu_nom, reg.email, reg.rol_id
      FROM doc_usu
      WHERE usu_id = lid
   CATCH
      INITIALIZE reg.* TO NULL 
   END TRY
   RETURN reg.*
END FUNCTION }

#########################################################################
## Function  : isGWC()
## Parametros: Ninguno
## Retorna   : False o True
## Comments  : Verifica si el ambiente es GWC
#########################################################################
{FUNCTION isGWC()
   RETURN ui.Interface.getFrontEndName() == "GWC"
END FUNCTION }

#########################################################################
## Function  : isGWC()
## Parametros: Ninguno
## Retorna   : False o True
## Comments  : Verifica si el ambiente es GWC
#########################################################################
{FUNCTION isGDC()
   RETURN ui.Interface.getFrontEndName() == "GDC"
END FUNCTION }

#########################################################################
## Function  : subir_edit()
## Parametros: ltipo 'P' PDF 'E' Editable
##             leditable ruta del archivo origen
##             es_remoto Ambiente 0 cliente 1 Servidor
## Retorna   : ruta del archivo destino
## Comments  : Funcion que sube un archivo y devuelve la ruta
#########################################################################
{FUNCTION subir_edit(ltipo, lnom, leditable, es_remoto)
DEFINE ltipo CHAR (1)
DEFINE lnom LIKE vdoc.codigo
DEFINE lcodigo STRING 
DEFINE leditable STRING 
DEFINE idx, es_remoto SMALLINT 
DEFINE ldest, lext STRING 
   IF ltipo = 'E' THEN 
      LET lext = "*.doc *.docx *.xls *.xlsx *.*"
   ELSE 
      LET lext = "*.pdf"
   END IF
   IF isGWC() THEN
      IF leditable IS NULL OR 
         leditable.equals("") THEN
         RETURN NULL 
      END IF 
   ELSE 
      CALL WINOPENFILE("C:\\","",lext,"Documento Editable")
      RETURNING leditable
   END IF 
   IF leditable IS NOT NULL OR  
      NOT leditable.equals("") THEN 
      IF es_remoto = 1 THEN 
         LET lext = os.Path.extension(leditable)
         IF ltipo = 'P' AND 
            downshift(lext) <> "pdf" THEN
            CALL msg("Archivo debe tener extension PDF")
            RETURN NULL 
         END IF 
         LET lcodigo = get_pal(lnom)
         --LET lcodigo = lcodigo.trim()
         --LET idx = lcodigo.getIndexOf(" ",1)
         --IF idx <> 0 THEN 
            --LET lcodigo = lcodigo.subString(1, idx)
         --END IF 
         LET ldest = "/tmp/",lcodigo,".",lext
         --LET ldest = "/tmp/",os.Path.basename(leditable) --Funciono pero puede dar problemas por tildes 
         TRY 
            CALL fgl_getfile(leditable, ldest)
            --
         CATCH 
            CALL msgError(status, "Subir Archivo")
            LET ldest = NULL 
         END TRY 
         LET leditable = ldest
      END IF 
   END IF 
   RETURN leditable
END FUNCTION }

FUNCTION get_blob_init(tipo_dest)
DEFINE tipo_dest   VARCHAR (6)
DEFINE query STRING 

   LET query = 
      --" SELECT LOTOFILE(doc_pdf,'c:\\temp\\prueba.PDF!','client')",
      " SELECT LOTOFILE(doc_pdf,?,'",tipo_dest,"')",
      " FROM doc_ver ver, doc_mdoc doc",
      " WHERE ver.doc_id = doc.doc_id ", 
      "   AND ver.doc_id = ? ",
      "   AND ver.ver_num = ? "
   PREPARE st_verpdf FROM query

   LET query = 
      " SELECT LOTOFILE(doc_editable,?,'",tipo_dest,"')",
      " FROM doc_ver ver, doc_mdoc doc",
      " WHERE ver.doc_id = doc.doc_id ", 
      "   AND ver.doc_id = ? ",
      "   AND ver.ver_num = ? "
   PREPARE st_veredit FROM query

   LET query = 
      " SELECT doc_bitacora", 
      " FROM doc_ver ver, doc_mdoc doc",
      " WHERE ver.doc_id = doc.doc_id ", 
      "   AND ver.doc_id = ? ",
      "   AND ver.ver_num = ? "
   PREPARE st_verbit FROM query
END FUNCTION 

{FUNCTION get_bit( r_doc, es_remoto )
DEFINE r_doc RECORD
   doc_id      LIKE doc_mdoc.doc_id,
   ver_num     LIKE doc_ver.ver_num,
   codigo      LIKE vdoc.codigo
END RECORD 
DEFINE es_remoto SMALLINT 
DEFINE bitacora TEXT
DEFINE lbitacora STRING 
DEFINE lfile, lname, ldest STRING 

   WHENEVER ERROR RAISE
   LET lname = get_pal(r_doc.codigo),'(',r_doc.ver_num USING "<<<<&&",')',".bit.txt"
   LET ldest = "c:\\temp\\",lname
   IF es_remoto THEN 
      LET lfile = WWW_LOCAL_DIR, lname
   ELSE 
      LET lfile = ldest
   END IF
   LOCATE bitacora IN FILE lfile 
   EXECUTE st_verbit USING r_doc.doc_id, r_doc.ver_num INTO bitacora
   IF es_remoto THEN 
      IF isGWC() THEN
         LET lfile = WWW_LOCAL_URL, lname
      ELSE
         CALL fgl_putfile(lfile, ldest)
      END IF
   END IF 
   --CALL bitacora.writeFile(lfile)
   LET lbitacora = bitacora
   RETURN lbitacora, lfile
END FUNCTION }

#########################################################################
## Function  : get_edit()
## Parametros: ltipo 'P' PDF 'E' Editable 
##             'M' Mail ( igual a Editable pero sin preguntar destino 
#              'R' Revision ( Igual a pdf pero no muestra el documento )
##             leditable ruta del archivo origen
##             es_remoto Ambiente 0 cliente 1 Servidor
## Retorna   : ruta del archivo destino
## Comments  : Funcion que sube un archivo y devuelve la ruta
#########################################################################
{FUNCTION get_edit ( r_doc, ltipo, es_remoto )
DEFINE r_doc RECORD
   doc_id      LIKE doc_mdoc.doc_id,
   ver_num     LIKE doc_ver.ver_num,
   codigo      LIKE vdoc.codigo,
   doc_tipo    LIKE doc_mdoc.doc_tipo
END RECORD 
DEFINE ltipo CHAR (1)
DEFINE es_remoto SMALLINT 
DEFINE lext, lname, lcodigo STRING 
DEFINE lori, ldest VARCHAR (200)

   IF ltipo == 'P' OR ltipo == 'R' THEN 
      LET lext = 'pdf'
   ELSE 
      LET lext = r_doc.doc_tipo
   END IF 
   TRY
      LET lcodigo = get_pal(r_doc.codigo)
      LET lname = lcodigo,'(',r_doc.ver_num USING "<<<<&&",')'
      IF es_remoto THEN
         LET lori = WWW_LOCAL_DIR,lname
         LET ldest = 'c:\\temp\\',lname,').',lext
         IF isGDC() AND ltipo = 'E' THEN
            CALL WINSAVEFILE(ldest,lext,"*."||lext,"Documento editable")
            RETURNING ldest
            IF ldest IS NULL THEN 
               RETURN NULL  
            END IF 
         END IF 
         --CALL os.Path.delete(lori) RETURNING int_flag
      ELSE 
         LET lori = 'c:\\temp\\',lname,').',lext
      END IF 
      --LET lpname = lori--,"!"
      
      IF ltipo == 'P' OR ltipo == 'R' THEN 
         EXECUTE st_verpdf USING lori, r_doc.doc_id, r_doc.ver_num INTO lori
      ELSE 
         EXECUTE st_veredit USING lori, r_doc.doc_id, r_doc.ver_num INTO lori
      END IF 
      LET lname = lori,".",lext
      LET lname = lname.trim()
      CALL os.Path.rename(lori, lname) RETURNING INT_FLAG
      LET lori = lname
      IF es_remoto THEN 
         TRY 
            IF isGWC() THEN
               IF ltipo = 'P' THEN 
                  LET lname = os.Path.basename(lori)
                  LET ldest = WWW_LOCAL_URL, lname
               ELSE 
                  --CALL box_valdato(lori||"&"||ldest)
                  CALL fgl_putfile(lori, ldest)
               END IF 
            END IF 
            IF isGDC() THEN 
               CASE  
               WHEN ltipo == 'E' OR ltipo == 'M'
                  CALL fgl_putfile(lori, ldest)
               WHEN ltipo == 'P'
                  CALL fgl_putfile(lori, ldest)
                  LET ldest = '"',ldest,'"'
                  IF WINSHELLEXEC(ldest) THEN END IF
               OTHERWISE --ltipo == 'R'
                  RETURN lori
               END CASE 
            END IF 
         CATCH 
            CALL msgError(status, "Descargar archivo")
            RETURN NULL 
         END TRY
      END IF 
   CATCH 
      CALL msgError(sqlca.sqlcode, "Recuperar archivo")
      RETURN NULL 
   END TRY 
   RETURN ldest
END FUNCTION} 

#########################################################################
## Function  : genera_filtro
## Parametros: lcriterio STRING Palabras separadas por espacios en blanco
##             que forman el criterio de busqueda
##             lempr_id Empresa
##             r_usu Usuario que realiza la consulta
## Retorna   : Condicion de clausula where para armar la consulta
## Comments  : Recibe critero de busqueda y arma consulta
#########################################################################
{FUNCTION genera_filtro( lcriterio, lempr_id, r_usu )
DEFINE lcriterio  STRING
DEFINE lempr_id   INTEGER 
DEFINE r_usu RECORD
   usu_id      LIKE doc_usu.usu_id,
   usu_login   LIKE doc_usu.usu_login,
   usu_nom     LIKE doc_usu.usu_nom,
   email       LIKE doc_usu.email,
   --empr_id     LIKE doc_usu.empr_id,
   rol_id      LIKE doc_usu.rol_id
END RECORD
DEFINE lcondicion, lpal STRING 
DEFINE lfiltro STRING
DEFINE idx SMALLINT 
 
   LET lcriterio = lcriterio.trim()
   LET lcondicion = "(",lempr_id USING "<<<<<&"," = 0 OR doc.ubic_empr_id=",lempr_id USING "<<<<<&",")"
   LET idx = lcriterio.getLength() 
   IF idx > 0 THEN 
      LET lcondicion = lcondicion," AND ("
      WHILE idx > 0 
         LET idx = lcriterio.getIndexOf(" ",1)
         IF idx = 0 THEN
            LET lpal = lcriterio
            LET lcriterio = ""
         ELSE 
            LET lpal = lcriterio.subString(1,idx - 1)
            LET lcriterio = lcriterio.subString(idx + 1,lcriterio.getLength())
            LET lcriterio = lcriterio.trim()
         END IF 
         LET lcondicion = lcondicion,genera_condicion(lpal)
         IF lcriterio.getLength() > 0 THEN
            LET lcondicion = lcondicion, " OR "
         END IF 
      END WHILE
      LET lcondicion = lcondicion, " )" 
   END IF 
   IF r_usu.rol_id <> 1 THEN
      LET lcondicion = lcondicion, 
         --Usuarios no administrativos no pueden ver documentos no vigentes
         " AND doc.doc_vigente = 1 ",
         --Ni tampoco privados 
         --A menos que esten en la lista de usuarios autorizados
         " AND ( doc.doc_priv = 0 ",
         " OR ",r_usu.usu_id USING "<<<<<<"," IN (",
         " SELECT usu_id FROM doc_dusu d WHERE d.doc_id = doc.doc_id )",
         --O sean miembros del comite de calidad
         " OR EXISTS (",
         " SELECT * FROM doc_usugru ",
         " WHERE usu_id = ",r_usu.usu_id USING "<<<<<<",
         "   AND gru_id IN ",
         "  ( SELECT gru_id FROM doc_dgru d WHERE d.doc_id = doc.doc_id )))"
   END IF 
   RETURN lcondicion 
END FUNCTION} 

FUNCTION genera_condicion( lcriterio )
DEFINE lcriterio STRING 
DEFINE lcondicion STRING 
DEFINE lfiltro STRING

   LET lcriterio = lcriterio.toUpperCase()
   LET  lfiltro = "LIKE '%",lcriterio.trim(),"%'"
   LET lcondicion = 
   " ( ",
   --"UPPER(dep.dep_cod) ", lfiltro," OR ",
   --"UPPER(dep.dep_nom) ", lfiltro," OR ",
   --"UPPER(cat.cat_cod) ", lfiltro," OR ",
   --"UPPER(cat.cat_nom) ", lfiltro," OR ",
   "UPPER(doc.codigo) ", lfiltro," OR ",
   --"UPPER(doc.doc_nom) ", lfiltro," OR ",
   --"UPPER(doc.doc_desc) ", lfiltro," OR ",
   "UPPER(elabora.usu_nom) ", lfiltro," OR ",
   "UPPER(usu_revisor) ", lfiltro,
   " ) "
   RETURN lcondicion
END FUNCTION 

FUNCTION get_pal( lpal )
DEFINE lpal STRING 
DEFINE idx SMALLINT 

   LET idx = lpal.getIndexOf(" ",1)
   IF idx > 0 THEN
      LET lpal = lpal.subString(1,idx - 1)
   END IF 
   RETURN lpal 
END FUNCTION 
