DATABASE rh

DEFINE gr_empl RECORD 
    id_commempl LIKE commempl.id_commempl,
    nomApe      VARCHAR(50), 
    --id_commemp  LIKE commempl.
    id_commpue  LIKE commempl.id_commpue,
    id_commdep  LIKE commempl.id_commdep
END RECORD 

DEFINE gr_reg dynamic ARRAY OF RECORD 
    id_commempl INT, 
    orden       SMALLINT, 
    rubro       LIKE commbd.descripcion,
    valor       LIKE commasig.valor
END RECORD 
DEFINE x SMALLINT 
DEFINE resp INT 

MAIN
  MENU 
    ON ACTION Calcula
      PROMPT "empleado " FOR resp
      CALL calcular(resp)
    ON ACTION Salir
      EXIT MENU 
  END MENU 
END MAIN 

FUNCTION calcular(empl_id)
DEFINE empl_id INT 

  --Empleados
  DECLARE curEmpl CURSOR FOR 
    SELECT id_commempl, TRIM(nombre) || ' ' || TRIM(apellido), 
           id_commdep, id_commpue
    FROM commempl WHERE id_commempl = empl_id --id_commempl > 0   

  LET x = 1  
  FOREACH curEmpl INTO gr_empl.id_commempl, gr_empl.nomApe,
          gr_empl.id_commdep, gr_empl.id_commpue    
     
     DISPLAY gr_empl.id_commempl, '-', gr_empl.nomApe
     DISPLAY "Asignaciones"
     --Asignaciones de empresa
     DECLARE curAsigEmp CURSOR FOR 
        SELECT a.descripcion, a.valor
        FROM commbd a, comdasi b
        WHERE a.id_commbd = b.id_commbd
        AND b.nivel = 1  --1=empresa, 2=depto, 3=puesto, 4=empleado
        AND a.operador = "+" -- + = asignaciones, - = descuentos
        --AND b.id_commemp = gr_empl.id_commemp 
     
     FOREACH curAsigEmp INTO gr_reg[x].rubro, gr_reg[x].valor
       DISPLAY "E->          ", gr_reg[x].rubro, '-',gr_reg[x].valor
     END FOREACH 
     CLOSE curAsigEmp
     
     --Asignaciones del depto
     DECLARE curAsigDep CURSOR FOR 
        SELECT a.descripcion, a.valor
        FROM commbd a, comdasi b
        WHERE a.id_commbd = b.id_commbd
        AND b.nivel = 2  --1=empresa, 2=depto, 3=puesto, 4=empleado
        AND a.operador = "+" -- + = asignaciones, - = descuentos
        AND b.id_commdep = gr_empl.id_commdep
     FOREACH curAsigDep INTO gr_reg[x].rubro, gr_reg[x].valor
       DISPLAY "D->          ", gr_reg[x].rubro, '-',gr_reg[x].valor
     END FOREACH
     CLOSE curAsigDep
     
     --Asignaciones del puesto
     DECLARE curAsigPue CURSOR FOR 
        SELECT a.descripcion, a.valor
        FROM commbd a, comdasi b
        WHERE a.id_commbd = b.id_commbd
        AND b.nivel = 3  --1=empresa, 2=depto, 3=puesto, 4=empleado
        AND a.operador = "+" -- + = asignaciones, - = descuentos
        AND b.id_commpue = gr_empl.id_commpue
     FOREACH curAsigPue INTO gr_reg[x].rubro, gr_reg[x].valor
       DISPLAY "P->           ", gr_reg[x].rubro, '-',gr_reg[x].valor
     END FOREACH  
     CLOSE curAsigPue
     
     --Asignaciones del empleado
     DECLARE curAsigEmpl CURSOR FOR 
        SELECT a.descripcion, a.valor
        FROM commbd a, comdasi b
        WHERE a.id_commbd = b.id_commbd
        AND b.nivel = 3  --1=empresa, 2=depto, 3=puesto, 4=empleado
        AND a.operador = "+" -- + = asignaciones, - = descuentos
        AND b.id_commempl = gr_empl.id_commempl
     FOREACH curAsigEmpl INTO gr_reg[x].rubro, gr_reg[x].valor
       DISPLAY "El->           ", gr_reg[x].rubro, '-',gr_reg[x].valor
     END FOREACH  
     CLOSE curAsigEmpl
     
     DISPLAY "Descuentos"
    --Descuentos de empresa
     DECLARE curDescEmp CURSOR FOR 
        SELECT a.descripcion, a.valor
        FROM commbd a, comdasi b
        WHERE a.id_commbd = b.id_commbd
        AND b.nivel = 1  --1=empresa, 2=depto, 3=puesto, 4=empleado
        AND a.operador = "-" -- + = asignaciones, - = descuentos
     
     FOREACH curDescEmp INTO gr_reg[x].rubro, gr_reg[x].valor
       DISPLAY "E->          ", gr_reg[x].rubro, '-',gr_reg[x].valor
     END FOREACH 
     CLOSE curDescEmp
     
     --Descuentos del depto
     DECLARE curDescDep CURSOR FOR 
        SELECT a.descripcion, a.valor
        FROM commbd a, comdasi b
        WHERE a.id_commbd = b.id_commbd
        AND b.nivel = 2  --1=empresa, 2=depto, 3=puesto, 4=empleado
        AND a.operador = "-" -- + = asignaciones, - = descuentos
     FOREACH curDescDep INTO gr_reg[x].rubro, gr_reg[x].valor
       DISPLAY "D->          ", gr_reg[x].rubro, '-',gr_reg[x].valor
     END FOREACH
     CLOSE curDescDep
     
     --Descuentos del puesto
     DECLARE curDescPue CURSOR FOR 
        SELECT a.descripcion, a.valor
        FROM commbd a, comdasi b
        WHERE a.id_commbd = b.id_commbd
        AND b.nivel = 3  --1=empresa, 2=depto, 3=puesto, 4=empleado
        AND a.operador = "-" -- + = asignaciones, - = descuentos
     FOREACH curDescPue INTO gr_reg[x].rubro, gr_reg[x].valor
       DISPLAY "P->           ", gr_reg[x].rubro, '-',gr_reg[x].valor
     END FOREACH  
     CLOSE curDescPue
     
     --Descuentos del empleado
     DECLARE curDescEmpl CURSOR FOR 
        SELECT a.descripcion, a.valor
        FROM commbd a, comdasi b
        WHERE a.id_commbd = b.id_commbd
        AND b.nivel = 3  --1=empresa, 2=depto, 3=puesto, 4=empleado
        AND a.operador = "-" -- + = asignaciones, - = descuentos
        AND b.id_commempl = gr_empl.id_commempl
     FOREACH curDescEmpl INTO gr_reg[x].rubro, gr_reg[x].valor
       DISPLAY "El->           ", gr_reg[x].rubro, '-',gr_reg[x].valor
     END FOREACH  
     CLOSE curDescEmpl  
  END FOREACH 
  FOR x=1 TO gr_reg.getLength()
     LET gr_reg[x].id_commempl = gr_empl.id_commempl
     LET gr_reg[x].orden = x
  END FOR 
END FUNCTION 