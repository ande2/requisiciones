#################################################################################
## Function  : picklist_4  --controla cuatro campos para despliegue
##
## Parameters: Title0,Title1,Title2,title3,title4,Field1,Field2,Field3,Field4,Tabname,Condicion,OrderNum
##
## Returnings: Codigo, campo2, campo3, descripcion, INT_FLAG
##
## Autor:  Ronald Palencia A. (08-mar-2007)
## Comments  : Utiliza arreglo dinamico y busca interectivamente (version Genero)
##             erick Alvarez         31-may-2007     cambio para mostrar todos los registros 
#################################################################################

FUNCTION picklist_4(Title0, Title1, Title2, title3, title4, Field1, Field2, Field3, Field4, Tabname, Condicion, OrderNum)
	DEFINE
		Title0 STRING,  -- Title OF FORM
		Title1,  		 -- Titles to display for the Fields
		Title2, title3, title4 STRING,  
		Field1,           -- Field names in the table 
		Field2, Field3, Field4,
		Condicion STRING,
		Tabname string, -- Table name to query from
		OrderNum  SMALLINT, -- Order by 1 or 2, a Number in a range of 1-2
   retorna RECORD  
		codigo VARCHAR(20),
		field2 VARCHAR(10),
		field3 VARCHAR(10),
		descripcion VARCHAR(100)
	END RECORD,
   i, aborta  smallint,
   w ui.Window,
   f ui.Form,
	la_busqueda dynamic ARRAY OF RECORD 	# Detalle de temas relacionados
		codigo VARCHAR(20),
		field2 VARCHAR(10),
		field3 VARCHAR(10),
		descripcion VARCHAR(100)
   END RECORD,
	comia CHAR(2),
	sqlstmnt STRING,
	refe VARCHAR(100),
	buscar VARCHAR(105)

	LET INT_FLAG = 0  LET comia = ASCII 34

	WHENEVER ERROR CONTINUE
		--Open the window at the position specified by the parameters Wxpos and Wypos
		OPEN WINDOW find_ayuda WITH FORM "picklist_4"
	WHENEVER ERROR STOP

	IF (STATUS < 0 ) THEN -- The Window could not be opened, 
                         --the form does not exist
                         -- or the parameters WxPos, WyPos are wrong
		ERROR "Error al abrir forma de búsqueda -- Consulte con el Programador --" SLEEP 1
		RETURN -1,NULL,NULL,NULL,INT_FLAG
	END IF

   LET w = ui.Window.getcurrent()
   LET f = w.getForm()

	CALL f.setElementText("filtro",title0)
	CALL f.setElementText("formonly.codigo",title1)
	CALL f.setElementText("formonly.field2",title2)
	CALL f.setElementText("formonly.field3",title3)
	CALL f.setElementText("formonly.descripcion",title4)

   CALL fgl_settitle("Búsqueda dinámica")

	WHILE TRUE
		INITIALIZE retorna.* TO NULL  LET refe = "Ingrese información a búscar..." 
		LET aborta = 0  LET INT_FLAG = FALSE  LET buscar = refe LET i = 1
		INPUT BY NAME refe WITHOUT DEFAULTS
				BEFORE INPUT
					CALL fgl_dialog_setkeylabel("ACCEPT","Detalle")
					CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
				AFTER input
					IF INT_FLAG THEN
						LET aborta = 1
						EXIT INPUT
					END IF
				AFTER FIELD refe

							  IF refe = "Ingrese información a búscar..." THEN
								  LET refe = "*"
							  END IF
								  LET buscar = "*"||refe||"*"

								  LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,",", Field3 CLIPPED, ",", field4 CLIPPED, 
												" FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
												" AND upper(", field4 CLIPPED, ") MATCHES upper(", 
												comia CLIPPED, buscar CLIPPED, comia CLIPPED, ")", " ORDER BY ",OrderNum

									WHENEVER ERROR CONTINUE
									  PREPARE ex_sqlst0 FROM sqlstmnt
									WHENEVER ERROR STOP

									--If the user supplied wrong values
									IF (SQLCA.SQLCODE <> 0) THEN
										DISPLAY sqlstmnt CLIPPED --rapavivi
										ERROR "Error en query -- Contacte al Programador --" SLEEP 1
										LET aborta = 1
										LET retorna.codigo = -1
										LET retorna.descripcion = ""
										EXIT INPUT
									END IF

								  DECLARE estos0 CURSOR FOR ex_sqlst0

								  CALL la_busqueda.CLEAR()
								  LET i =1
								  FOREACH estos0 INTO la_busqueda[i].*
									  LET i=i+1
								  END FOREACH

								  FREE estos0
								  FREE ex_sqlst0

								  CALL f.setElementText("registros",'Registros encontrados '||(i-1))

								  LET aborta = 0  
								  DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
									  BEFORE DISPLAY
											  CALL fgl_dialog_setkeylabel("ACCEPT","")
											  CALL fgl_dialog_setkeylabel("INTERRUPT","")
											  CALL fgl_dialog_setkeylabel("insert", "")
											  CALL fgl_dialog_setkeylabel("Append", "")
											  CALL fgl_dialog_setkeylabel("Delete", "")

									  BEFORE ROW
											  EXIT DISPLAY
								  END DISPLAY
								  LET buscar = refe
								  EXIT INput

				ON idle 1
					CALL GET_FLDBUF(refe) RETURNING refe
					IF refe IS NULL THEN
						LET refe = "*"
					END IF
					IF buscar <> refe THEN
						LET buscar = "*"||refe||"*"

					  	LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,",", Field3 CLIPPED, ",", field4 CLIPPED, 
									 " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
									 " AND upper(", field4 CLIPPED, ") MATCHES upper(", 
									 comia CLIPPED, buscar CLIPPED, comia CLIPPED, ")", " ORDER BY ",OrderNum

						 WHENEVER ERROR CONTINUE
						 	PREPARE ex_sqlst FROM sqlstmnt
						 WHENEVER ERROR STOP

					 	 --If the user supplied wrong values
						 IF (SQLCA.SQLCODE <> 0) THEN
							 DISPLAY sqlstmnt CLIPPED --rapavivi
							 ERROR "Error en query -- Contacte al Programador --" SLEEP 1
							 LET aborta = 1
							 LET retorna.codigo = -1
							 LET retorna.descripcion = ""
							 EXIT INPUT
						 END IF

						DECLARE estos2 CURSOR FOR ex_sqlst

						CALL la_busqueda.CLEAR()
   					LET i =1
   					FOREACH estos2 INTO la_busqueda[i].*
      					LET i=i+1
   					END FOREACH

						FREE estos2
						FREE ex_sqlst

						CALL f.setElementText("registros",'Registros encontrados '||(i-1))

						LET aborta = 0  
						DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
							BEFORE DISPLAY
									CALL fgl_dialog_setkeylabel("ACCEPT","")
									CALL fgl_dialog_setkeylabel("INTERRUPT","")
	  								CALL fgl_dialog_setkeylabel("insert", "")
	  								CALL fgl_dialog_setkeylabel("Append", "")
	  								CALL fgl_dialog_setkeylabel("Delete", "")

							BEFORE ROW
									EXIT DISPLAY
						END DISPLAY
						LET buscar = refe

					END IF --si es diferente ya el ingreso

			END INPUT

			IF aborta THEN
				LET INT_FLAG = TRUE
				EXIT WHILE
			END IF

			  --IF i = 1 THEN
					  --("No encontró ó seleccionó datos!")
					  --LET INT_FLAG = TRUE
					  --EXIT WHILE
			  --ELSE
				  --CALL la_busqueda.deleteElement(i)

				  LET aborta = 0  
				  DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
					  BEFORE DISPLAY
							CALL fgl_dialog_setkeylabel("ACCEPT","Seleccionar\nRegistro")
							CALL fgl_dialog_setkeylabel("insert", "")
							CALL fgl_dialog_setkeylabel("Append", "")
							CALL fgl_dialog_setkeylabel("Delete", "")
						  	CALL fgl_dialog_setkeylabel("INTERRUPT","Nueva\nBúsqueda")

			  				IF i = 1 THEN
					  			CALL box_valdato("No encontró ó seleccionó datos!")
							END IF

					  BEFORE ROW
						  LET i = ARR_CURR()
						  LET retorna.* = la_busqueda[i].*


					  AFTER DISPLAY
						  IF INT_FLAG THEN
							  LET aborta = 1
							  EXIT DISPLAY
						  END IF
				  END DISPLAY

				  IF aborta THEN
					  LET refe = "Ingrese información a búscar..."
					  CALL la_busqueda.CLEAR()
					  DISPLAY ARRAY la_busqueda TO sa_busqueda.*
						  BEFORE DISPLAY
						  EXIT DISPLAY
					  END DISPLAY
					  LET aborta = 0
				  ELSE --NO presiono nueva busqueda
					  LET aborta = 1
				  END IF
			  --END IF

			  IF aborta = 1 THEN
				  EXIT WHILE
			  END if

	END WHILE

   CLOSE window find_ayuda
	RETURN retorna.*, INT_FLAG
END FUNCTION

