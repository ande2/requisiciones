#########################################################################
## Function  : get_AndeVarLog()
##
## Parameters: width    << Ancho del Grid
##             height   << Alto del Grid
##             wName    << Window name
##
## Returnings: none
##
## Comments  : Crea un Grid en la forma
#########################################################################
SCHEMA rh

FUNCTION get_AndeVarLog(vParNombre)
DEFINE vAndeSesId CHAR(25) --LIKE andeVarLog.andeSesId
DEFINE vParNombre VARCHAR(25) --LIKE andeVarLog.parNombre
DEFINE vParValor  VARCHAR(50) --LIKE andeVarLog.parValor

   --DATABASE ande 
   --INITIALIZE vAndeSesId TO NULL 
   LET vAndeSesId = arg_val(2)
   IF vAndeSesId IS NULL THEN
      LET vParValor = NULL  
   ELSE
      --LET g_reg.userid = get_AndeVarLog(vAndeSesId, "LOGNAME")
      SELECT parValor INTO vParValor FROM ande:andeVarLog 
        WHERE andeSesId = vAndeSesId AND parNombre = vParNombre
   END IF 

--SELECT parValor INTO g_reg.userid FROM ande:andeVarLog 
     --WHERE andeSesId = vAndeSesId AND parNombre = "LOGNAME"
RETURN vParValor 
END FUNCTION 

#########################################################################
## Function  : canDelete()
##
## Parameters: tabValida    << Tabla donde va a validar
##             colValida    << Columna que va a ir a validar
##             valKey       << Variable local que tiene la llave que va a validar
##
## Returnings: True / False
##
## Comments  : Cuando la tabla es corporativa (creada en ANDE) y necesita 
##             validar que no se haya usado en las bases de datos 
##             de empresas (db0001, db0002, etc)
## Ejemplo   : En catalogo de empresas (catm0103) valida que la empresa
##             no exista en las fincas creadas en las distintas empresas
#########################################################################

FUNCTION can_deleteAll(tabValida, colValida, valKey)
DEFINE tabValida, colValida, valKey STRING 
DEFINE vDbname LIKE comMEmp.dbname
DEFINE vResult VARCHAR(50) 
DEFINE sql_stmt STRING 
 
  DECLARE curEmp CURSOR FOR 
    SELECT a.dbname FROM ande:commemp a
    WHERE a.esPropia = 't'
      AND a.dbname IS NOT NULL 

  INITIALIZE vResult TO NULL 
  FOREACH curEmp INTO vDbname
  DISPLAY "vDbname ", vDbname
    LET sql_stmt = "SELECT FIRST 1 ", colValida, " FROM ", vDbname CLIPPED, ":", tabValida,
                   " WHERE ", colValida, " = ", valKey
    PREPARE ex_stmt FROM sql_stmt
    WHENEVER ERROR CONTINUE 
      EXECUTE ex_stmt INTO vResult
    WHENEVER ERROR STOP 
    IF vResult IS NOT NULL THEN
       RETURN FALSE  
    END IF    
  END FOREACH 
  RETURN TRUE  
  
END FUNCTION 

#########################################################################
## Function  : canDelete()
##
## Parameters: tabValida    << Tabla donde va a validar
##             colValida    << Columna que va a ir a validar
##             valKey       << Variable local que tiene la llave que va a validar
##
## Returnings: True / False
##
## Comments  : Para saber si se puede eliminar un registro verificando 
##             la integridad referencial en una tabla de la BD local
#########################################################################

FUNCTION can_delete(tabValida, colValida, valKey)
DEFINE tabValida, colValida, valKey STRING 
DEFINE vResult VARCHAR(50) 
DEFINE sql_stmt STRING 
 
  LET sql_stmt = "SELECT FIRST 1 ", colValida, " FROM ", tabValida,
                 " WHERE ", colValida, " = ", valKey
  PREPARE ex_canDel FROM sql_stmt
  WHENEVER ERROR CONTINUE 
    EXECUTE ex_canDel INTO vResult
  WHENEVER ERROR STOP 
  IF vResult IS NOT NULL THEN
     RETURN FALSE  
  END IF     
  RETURN TRUE  
  
END FUNCTION 

#########################################################################
## Function  : getValParam()
##
## Parameters: tabValida    << Tabla donde va a validar
##             colValida    << Columna que va a ir a validar
##             valKey       << Variable local que tiene la llave que va a validar
##
## Returnings: True / False
##
## Comments  : Recibe como parametro el nombre del parámetro global y
##             retorna el valor del parámetro
##             El nombre del parametro se ingresa en el catalogo general
##             de parámetros glbm0003.42r
#########################################################################

FUNCTION getValParam(lNomParam)
DEFINE lNomParam VARCHAR(60,1) 
DEFINE lValParam VARCHAR(255,1) 

   WHENEVER ERROR CONTINUE 
   SELECT trim(valChr) INTO lValParam
   FROM glb_paramtrs
   WHERE nomPar = lNomParam
   WHENEVER ERROR STOP  
   IF sqlca.sqlcode = 0 THEN 
      RETURN lValParam
   ELSE 
      RETURN NULL 
   END IF 
END FUNCTION 

#########################################################################
## Function  : getPeso()
##
## Parameters: tabValida    << Tabla donde va a validar
##             colValida    << Columna que va a ir a validar
##             valKey       << Variable local que tiene la llave que va a validar
##
## Returnings: True / False
##
## Comments  : Recibe como parametro el nombre del parámetro global y
##             retorna el valor del parámetro
##             El nombre del parametro se ingresa en el catalogo general
##             de parámetros glbm0003.42r
#########################################################################

FUNCTION gral_reporte(tamanio, posicion, tipo, columnas, nom_reporte)
DEFINE tamanio     STRING
DEFINE posicion    STRING
DEFINE tipo        STRING
DEFINE ancho       STRING
DEFINE alto        STRING
DEFINE columnas    SMALLINT
DEFINE nom_reporte STRING
DEFINE myReporte   om.SaxDocumentHandler

        IF tamanio = "carta" OR tamanio = "CARTA" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "11in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "11in"
           END IF
        END IF

        IF tamanio = "oficio" OR tamanio = "OFICIO" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "13in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "13in"
           END IF       
        END IF

        {IF tamanio = "unapagina" THEN
           LET ancho = "10000"
           LET alto  = "10000"  
        END IF}

        IF NOT fgl_report_loadCurrentSettings(NULL) THEN
            RETURN
         END IF
         DISPLAY "ancho y alto ", ancho, alto
         CALL fgl_report_configurePageSize(ancho,alto)
         CALL fgl_report_configureCompatibilityOutput(columnas,"Monospaced",false,nom_reporte,"","")
         --CALL fgl_report_configureMultipageOutput(0, 0, FALSE)
         CALL fgl_report_selectDevice(tipo)
         IF tamanio = "unapagina" THEN
           CALL fgl_report_configureXLSDevice(1,1,1,1,1,1,0)  
        END IF 
         LET myReporte = fgl_report_commitCurrentSettings()
RETURN myReporte
END FUNCTION

#########################################################################
## Function  : getPeso()
##
## Parameters: tabValida    << Tabla donde va a validar
##             colValida    << Columna que va a ir a validar
##             valKey       << Variable local que tiene la llave que va a validar
##
## Returnings: True / False
##
## Comments  : Recibe como parametro el nombre del parámetro global y
##             retorna el valor del parámetro
##             El nombre del parametro se ingresa en el catalogo general
##             de parámetros glbm0003.42r
#########################################################################

FUNCTION gral_reporte_all(tamanio, posicion, tipo, columnas, nom_reporte)
DEFINE tamanio     STRING
DEFINE posicion    STRING
DEFINE tipo        STRING
DEFINE ancho       STRING
DEFINE alto        STRING
DEFINE columnas    SMALLINT
DEFINE nom_reporte STRING
DEFINE myReporte   om.SaxDocumentHandler

        IF tamanio = "carta" OR tamanio = "CARTA" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "11in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "11in"
           END IF
        END IF

        IF tamanio = "oficio" OR tamanio = "OFICIO" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "13in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "13in"
           END IF       
        END IF
        
        IF NOT fgl_report_loadCurrentSettings(NULL) THEN
            RETURN
         END IF
         --CALL fgl_report_configurePageSize(ancho,alto)
         CALL fgl_report_configureCompatibilityOutput(columnas,"Monospaced",false,nom_reporte,"","")
         --CALL fgl_report_configureMultipageOutput(0, 0, FALSE)
         CALL fgl_report_selectDevice(tipo)
         CALL fgl_report_configureXLSDevice(NULL,NULL,NULL,NULL,TRUE ,NULL,TRUE)
         LET myReporte = fgl_report_commitCurrentSettings()
RETURN myReporte
END FUNCTION

FUNCTION round(dval,dpos)

DEFINE 
  dval DECIMAL ,
  dpos SMALLINT ,
  ival DECIMAL (16,0)
  
LET ival = dval * (10 ** dpos)
LET dval = ival / (10 ** dpos)
RETURN dval

END FUNCTION 

#------------------------------------------------------
# Funcion:  numsem
# Recibe una fecha y devuelve el numero de la semana
#
# Let lNumSem = numSem(lfecha)
#
#------------------------------------------------------
FUNCTION numsem(fec1)
  DEFINE fec1, fec2 DATE 

  DEFINE numday SMALLINT 
  DEFINE numwday TINYINT 
  DEFINE numsem SMALLINT 

  LET fec2 = MDY(1,1,YEAR(TODAY))
  
  LET numday = (fec1 - fec2) + 1

  LET numwday = WEEKDAY (TODAY)

  LET numsem = (numday - numwday + 10) / 7

  RETURN numsem
  
END FUNCTION

FUNCTION nospace(texto STRING )
   DEFINE buf base.StringBuffer
   LET buf = base.StringBuffer.create()
   CALL buf.append(texto)
   CALL buf.replace(" ", "_", 0)
   RETURN buf.toString()
END FUNCTION 

FUNCTION nomUsuario(id INT)
DEFINE lnombre STRING 
    SELECT trim(nombre)||' '||trim(apellido) INTO lnombre
      FROM commempl 
      WHERE id_commempl = id
   RETURN lnombre 
END FUNCTION 