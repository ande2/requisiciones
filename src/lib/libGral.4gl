#########################################################################
## Function  : isGWC()
## Parametros: Ninguno
## Retorna   : False o True
## Comments  : Verifica si el ambiente es GWC
#########################################################################
FUNCTION isGWC()
   RETURN ui.Interface.getFrontEndName() == "GWC"
END FUNCTION 

#########################################################################
## Function  : isGWC()
## Parametros: Ninguno
## Retorna   : False o True
## Comments  : Verifica si el ambiente es GWC
#########################################################################
FUNCTION isGDC()
   RETURN ui.Interface.getFrontEndName() == "GDC"
END FUNCTION 
