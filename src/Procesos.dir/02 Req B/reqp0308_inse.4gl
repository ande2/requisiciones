################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "reqp0308_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO reqmreq ( ",
        " reqFecRec, reqEmpCon, pueLoc, reqMotivo, ",
        " reqTmpMes, reqVacAnt, reqTipo, ",
        " pueAreaF, pueUniOrg, puePosicion, pueCco, ",
        " puenecveh, puehorario, ",
        " jefeAreaF, jefeuniorg, jefepue, jefenom, ",
        " reqautplatmp, reqDigitador, reqUsuSol, reqUsuAut, gerFunAut, ",
        " reqEtapa, reqEstado, fecHorIngreso ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) "
--DISPLAY "INSERT ---- ", strSql 
   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   
   LET g_reg.reqId = 0
   LET g_reg.reqEtapa = 1 --Pendiente de autorizar
   LET g_reg.reqEstado = 1 --Pendiente de autorización
   LET g_reg.fecHorIngreso = CURRENT 
   
   TRY 
      EXECUTE st_insertar USING 
            --En postgres no se envía para que se autoincremente
            --g_reg.reqId, 
            g_reg.reqFecRec, g_reg.reqEmpCon, g_reg.pueLoc, g_reg.reqMotivo,
            g_reg.reqTmpMes, g_reg.reqVacAnt, g_reg.reqTipo,
            g_reg.pueAreaF, g_reg.pueUniOrg, g_reg.puePosicion, g_reg.pueCco,
            g_reg.puenecveh, g_reg.puehorario,
            g_reg.jefeAreaF, g_reg.jefeuniorg, g_reg.jefepue, g_reg.jefenom,
            g_reg.reqautplatmp, g_reg.reqDigitador, g_reg.reqUsuSol, g_reg.reqUsuAut, g_reg.gerFunAut,
            g_reg.reqEtapa, g_reg.reqEstado, g_reg.fecHorIngreso

      --CS agregarlo cuando se agregue el ID 
      LET g_reg.reqId = SQLCA.sqlerrd[2]
      DECLARE curcargadoc CURSOR FOR 
         SELECT rdaId, rdaNom, rdaDoc FROM tmpDoc
      LOCATE g_doc.rdaDoc IN MEMORY 
      FOREACH curcargadoc INTO g_doc.rdaId, g_doc.rdaNom, g_doc.rdaDoc
         INSERT INTO reqDDoc (reqId, rdaId, rdaNom, rdaDoc)
            VALUES (g_reg.reqId, g_doc.rdaId, g_doc.rdaNom, g_doc.rdaDoc)
      END FOREACH 
      --Notificación cuando se ingresa la Requi
      CALL notifica(g_reg.reqId, NULL, NULL, 1)
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   CALL display_reg()
   --DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("jefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   RETURN TRUE 
END FUNCTION 