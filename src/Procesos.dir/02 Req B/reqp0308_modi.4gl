################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar datos de un umdor
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "reqp0308_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE reqMReq ",
      "SET ",
      " reqFecRec       = ?, ",
      " reqEmpCon       = ?, ", 
      " pueLoc          = ?, ", 
      " reqMotivo       = ?, ",

      " reqTmpMes       = ?, ",
      " reqVacAnt       = ?, ",
      " reqTipo         = ?, ",
      
      " pueAreaF        = ?, ",
      " pueUniOrg       = ?, ",
      " puePosicion     = ?, ", 
      " pueCco          = ?, ",
      " puenecveh       = ?, ", 
      " puehorario      = ?, ", 

      " jefeAreaF       = ?, ",
      " jefeuniorg      = ?,  ",
      " jefepue         = ?,  ",
      " jefenom         = ?,  ",
      
      " reqautplatmp    = ?,  ",
      " reqDigitador    = ?,  ",
      " reqUsuSol       = ?,  ",
      " reqUsuAut       = ?,  ",
      " gerFunAut       = ?,  ",
      
      " reqEtapa        = ?,  ",
      " reqEstado       = ?,  ",
      " fecHorIngreso   = ?   ",
       
      " WHERE reqId     = ?"

   PREPARE st_modificar FROM strSql

END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION actualizar()
   DEFINE lfechora LIKE reqmbit.bitfecoper  
   TRY
      IF g_reg.reqEtapa = 2 AND g_reg.reqEstado = 2 THEN
         LET g_reg.reqEtapa = 1 LET g_reg.reqEstado = 1
         DISPLAY BY NAME g_reg.reqEtapa, g_reg.reqEstado

         LET lfechora = CURRENT
         INSERT INTO reqmbit ( etaidini, etaidfin, accid, bitobserva, 
                            reqid, etaestadoini, etaestadofin, 
                            bitusuoper, bitfecoper )
            VALUES ( 2, 1, 4, "Corregida y reenviada",
               g_reg.reqid, 2, 1,
               usuario, lfechora  )
      END IF
      EXECUTE st_modificar USING 
        g_reg.reqFecRec, g_reg.reqEmpCon, g_reg.pueLoc, g_reg.reqMotivo, 
        g_reg.reqTmpMes, g_reg.reqVacAnt, g_reg.reqTipo, 
        g_reg.pueAreaF, g_reg.pueUniOrg, g_reg.puePosicion, g_reg.pueCco, 
        g_reg.puenecveh, g_reg.puehorario, 
        g_reg.jefeAreaF, g_reg.jefeuniorg, g_reg.jefepue, g_reg.jefenom, 
        g_reg.reqautplatmp, g_reg.reqDigitador, g_reg.reqUsuSol, g_reg.reqUsuAut, g_reg.gerFunAut, 
        g_reg.reqEtapa, g_reg.reqEstado, g_reg.fecHorIngreso,
        u_reg.reqId

        DELETE FROM reqDDoc WHERE reqId = g_reg.reqId
        DECLARE curcargaupddoc CURSOR FOR 
         SELECT rdaId, rdaNom, rdaDoc FROM tmpDoc
         LOCATE g_doc.rdaDoc IN MEMORY 
         FOREACH curcargaupddoc INTO g_doc.rdaId, g_doc.rdaNom, g_doc.rdaDoc
            INSERT INTO reqDDoc (reqId, rdaId, rdaNom, rdaDoc)
               VALUES (g_reg.reqId, g_doc.rdaId, g_doc.rdaNom, g_doc.rdaDoc)
         END FOREACH

   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM reqMReq ",
      "WHERE reqId = ? "
      
   PREPARE st_delete FROM strSql

END FUNCTION 

FUNCTION anular()
   IF NOT box_confirma("Esta seguro de anular el registro") THEN
      RETURN FALSE
   END IF 
   TRY
      DELETE FROM reqDDoc WHERE reqId = g_reg.reqId
      EXECUTE st_delete USING g_reg.reqId
      
   CATCH 
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   CALL msg("Registro eliminado")
   RETURN TRUE 
END FUNCTION 