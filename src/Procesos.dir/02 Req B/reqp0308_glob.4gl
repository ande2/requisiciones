################################################################################
# Funcion     : %M%
# Descripcion : Catalogo de Empresas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
DATABASE rh

GLOBALS
TYPE 
   tDet RECORD 
      reqId          LIKE reqmReq.reqId,
      reqFecRec      LIKE reqmReq.reqFecRec,
      reqEmpCon      LIKE reqmReq.reqEmpCon,
      pueLoc         LIKE reqmReq.pueLoc,
      reqMotivo      LIKE reqmReq.reqMotivo,
      reqTmpMes      LIKE reqmReq.reqTmpMes,
      reqVacAnt      LIKE reqmReq.reqVacAnt,
      reqTipo        LIKE reqmReq.reqTipo,
      
      pueAreaF       LIKE reqmReq.pueAreaF,
      pueUniOrg      LIKE reqmReq.pueUniOrg,
      puePosicion    LIKE reqmReq.puePosicion,
      pueCco         LIKE reqmReq.pueCco,
      puenecveh      LIKE reqmReq.puenecveh, 
      puehorario     LIKE reqmReq.puehorario,
      
      jefeAreaF      LIKE reqmReq.jefeAreaF,
      jefeuniorg     LIKE reqmReq.jefeuniorg,
      jefepue        LIKE reqmReq.jefepue,
      jefenom        LIKE reqmReq.jefenom,

      reqautplatmp   LIKE reqmReq.reqautplatmp,

      reqDigitador   LIKE reqMReq.reqdigitador,
      reqUsuSol      LIKE reqMReq.reqususol,
      reqUsuAut      LIKE reqMReq.requsuaut,
      gerFunAut      LIKE reqMReq.gerfunaut,
      
      reqEtapa       LIKE reqmReq.reqEtapa,
      reqEstado      LIKE reqmReq.reqEstado,
      fecHorIngreso  LIKE reqmReq.fecHorIngreso,

      reqSalario     LIKE reqmReq.reqsalario,
      reqCondComp    LIKE reqmReq.reqcondcomp,
      reqHrsExt      LIKE reqMreq.reqhrsext,
      reqObs         LIKE reqMreq.reqobs,
      fechaIng       LIKE reqMreq.fechaing,
      codPosicion    LIKE reqMReq.codposicion,

      reqAnalista    LIKE reqMreq.reqanalista
      
   END RECORD
   TYPE t_doc RECORD
      reqId          LIKE reqDDoc.reqid,
      rdaId          LIKE reqDDoc.rdaid,
      rdaNom         LIKE reqDDoc.rdanom,
      rdaDoc         LIKE reqDDoc.rdadoc
   END RECORD

   TYPE
   rep_det  RECORD 
        reqDia         SMALLINT
      , reqMes         SMALLINT
      , reqAnio        SMALLINT
      , reqEmpresa     LIKE commemp.iniciales
      , reqMotivo      LIKE glbMMot.motnombre
      , reqVacante     LIKE reqMReq.reqvacant
      , reqTipo        LIKE reqMReq.reqtipo
      , reqAfuNombre   LIKE glbmareaf.afunombre
      , reqPueNombre   LIKE glbmpue.puenombre
      , reqCcoNombre   LIKE glbCco.cconombre
      , reqLocNombre   LIKE glbMLoc.locnombre
      , reqPueNecVeh   LIKE reqMReq.puenecveh
      , reqPueHorario  LIKE reqMreq.puehorario
   END RECORD 
   
   DEFINE codformato STRING
   DEFINE numdocs    SMALLINT  
   DEFINE
      reg_det DYNAMIC ARRAY OF tDet, 
      g_reg, u_reg tDet,
      g_doc    t_doc,
      a_doc DYNAMIC ARRAY OF t_doc
   
   DEFINE condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "reqp0308"

   DEFINE codformato STRING

   --Parámetros
   DEFINE usuario INTEGER 
   DEFINE numreq  INTEGER 
   DEFINE accion  CHAR(1)

END GLOBALS 