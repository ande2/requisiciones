################################################################################
# Funcion     : %M%
# Descripcion : Modulo para reporte
# Funciones   
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
IMPORT util 

GLOBALS "reqp0308_glob.4gl"

FUNCTION run_report_to_handler(handler)
DEFINE  pDet         rep_det 

DEFINE HANDLER       om.SaxDocumentHandler

   TRY
       START REPORT report1_report TO XML HANDLER HANDLER

      
      --Cargar los Datos de la requisicion
      SELECT    DAY(a.reqFecRec) as diaRec, MONTH (a.reqFecRec) as mesRec, YEAR(a.reqFecRec) as anioRec
               , b.iniciales, c.motNombre, a.reqvacant, a.reqTipo
               , f.afuNombre, g.pueNombre, d.ccoNombre, h.locNombre
               , a.puenecveh, a.pueHorario
               , a.JefePue, j.pueID, j.puenombre
         FROM  reqMReq a, commemp b, glbMMot c, glbCco d
               , glbuniorga e, glbmareaf f, glbmpue g
               , glbmloc h
               , glbuniorga i, glbmpue j
         WHERE a.reqId = 2
         AND   b.id_commemp = a.reqEmpCon
         AND   c.motId = a.reqmotivo
         AND   d.ccoID = a.pueCco
         AND   e.uorId = d.ccoUniOrga
         AND   f.afuID = e.uorareaf
         AND   g.pueId = a.pueposicion 
         AND   h.locid = a.pueloc
         AND   i.uorId = a.jefeUniOrg
         AND   j.pueUniOrga = i.uorId
         AND   j.pueId = a.JefePue


         OUTPUT TO REPORT report1_report ( pDet.* )
       
       FINISH REPORT report1_report
   END TRY
END FUNCTION

REPORT report1_report( rDet)
DEFINE  rDet      rep_det

   FORMAT
      ON EVERY ROW
         PRINTX rDet.*
END REPORT
