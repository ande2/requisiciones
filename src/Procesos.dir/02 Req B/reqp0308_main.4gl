################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal para umdores 
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
IMPORT util 

GLOBALS "reqp0308_glob.4gl"
   

MAIN
   DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING

   OPTIONS INPUT WRAP    

   LET prog_name2 = prog_name||".log"

   --'fglrun reqp0308.42r ', usuario.ID, ' ', reg_req[arr_curr()].reqid, ' ', 'V'  
   LET usuario = arg_val(1) 
   LET numreq  = arg_val(2)
   LET accion  = arg_val(3) --V=Ver una requisici�n específica 
                            --C=Crear
                            --E=Editar
   DISPLAY 'accion lleva --> ', accion
   CALL STARTLOG(prog_name2)
   CALL main_init()

END MAIN 

FUNCTION main_init()
   DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form
    
   CALL ui.Interface.loadActionDefaults("actiondefaults_rh1")
   --CALL combo_init()
	  
	INITIALIZE u_reg.* TO NULL
	LET g_reg.* = u_reg.*

   LET nom_forma = prog_name CLIPPED, "_form"

	OPEN WINDOW w1 WITH FORM nom_forma 
   
   CALL ui.Window.getCurrent().setText("ANDE - Catálogo de Empleados - "||nomUsuario(usuario))

	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()

   CALL query_init()
   CALL insert_init()
   CALL update_init()
   CALL delete_init()
   CALL combo_init()
   CALL creatmp()
   CALL main_menu()
   
END FUNCTION 

FUNCTION main_menu()

   DEFINE cuantos             SMALLINT,
         runcmd               STRING,
         lescoorrhh, lesrrhh  CHAR(1),
         lreqUsuSol           INTEGER 
    
    MENU
      BEFORE MENU
         CALL DIALOG.setActionActive("candidatos", 0) --can_user_append() )
         CALL DIALOG.setActionActive("siguiente",  0) --can_user_append() )
         CALL DIALOG.setActionActive("anterior",   0) --can_user_append() )
         CALL DIALOG.setActionActive("verdoc",     0) --can_user_append() )
         CALL DIALOG.setActionActive("modificar",  0) --can_user_append() )
         CALL DIALOG.setActionActive("anular",     0) --can_user_append() )
         CALL DIALOG.setActionActive("reporte",    0) --can_user_append() )
         CALL DIALOG.setActionActive("tiempos",    0) --can_user_append() )
         
         SELECT valchr INTO codformato FROM glb_paramtrs WHERE numpar = 2 AND tippar = 1
         DISPLAY BY NAME codformato
         
         IF accion = 'V' THEN --Ver llamado desde fuera del programa
            CALL DIALOG.setActionActive("buscar",  0) --can_user_append() )
            CALL DIALOG.setActionActive("agregar", 0) --can_user_append() )
            CALL DIALOG.setActionActive("reporte", 1)
            LET cuantos = consulta(FALSE)
            SELECT escoorrhh, esrrhh INTO lescoorrhh, lesrrhh FROM commempl
               WHERE id_commempl = usuario

            IF g_reg.reqUsuSol = usuario THEN 
               LET lreqUsuSol = 1
            ELSE 
               LET lreqUsuSol = 0
            END IF
            IF g_reg.reqDigitador = usuario THEN
               LET lreqUsuSol = 1
            END IF    
            IF lreqUsuSol = 1 AND g_reg.reqEstado IN (1,2) THEN
               CALL DIALOG.setActionActive("modificar", 1)
            ELSE    
               CALL DIALOG.setActionActive("modificar", 0)
            END IF   
            IF lreqUsuSol = 1 THEN
               CALL DIALOG.setActionActive("anular",     1)
            ELSE 
               CALL DIALOG.setActionActive("anular",     0)
            END IF 
            IF lescoorrhh = '1' OR lesrrhh = 1 OR lreqUsuSol = 1 THEN
               CALL DIALOG.setActionActive("candidatos",1) --can_user_append() )
               
               --DISPLAY "Soy coord o analista de RH"
               --CALL dialog.setActionHidden("compensacionesg", 0)
            ELSE 
               CALL DIALOG.setActionActive("candidatos",0) --can_user_append() )
            END IF 
         END IF
         IF accion = 'C' THEN --Crear llamado desde fuera del programa
            CALL DIALOG.setActionActive("buscar",  0) --can_user_append() )
            CALL DIALOG.setActionActive("agregar", 0) --can_user_append() )
            IF ingreso() THEN
                EXIT PROGRAM    
            END IF
         END IF 
         SELECT escoorrhh INTO lescoorrhh FROM commempl WHERE id_commempl = usuario
         IF lescoorrhh = 1 THEN 
            CALL DIALOG.setActionActive("tiempos",   1) --can_user_append() )
         END IF 

          IF accion = 'V' THEN --Crear llamado desde fuera del programa
            CALL DIALOG.setActionActive("verdoc",    1)
          END IF 
         --Mostrando info de compensaciones
         SELECT escoorrhh, esrrhh INTO lescoorrhh, lesrrhh FROM commempl
               WHERE id_commempl = usuario
         IF lescoorrhh = '1' OR lesrrhh = 1 THEN
            DISPLAY "Soy coord o analista de RH"
            CALL dialog.getForm().setElementHidden("compensacionesg", 0)
         ELSE 
            CALL dialog.getForm().setElementHidden("compensacionesg", 1)
         END IF 
         

      ON ACTION agregar
         CALL cleartmp()
         IF ingreso() THEN
         END IF 
         
      ON ACTION buscar
         LET cuantos = consulta(true)
         IF cuantos > 0 THEN 
            CALL DIALOG.setActionActive("candidatos",1) --can_user_append() )
            CALL DIALOG.setActionActive("siguiente", 1) --can_user_append() )
            CALL DIALOG.setActionActive("anterior",  1) --can_user_append() )
            IF numdocs > 0 THEN CALL DIALOG.setActionActive("verdoc",    1) END IF 
            CALL DIALOG.setActionActive("modificar", 1) --can_user_append() )
            CALL DIALOG.setActionActive("anular",    1) --can_user_append() )
            CALL DIALOG.setActionActive("reporte",   1) --can_user_append() )
            CALL display_reg()
         ELSE
            CALL DIALOG.setActionActive("candidatos",0) --can_user_append() )
            CALL DIALOG.setActionActive("siguiente", 0) --can_user_append() )
            CALL DIALOG.setActionActive("anterior",  0) --can_user_append() )
            CALL DIALOG.setActionActive("verdoc",    0) --can_user_append() )
            CALL DIALOG.setActionActive("modificar", 0) --can_user_append() )
            CALL DIALOG.setActionActive("anular",    0) --can_user_append() )
            CALL DIALOG.setActionActive("reporte",   0) --can_user_append() )
         END IF 
         CALL encabezado("")  

      ON ACTION candidatos
         LET runcmd = 'fglrun Candidatos.42r ', usuario, g_reg.reqId
         RUN runcmd 
         
      ON ACTION siguiente
         CALL sigant(1)
         IF numdocs > 0 THEN 
            CALL DIALOG.setActionActive("verdoc",    1)
         ELSE 
            CALL DIALOG.setActionActive("verdoc",    0)
         END IF 

      ON ACTION anterior
         CALL sigant(-1)
         IF numdocs > 0 THEN 
            CALL DIALOG.setActionActive("verdoc",    1)
         ELSE 
            CALL DIALOG.setActionActive("verdoc",    0)
         END IF

      ON ACTION verdoc
         CALL display_doc('D')

      ON ACTION modificar
         CALL cleartmp()
         IF g_reg.reqId > 0 THEN 
            IF modifica() THEN
               
            END IF   
         END IF 
         CALL encabezado("")
         
      ON ACTION anular
         IF g_reg.reqId > 0 THEN 
            IF anular() THEN
               --Si es anulacion
               --LET reg_det[id].* = g_reg.*
               --DISPLAY reg_det[id].* TO sDet[ids].*
               --Si es eliminacion
   
               INITIALIZE g_reg.* TO NULL
               DISPLAY BY NAME g_reg.*
            END IF   
         END IF 

      ON ACTION reporte
         LET runcmd = "fglrun reqp0310.42r ", g_reg.reqId
         RUN runcmd 

      ON ACTION tiempos
         LET runcmd = "fglrun Tiempos.42r ", g_reg.reqId
         RUN runcmd 

         
      ON ACTION salir 
         EXIT MENU 
   END MENU 

END FUNCTION 

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 

   DISPLAY BY NAME  gtit_enc
END FUNCTION 

FUNCTION combo_init()
   --Empresa contratante
   CALL combo_din2("reqempcon","select id_commemp, trim(iniciales) from commemp WHERE estado=1 ORDER BY 2 ")
   --Locación / Ubicación
   CALL combo_din2("pueLoc","select locId, trim(locNombre) from glbMLoc ORDER BY 2")
   --Motivo
   CALL combo_din2("reqMotivo","select motId, trim(motNombre) from glbMMot ORDER BY 2")
   --Area Funcional del Puesto
   CALL combo_din2("pueAreaF", "SELECT afuId, trim(afuNombre) FROM glbMAreaF ORDER BY 2")
   --Unidad Organizativa
   CALL combo_din2("pueUniOrg","select uorId, trim(uorNombre) from glbUniOrga ORDER BY 2")
   --Posición / Puesto
   CALL combo_din2("puePosicion","select pueId, trim(pueNombre) from glbmpue ORDER BY 2")
   --Centro de Costo
   CALL combo_din2("pueCco","select ccoId, trim(ccoNombre) from glbCco ORDER BY 2")
   --Area Funcional del Puesto
   CALL combo_din2("jefeAreaF", "SELECT afuId, trim(afuNombre) FROM glbMAreaF ORDER BY 2")
   --Digitador
   CALL combo_din2("reqDigitador", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl ORDER BY 2")
   --Solicitante
   --CALL combo_din2("reqUsuSol", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esSolicitante = '1' ORDER BY 2" )
   CALL combo_din2("reqUsuSol", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE essolicitante = '1' ORDER BY 2" )
   --Autorizante
   --CALL combo_din2("reqUsuAut", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esAutorizante = '1' ORDER BY 2" )
   CALL combo_din2("reqUsuAut", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esAutorizante = '1' ORDER BY 2" )
   --Gerencia funcional que autoriza
   --CALL combo_din2("gerFunAut", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esGerAreaF = '1' ORDER BY 2")
   CALL combo_din2("gerFunAut", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esGerAreaF = '1' ORDER BY 2")
   --Unidad Organizativa Jefe
   CALL combo_din2("jefeUniOrg","select uorId, trim(uorNombre) from glbUniOrga ORDER BY 2")
   --Posición / Puesto Jefe
   CALL combo_din2("jefePue","select pueId, trim(pueNombre) from glbmpue ORDER BY 2")
   --CALL combo_din2("cmbccoUniOrga","select uorId, trim(uorNombre) from glbUniOrga")
   CALL combo_din2("reqanalista","select id_commempl, trim(nombre)||' '||trim(apellido) from commempl")
END FUNCTION 

FUNCTION display_reg()

   --CALL display_doc()
   --DIALOG
      
      DISPLAY BY NAME g_reg.*
      DISPLAY util.Datetime.format(g_reg.fecHorIngreso, "%Y-%m-%d %H:%M:%S") TO fecHorIngreso 
      CALL display_doc('R')

      --DISPLAY ARRAY a_doc TO sa_doc.*
        -- BEFORE DISPLAY 
          --  EXIT DISPLAY
         --DISPLAY BY NAME g_reg.*
      --END DISPLAY
      {ON ACTION siguiente
         CALL sigant(1)

      ON ACTION anterior
         CALL sigant(-1)}
   --END DIALOG 
END FUNCTION 

FUNCTION creatmp()
   CREATE TEMP TABLE tmpdoc
   (
      reqId                 INTEGER,
      rdaId                 SMALLINT,
      rdaNom                VARCHAR(255),
      rdaDoc                BYTE   
   )
END FUNCTION 

FUNCTION cleartmp()
   DELETE FROM tmpdoc WHERE 1=1
END FUNCTION 

FUNCTION display_doc(accion CHAR(1))
   --R = Como parte del display Registro
   --D = Accion ver Documento
   --E = Para Eliminar el documento
   DEFINE x SMALLINT
   DEFINE result INTEGER  
   DEFINE nombre STRING
   --DEFINE sql_stmt STRING 

   IF accion = 'E' THEN
      DECLARE tdoclist CURSOR FOR SELECT rdaId, rdaNom FROM tmpdoc
      CALL a_doc.clear()
      LET x = 1
      FOREACH tdoclist INTO a_doc[x].rdaId, a_doc[x].rdaNom
         LET x = x + 1
      END FOREACH
      LET x = x - 1
      LET numdocs = x
   ELSE
      DECLARE rdoclist CURSOR FOR SELECT rdaId, rdaNom FROM reqDDoc WHERE reqId = g_reg.reqId
      CALL a_doc.clear()
      LET x = 1
      FOREACH rdoclist INTO a_doc[x].rdaId, a_doc[x].rdaNom
         LET x = x + 1
      END FOREACH
      LET x = x - 1
      LET numdocs = x
   END IF 
    
   DISPLAY ARRAY a_doc TO sa_doc.*
      ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
      BEFORE DISPLAY 
         --DISPLAY "accion lleva ------------> ", accion 
         IF accion = 'R' THEN 
            EXIT DISPLAY
         END IF 
         IF accion = 'E' THEN
            CALL DIALOG.setActionActive("deldocdel", 1)
         ELSE 
            CALL DIALOG.setActionActive("deldocdel", 0)
         END IF 

      ON ACTION regresar
         EXIT DISPLAY 
         
      ON ACTION deldocdel
         IF (box_pregunta("Eliminar el documento "||a_doc[arr_curr()].rdaNom)) = 'Si' THEN 
            DELETE FROM tmpdoc WHERE rdaId = a_doc[arr_curr()].rdaId
            CALL a_doc.deleteElement(arr_curr())
            
         END IF 
         
      ON ACTION mostrar
         LOCATE g_doc.rdaDoc IN FILE './files/'||nospace(a_doc[arr_curr()].rdaNom)
         SELECT rdaDoc INTO g_doc.rdaDoc FROM reqDDoc WHERE reqId = g_reg.reqId AND rdaId = a_doc[arr_curr()].rdaId
         CALL fgl_putfile('./files/'||nospace(a_doc[arr_curr()].rdaNom), a_doc[arr_curr()].rdaNom)
         CALL ui.interface.frontcall("standard","shellexec",[a_doc[arr_curr()].rdaNom],[result])
          
   END DISPLAY
   
END FUNCTION 

 

