
GLOBALS "reqp0309_glob.4gl"

FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE consulta  STRING
DEFINE x         SMALLINT
DEFINE rDet tDGR
DEFINE condicion STRING
DEFINE fdl, fal  DATE
DEFINE cmbreqetapa  INT  
DEFINE tipoVista SMALLINT

   LET x = 0

   IF flagConsulta THEN
       LET fdl = condi_fdl
       LET fal = condi_fal

       {CREATE VIEW vreqmeta AS
       SELECT 0 reqid, 'Todas' reqnom FROM reqmeta 
       UNION
       SELECT etaid, etanombre FROM reqmeta  
       ORDER BY 2}
       CALL combo_din2("cmbreqetapa","SELECT etaid, etanombre FROM vreqmeta ORDER BY etanombre ")
       
       DIALOG ATTRIBUTES(UNBUFFERED)
         INPUT BY NAME  
          condi_fdl, condi_fal, cmbreqetapa, condi_codpos
          ATTRIBUTES (WITHOUT DEFAULTS)

          BEFORE INPUT
             CALL DIALOG.setActionHidden("close",TRUE)          
       END INPUT 
       
       ON ACTION ACCEPT
          LET condi_eta = cmbreqetapa
          IF condi_eta = '0' THEN
             LET condi_eta = '1,2,3,4,5,6,7,8,9,10,11,12,13,14'
          END IF    
          DISPLAY "cmbreqetapa ", cmbreqetapa
          DISPLAY "condi_eta ", condi_eta
          
          IF condi_fdl >  condi_fal THEN
            CALL msg("La fecha 'DEL' debe ser menor a la fecha de 'AL' ")
            CONTINUE DIALOG
          END IF
          CALL reg_det.clear()
          CALL reg_req.clear()
          INITIALIZE gb_reg.* TO NULL
          INITIALIZE u_reg.* TO NULL
          INITIALIZE g_reg.* TO NULL
          INITIALIZE ur_reg.* TO NULL
          INITIALIZE gr_reg.* TO NULL
          LET permiso.vActualiza = FALSE
          LET permiso.vProcesar = FALSE
          LET permiso.vSoloVer = FALSE
          EXIT DIALOG 

         ON ACTION CANCEL
            LET condi_fdl = fdl
            LET condi_fal = fal
            EXIT DIALOG
            
       END DIALOG
   
   END IF

   LET tipoVista = 1 --Es analista

   --Digitador, Solicitante y Autorizante
   IF usuario.vDigita OR usuario.vSolicita OR usuario.vAutoriza THEN
      LET tipoVista = 4
   END IF
   IF usuario.vEsNomina THEN
      LET tipoVista = 2
   END IF
   IF usuario.vEsCooRRHH THEN
      LET tipoVista = 0
   END IF
   IF usuario.vAutorFun THEN
      LET tipoVista = 3
   END IF 
   
   LET condicion = ' 1 = 1 '
   --Armar la consulta
   CASE tipoVista
      WHEN 0  --coord rrhh
       IF NOT flagConsulta OR condi_eta IS NULL THEN
          LET condi_eta = '6'
       END IF 
       LET consulta = 
          "SELECT  r.reqid, r.reqfecrec, r.reqmotivo, r.reqetapa, r.reqestado, ", 
          " u.uornombre, a.afunombre, p.puenombre, ", 
          " r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut ", 
          " FROM reqmreq r, glbuniorga u, glbmareaf a, glbmpue p", 
          " WHERE r.reqid > 0 ", 
          " AND r.reqfecrec BETWEEN '", condi_fdl USING "DD/MM/YYYY", "' AND '", condi_fal USING "DD/MM/YYYY", "' "
          IF condi_eta IS NOT NULL THEN 
             LET consulta = consulta CLIPPED, " AND r.reqetapa IN ( ", condi_eta ,") "
          END IF
          
          IF condi_codpos IS NOT NULL OR condi_codpos.getLength() > 0 THEN
             LET consulta = consulta CLIPPED, " AND r.codposicion = '", condi_codpos CLIPPED, "'"
          END IF
          LET consulta = consulta CLIPPED,
          " AND p.pueid = r.pueposicion AND u.uorid = r.pueuniorg AND a.afuid = u.uorareaf ",
          " AND (", usuario.ID, " IN (r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut) ",
          "       OR ", usuario.ID, " IN (r.reqanalista) ) ",
          " UNION ", 
          " SELECT  r.reqid, r.reqfecrec, r.reqmotivo, r.reqetapa, r.reqestado, ", 
          " u.uornombre, a.afunombre, p.puenombre, ", 
          " r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut ", 
          " FROM reqmreq r, glbuniorga u, glbmareaf a, glbmpue p", 
          " WHERE r.reqid > 0 ", 
          " AND r.reqfecrec BETWEEN '", condi_fdl USING "DD/MM/YYYY", "' AND '", condi_fal USING "DD/MM/YYYY", "' "
          IF condi_eta IS NOT NULL THEN 
             LET consulta = consulta CLIPPED, " AND r.reqetapa IN ( ", condi_eta ,") "
          END IF
          IF condi_codpos IS NOT NULL OR condi_codpos.getLength() > 0 THEN
          DISPLAY "condi_eta =======> ", condi_eta
             LET consulta = consulta CLIPPED, " AND r.codposicion = '", condi_codpos CLIPPED, "'"
          END IF
          LET consulta = consulta CLIPPED,
          " AND p.pueid = r.pueposicion AND u.uorid = r.pueuniorg AND a.afuid = u.uorareaf ", 
          " AND ( r.gerfunaut IS NULL OR ", usuario.ID, " NOT IN (r.gerfunaut) ) ", 
          " AND ( r.requsuaut IS NULL OR ", usuario.ID, " NOT IN (r.requsuaut) ) ", 
          " AND ",usuario.ID, " NOT IN (r.reqdigitador, r.reqususol) ", 
          " AND ( EXISTS ( SELECT * FROM reqmflu WHERE reqmflu.etaid = r.reqetapa AND reqmflu.motid = r.reqmotivo AND reqmflu.flurestinusu = ", usuario.vTipo," ) ",
          "     OR EXISTS ( SELECT * FROM reqmflu WHERE reqmflu.etaid = r.reqetapa AND ( reqmflu.motid = r.reqmotivo OR reqmflu.motid IS NULL ) AND reqmflu.flurestinusu IS NULL ) ) ",
          " ORDER BY 1"
         DISPLAY "condi_codpos =======> ", condi_codpos 
          DISPLAY "Consulta 0 coor rrhh =======> ", consulta
          
   WHEN 1 --analistas
       LET consulta = 
          "SELECT  r.reqid, r.reqfecrec, r.reqmotivo, r.reqetapa, r.reqestado, ", 
          " u.uornombre, a.afunombre, p.puenombre, ", 
          " r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut ", 
          " FROM reqmreq r, glbuniorga u, glbmareaf a, glbmpue p", 
          " WHERE r.reqid > 0 ", 
          " AND r.reqfecrec BETWEEN '", condi_fdl USING "DD/MM/YYYY", "' AND '", condi_fal USING "DD/MM/YYYY", "' "
          IF condi_eta IS NOT NULL THEN 
             LET consulta = consulta CLIPPED, " AND r.reqetapa IN ( ", condi_eta ,") "
          END IF
          IF condi_codpos IS NOT NULL OR condi_codpos.getLength() > 0 THEN
             LET consulta = consulta CLIPPED, " AND r.codposicion = ", condi_codpos
          END IF
          LET consulta = consulta CLIPPED,
          " AND p.pueid = r.pueposicion AND u.uorid = r.pueuniorg AND a.afuid = u.uorareaf ", 
          " AND (", usuario.ID, " IN (r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut) ",
          "       OR ", usuario.ID, " IN (r.reqanalista) ) ", 
          --" UNION ", 
          --" SELECT  r.reqid, r.reqfecrec, r.reqmotivo, r.reqetapa, r.reqestado, ", 
          --" u.uornombre, a.afunombre, p.puenombre, ", 
          --" r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut ", 
          --" FROM reqmreq r, glbuniorga u, glbmareaf a, glbmpue p", 
          --" WHERE r.reqid > 0 ", 
          --" AND r.reqfecrec BETWEEN '", condi_fdl USING "DD/MM/YYYY", "' AND '", condi_fal USING "DD/MM/YYYY", "' ", 
          --" AND p.pueid = r.pueposicion AND u.uorid = r.pueuniorg AND a.afuid = u.uorareaf ", 
          --" AND ( r.gerfunaut IS NULL OR ", usuario.ID, " NOT IN (r.gerfunaut) ) ", 
          --" AND ( r.requsuaut IS NULL OR ", usuario.ID, " NOT IN (r.requsuaut) ) ", 
          --" AND ",usuario.ID, " NOT IN (r.reqdigitador, r.reqususol) ", 
          --" AND r.reqetapa IN ( ", condi_eta ,") ", 
          " ORDER BY 1" 
          DISPLAY "Consulta 1 analista =======> ", consulta
          
     WHEN 2 --usu nomina
     DISPLAY "condi_eta =======> ", condi_eta
        LET consulta = 
          "SELECT  r.reqid, r.reqfecrec, r.reqmotivo, r.reqetapa, r.reqestado, ", 
          " u.uornombre, a.afunombre, p.puenombre, ", 
          " r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut ", 
          " FROM reqmreq r, glbuniorga u, glbmareaf a, glbmpue p", 
          " WHERE r.reqid > 0 ", 
          " AND r.reqfecrec BETWEEN '", condi_fdl USING "DD/MM/YYYY", "' AND '", condi_fal USING "DD/MM/YYYY", "' "
          IF condi_eta IS NOT NULL THEN
            LET consulta = consulta CLIPPED, " AND r.reqetapa IN ( ", condi_eta ,") "
          END IF
          IF condi_codpos IS NOT NULL OR condi_codpos.getLength() > 0 THEN
             LET consulta = consulta CLIPPED, " AND r.codposicion = ", condi_codpos
          END IF
          LET consulta = consulta CLIPPED, 
          " AND p.pueid = r.pueposicion AND u.uorid = r.pueuniorg AND a.afuid = u.uorareaf ", 
          " AND (", usuario.ID, " IN (r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut) ",
          "       OR ", usuario.ID, " IN (r.reqanalista) ) ", 
          " UNION ", 
          " SELECT  r.reqid, r.reqfecrec, r.reqmotivo, r.reqetapa, r.reqestado, ", 
          " u.uornombre, a.afunombre, p.puenombre, ", 
          " r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut ", 
          " FROM reqmreq r, glbuniorga u, glbmareaf a, glbmpue p", 
          " WHERE r.reqid > 0 ", 
          " AND r.reqfecrec BETWEEN '", condi_fdl USING "DD/MM/YYYY", "' AND '", condi_fal USING "DD/MM/YYYY", "' "
          IF condi_eta IS NOT NULL THEN 
             LET consulta = consulta CLIPPED, " AND r.reqetapa IN ( ", condi_eta ,") "
          END IF
          IF condi_codpos IS NOT NULL OR condi_codpos.getLength() > 0 THEN
          DISPLAY "condi_codpos lleva ======> ", condi_codpos
             LET consulta = consulta CLIPPED, " AND r.codposicion = ", condi_codpos
          END IF
          LET consulta = consulta CLIPPED,
          " AND p.pueid = r.pueposicion AND u.uorid = r.pueuniorg AND a.afuid = u.uorareaf ", 
          " AND ( r.gerfunaut IS NULL OR ", usuario.ID, " NOT IN (r.gerfunaut) ) ", 
          " AND ( r.requsuaut IS NULL OR ", usuario.ID, " NOT IN (r.requsuaut) ) ", 
          -- ETAPA 4 ES AUTORIZA Y QUE EL MOTIVO NO SEA PLAZA NUEVA
          " AND ( r.reqetapa <> 4 or ( r.reqetapa = 4 AND r.reqmotivo not in ( select motid from glbMMot where flagvac is null or flagvac NOT IN ('1') ) ) ) ",
          " AND ",usuario.ID, " NOT IN (r.reqdigitador, r.reqususol) ", 
          " ORDER BY 1" 
          DISPLAY "Consulta 2 usu nom =======> ", consulta  

     WHEN 3  --Ger Funcional
       IF NOT flagConsulta THEN
          LET condi_eta = 4
       END IF 
       
       LET consulta = 
          "SELECT  r.reqid, r.reqfecrec, r.reqmotivo, r.reqetapa, r.reqestado, ", 
          " u.uornombre, a.afunombre, p.puenombre, ", 
          " r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut ", 
          " FROM reqmreq r, glbuniorga u, glbmareaf a, glbmpue p", 
          " WHERE r.reqid > 0 ", 
          " AND r.reqfecrec BETWEEN '", condi_fdl USING "DD/MM/YYYY", "' AND '", condi_fal USING "DD/MM/YYYY", "' "
          IF condi_eta IS NOT NULL THEN 
             LET consulta = consulta CLIPPED, " AND r.reqetapa IN ( ", condi_eta ,") "
          END IF
          LET x = condi_codpos.getLength()
          IF condi_codpos IS NOT NULL OR condi_codpos.getLength() > 0 THEN
             LET consulta = consulta CLIPPED, " AND r.codposicion = ", condi_codpos
          END IF
          LET consulta = consulta CLIPPED, 
          " AND p.pueid = r.pueposicion AND u.uorid = r.pueuniorg AND a.afuid = u.uorareaf ", 
          " AND (", usuario.ID, " IN (r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut) ",
          "       OR ", usuario.ID, " IN (r.reqanalista) ) ",
          " ORDER BY 1" 
          DISPLAY "Consulta 3 GAreaF =======> ", consulta  

      WHEN 4 --digitador, solcitante y autorizante
       IF NOT flagConsulta OR condi_eta IS NULL THEN
          LET condi_eta = '1'
       END IF
       LET consulta = 
          "SELECT  r.reqid, r.reqfecrec, r.reqmotivo, r.reqetapa, r.reqestado, ", 
          " u.uornombre, a.afunombre, p.puenombre, ", 
          " r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut ", 
          " FROM reqmreq r, glbuniorga u, glbmareaf a, glbmpue p", 
          " WHERE r.reqid > 0 ", 
          " AND r.reqfecrec BETWEEN '", condi_fdl USING "DD/MM/YYYY", "' AND '", condi_fal USING "DD/MM/YYYY", "' "
          IF condi_eta IS NOT NULL THEN 
             LET consulta = consulta CLIPPED, " AND r.reqetapa IN ( ", condi_eta ,") "
          END IF
          IF condi_codpos IS NOT NULL OR condi_codpos.getLength() > 0 THEN
             LET consulta = consulta CLIPPED, " AND r.codposicion = ", condi_codpos
          END IF
          LET consulta = consulta CLIPPED,
          " AND p.pueid = r.pueposicion AND u.uorid = r.pueuniorg AND a.afuid = u.uorareaf ", 
          " AND (", usuario.ID, " IN (r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut) ",
          --"       OR ", usuario.ID, " IN (r.reqanalista) ",
          " ) ", 
          --" UNION ", 
          --" SELECT  r.reqid, r.reqfecrec, r.reqmotivo, r.reqetapa, r.reqestado, ", 
          --" u.uornombre, a.afunombre, p.puenombre, ", 
          --" r.reqdigitador, r.reqususol, r.requsuaut, r.gerfunaut ", 
          --" FROM reqmreq r, glbuniorga u, glbmareaf a, glbmpue p", 
          --" WHERE r.reqid > 0 ", 
          --" AND r.reqfecrec BETWEEN '", condi_fdl USING "DD/MM/YYYY", "' AND '", condi_fal USING "DD/MM/YYYY", "' ", 
          --" AND p.pueid = r.pueposicion AND u.uorid = r.pueuniorg AND a.afuid = u.uorareaf ", 
          --" AND ( r.gerfunaut IS NULL OR ", usuario.ID, " NOT IN (r.gerfunaut) ) ", 
          --" AND ( r.requsuaut IS NULL OR ", usuario.ID, " NOT IN (r.requsuaut) ) ", 
          --" AND ",usuario.ID, " NOT IN (r.reqdigitador, r.reqususol) ", 
          --" AND r.reqetapa IN ( ", condi_eta ,") ", 
          " ORDER BY 1" 
          DISPLAY "Consulta 4 digitador, solicitante y autorizante =======> ", consulta    
   END CASE
   --DISPLAY "Consulta =======> ", consulta
      
   
   --definir cursor con consulta de BD
   DECLARE curDetR CURSOR FROM consulta
   CALL reg_req.clear()
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDetR INTO rDet.*
      LET x = x + 1 
      LET reg_req[x].* = rDet.*  
   END FOREACH

   
   RETURN x --Cantidad de registros
END FUNCTION
 
FUNCTION consulta_b(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE 
   rDet tLR, x INTEGER,
   consulta STRING,
   vUsuario LIKE reqmbit.bitusuoper,
   vNombre  LIKE commempl.nombre,
   vApellido LIKE commempl.apellido

   IF flagConsulta THEN 
      CALL encabezado("Consulta")

      CONSTRUCT condicion 
         ON bitid, etaidini, accid
         FROM bitid, etaidini, accid

         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
             
      END CONSTRUCT 

   ELSE
      LET condicion = " 1=1 "
   END IF
   --Armar la consulta
   LET consulta = 
      "SELECT  bitid, null, etaidfin, bitfecoper, accid, bitobserva, bitusuoper ",
      " FROM reqmbit ",
      " WHERE reqid = ", gr_reg.reqid,
      " AND ", condicion,
      " ORDER BY 1 DESC "
  
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rDet.*, vUsuario
      LET x = x + 1 
      SELECT nombre, apellido
        INTO vNombre, vApellido
        FROM commempl
       WHERE id_commempl = vUsuario
      LET rDet.usuario_1 = vNombre CLIPPED, ' ', vApellido CLIPPED
      LET reg_det [x].* = rDet.* 
   END FOREACH

   DISPLAY ARRAY reg_det TO sLRB.*
       BEFORE DISPLAY EXIT DISPLAY
   END DISPLAY
   
   RETURN x --Cantidad de registros
END FUNCTION

