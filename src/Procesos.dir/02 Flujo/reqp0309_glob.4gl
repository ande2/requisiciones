DATABASE rh

GLOBALS
TYPE tDG RECORD
    etaidini  LIKE reqmbit.etaidini,
    accid LIKE reqmbit.accid,    
    etaidfin  LIKE reqmbit.etaidfin,
    bitobserva LIKE reqmbit.bitobserva
END RECORD

TYPE tLR RECORD
    bitid_1      LIKE reqmbit.bitid,
    usuario_1    VARCHAR(40),
    etaidini_1   LIKE reqmbit.etaidfin,
    bitfecoper_1 LIKE reqmbit.bitfecoper,
    accid_1      LIKE reqmbit.accid,
    bitobserva_1 LIKE reqmbit.bitobserva
END RECORD

TYPE tDGR RECORD
    reqid       LIKE reqmreq.reqid,
    reqfecrec   LIKE reqmreq.reqfecrec,
    reqmotivo   LIKE reqmreq.reqmotivo,
    reqetapa  LIKE reqmreq.reqetapa,
    reqestado  LIKE reqmreq.reqestado,
    uornombre LIKE glbuniorga.uornombre,
    afunombre LIKE glbmareaf.afunombre,
    puenombre LIKE glbmpue.puenombre,
    reqdigitador LIKE reqmreq.reqdigitador,
    reqususol LIKE reqmreq.reqususol,
    requsuaut LIKE reqmreq.requsuaut,
    gerfunaut LIKE reqmreq.gerfunaut
END RECORD

TYPE tUsuario RECORD
  ID         LIKE commempl.id_commempl, 
  vTipo      SMALLINT,
  vEsNomina  SMALLINT,
  vEsCooRRHH SMALLINT,
  vEsRRHH    SMALLINT,
  vEsCliMed  SMALLINT,
  vDigita    SMALLINT,
  vSolicita  SMALLINT,
  vAutoriza  SMALLINT,
  vAutorFun  SMALLINT,
  vNomina    SMALLINT,
  vCooRRHH   SMALLINT,
  vRRHH      SMALLINT,
  vCliMed    SMALLINT
END RECORD

TYPE tPermisos RECORD
    vSoloVer   SMALLINT,
    vProcesar  SMALLINT,
    vActualiza SMALLINT,
    vReasigna  SMALLINT
END RECORD

TYPE tDatos1 RECORD
    reqsalario  LIKE reqmreq.reqsalario,
    reqcondcomp LIKE reqmreq.reqcondcomp,
    reqhrsext   LIKE reqmreq.reqhrsext,
    reqobs      LIKE reqmreq.reqobs,
    codposicion    LIKE reqmreq.codposicion
END RECORD

TYPE tDatos2 RECORD
    reqanalista  LIKE reqmreq.reqanalista
END RECORD

    DEFINE dbname STRING
    CONSTANT    prog_name = "reqp0309"
    DEFINE usuario tUsuario

    DEFINE reg_det DYNAMIC ARRAY OF tLR
    DEFINE gb_reg                   tLR
    DEFINE u_reg, g_reg             tDG
    DEFINE g_datos1                 tDatos1
    DEFINE g_datos2                 tDatos2

    DEFINE reg_req DYNAMIC ARRAY OF tDGR
    DEFINE ur_reg, gr_reg           tDGR
    
    DEFINE condicion STRING
    DEFINE condi_eta STRING
    DEFINE condi_fdl DATE
    DEFINE condi_fal DATE
    DEFINE condi_codpos STRING 

    DEFINE flujos STRING

    DEFINE permiso tPermisos

DEFINE w           ui.Window,
       f           ui.Form
END GLOBALS