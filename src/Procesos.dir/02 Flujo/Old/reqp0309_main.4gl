IMPORT util 

GLOBALS "reqp0309_glob.4gl"

MAIN
DEFINE i SMALLINT 


   OPEN WINDOW w1 WITH FORM 'reqp0309_form01'

   CALL ui.Interface.loadActionDefaults("actiondefaults_rh1")

      
   CALL combo_init()
   CALL query_init()
   
   DECLARE ccur CURSOR FOR 
      SELECT reqid, reqEmpCon, reqMotivo, pueAreaF, pueUniOrg, puePosicion,    
             pueCco, jefePue,reqEstado
      FROM reqMReq

   LET i = 1
   FOREACH ccur INTO reg_det[i].*
      LET i = i + 1
   END FOREACH 

   DISPLAY ARRAY reg_det TO sDet.*
      ON ACTION detalle
         CALL fdetalle(reg_det[arr_curr()].reqid)
   END DISPLAY 

END MAIN  

FUNCTION query_init()
   DEFINE select_stmt3 CHAR(350)

   LET select_stmt3 = "SELECT reqId, reqFecRec, reqEmpCon, pueLoc, reqMotivo, ", 
               " reqTmpMes, reqVacAnt, reqTipo, ", 
               " pueAreaF, pueUniOrg, puePosicion, pueCco, ", 
               " puenecveh, puehorario, ",
               " jefeAreaF, jefeuniorg, jefepue, jefenom, ", 
               " reqautplatmp, reqDigitador, reqUsuSol, reqUsuAut, gerFunAut, ", 
               " reqEstado, fecHorIngreso ",
               " FROM reqMReq ",
               " WHERE reqId = ? "

   PREPARE ex_stmt3 FROM select_stmt3
END FUNCTION

FUNCTION fdetalle(reqId INTEGER)
DEFINE   lFlagTemp, lFlagVac, lFlagAut CHAR(1)
   DEFINE   win ui.Window,
            fm ui.Form
            
   OPEN WINDOW w2 WITH FORM 'reqp0309_form02'

   CALL combo_init()

   LET win = ui.Window.getCurrent()
   LET fm = win.getForm()

   SELECT flagTemp, flagVac, motReqAut 
      INTO lFlagTemp, lFlagVac, lFlagAut
      FROM glbMMot
      WHERE motId = g_reg.reqMotivo
   --vacante
   IF lFlagVac = '1' THEN 
      CALL fm.setElementHidden("reqmreq_reqvacant_label1", 0)
      CALL fm.setFieldHidden("reqvacant",0)
   ELSE 
      CALL fm.setElementHidden("reqmreq_reqvacant_label1", 1)
      CALL fm.setFieldHidden("reqvacant",1)
   END IF 
   --temporal
   IF lFlagTemp = '1' THEN 
      CALL fm.setElementHidden("reqmreq_reqtmpmes_label1", 0)
      CALL fm.setFieldHidden("reqtmpmes",0)
   ELSE 
      CALL fm.setElementHidden("reqmreq_reqtmpmes_label1", 1)
      CALL fm.setFieldHidden("reqtmpmes",1)
   END IF
   --Requiere Autorizacion
   IF lFlagAut = '1' THEN 
      CALL fm.setElementHidden("reqmreq_requsuaut_label1", 0)
      CALL fm.setFieldHidden("gerFunAut",0)
   ELSE
      CALL fm.setElementHidden("reqmreq_requsuaut_label1", 1)
      CALL fm.setFieldHidden("gerFunAut",1)
   END IF 
   
   DECLARE cur_all CURSOR FOR ex_stmt3
   OPEN cur_all USING reqId
   FETCH cur_all INTO g_reg.*
   CALL display_reg()
   MENU
      ON ACTION verdoc
         CALL display_doc('D')
      ON ACTION salir
         EXIT MENU 
   END MENU 
   CLOSE WINDOW w2

END FUNCTION 

FUNCTION display_reg()
DEFINE codformato STRING

   --Código de requerimiento
   SELECT valChr INTO codFormato FROM glb_paramtrs WHERE numPar=2 AND tipPar=1
   DISPLAY BY NAME codFormato
   
   DISPLAY BY NAME g_reg.*
   DISPLAY util.Datetime.format(g_reg.fecHorIngreso, "%Y-%m-%d %H:%M:%S") TO fecHorIngreso 
   CALL display_doc('R')

END FUNCTION 

FUNCTION display_doc(accion CHAR(1))
   --R = Como parte del display Registro
   --D = Accion ver Documento
   --E = Para Eliminar el documento
   DEFINE x SMALLINT
   DEFINE result INTEGER  
   DEFINE nombre STRING

   DECLARE rdoclist CURSOR FOR SELECT rdaId, rdaNom FROM reqDDoc WHERE reqId = g_reg.reqId
   CALL a_doc.clear()
   LET x = 1
   FOREACH rdoclist INTO a_doc[x].rdaId, a_doc[x].rdaNom
      LET x = x + 1
   END FOREACH
   LET x = x - 1
   LET numdocs = x
    
   DISPLAY ARRAY a_doc TO sa_doc.*
      ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
      BEFORE DISPLAY 
         --DISPLAY "accion lleva ------------> ", accion 
         IF accion = 'R' THEN 
            EXIT DISPLAY
         END IF 

      ON ACTION regresar
         EXIT DISPLAY 
         
      ON ACTION mostrar
         LOCATE g_doc.rdaDoc IN FILE './files/'||nospace(a_doc[arr_curr()].rdaNom)
         SELECT rdaDoc INTO g_doc.rdaDoc FROM reqDDoc WHERE reqId = g_reg.reqId AND rdaId = a_doc[arr_curr()].rdaId
         CALL fgl_putfile('./files/'||nospace(a_doc[arr_curr()].rdaNom), a_doc[arr_curr()].rdaNom)
         CALL ui.interface.frontcall("standard","shellexec",[a_doc[arr_curr()].rdaNom],[result])
          
   END DISPLAY
   
END FUNCTION 

FUNCTION nospace(texto STRING )
   DEFINE buf base.StringBuffer
   LET buf = base.StringBuffer.create()
   CALL buf.append(texto)
   CALL buf.replace(" ", "_", 0)
   RETURN buf.toString()
END FUNCTION

FUNCTION combo_init()
   --Empresa contratante
   CALL combo_din2("reqempcon","select id_commemp, trim(iniciales) from commemp WHERE estado=1 ORDER BY 2 ")
   --Locación / Ubicación
   CALL combo_din2("pueLoc","select locId, trim(locNombre) from glbMLoc ORDER BY 2")
   --Motivo
   CALL combo_din2("reqMotivo","select motId, trim(motNombre) from glbMMot ORDER BY 2")
   --Area Funcional del Puesto
   CALL combo_din2("pueAreaF", "SELECT afuId, trim(afuNombre) FROM glbMAreaF ORDER BY 2")
   --Unidad Organizativa
   CALL combo_din2("pueUniOrg","select uorId, trim(uorNombre) from glbUniOrga ORDER BY 2")
   --Posición / Puesto
   CALL combo_din2("puePosicion","select pueId, trim(pueNombre) from glbmpue ORDER BY 2")
   --Centro de Costo
   CALL combo_din2("pueCco","select ccoId, trim(ccoNombre) from glbCco ORDER BY 2")
   --Area Funcional del Puesto
   CALL combo_din2("jefeAreaF", "SELECT afuId, trim(afuNombre) FROM glbMAreaF ORDER BY 2")
   --Digitador
   CALL combo_din2("reqDigitador", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl ORDER BY 2")
   --Solicitante
   CALL combo_din2("reqUsuSol", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esSolicitante = '1' ORDER BY 2" )
   --Autorizante
   CALL combo_din2("reqUsuAut", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esAutorizante = '1' ORDER BY 2" )
   --Gerencia funcional que autoriza
   CALL combo_din2("gerFunAut", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esGerAreaF = '1' ORDER BY 2")
   --Jefe
   CALL combo_din2("jefePue",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --Estado
   --CALL combo_din2("reqEstado", "SELECT id_commempl, trim(nombre)||' '||trim(apellido) FROM comMEmpl WHERE esGerAreaF = '1' ORDER BY 2")
   --CALL combo_din2("cmbccoUniOrga","select uorId, trim(uorNombre) from glbUniOrga")
END FUNCTION    