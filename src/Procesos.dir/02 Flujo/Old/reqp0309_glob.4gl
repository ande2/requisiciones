DATABASE rh

GLOBALS

   TYPE tDet RECORD
      reqid             LIKE reqMReq.reqid,
      reqEmpCon         LIKE reqMReq.reqEmpCon,
      reqMotivo         LIKE reqMReq.reqMotivo,
      pueAreaF          LIKE reqMReq.pueAreaF,
      pueUniOrg         LIKE reqMReq.pueUniOrg,
      puePosicion       LIKE reqMReq.puePosicion,
      pueCco            LIKE reqMReq.pueCco,
      jefePue           LIKE reqMReq.jefePue,
      reqEstado         LIKE reqMReq.reqEstado
   END RECORD 

   TYPE tDoc RECORD 
      reqId          LIKE reqmReq.reqId,
      reqFecRec      LIKE reqmReq.reqFecRec,
      reqEmpCon      LIKE reqmReq.reqEmpCon,
      pueLoc         LIKE reqmReq.pueLoc,
      reqMotivo      LIKE reqmReq.reqMotivo,
      reqTmpMes      LIKE reqmReq.reqTmpMes,
      reqVacAnt      LIKE reqmReq.reqVacAnt,
      reqTipo        LIKE reqmReq.reqTipo,
      
      pueAreaF       LIKE reqmReq.pueAreaF,
      pueUniOrg      LIKE reqmReq.pueUniOrg,
      puePosicion    LIKE reqmReq.puePosicion,
      pueCco         LIKE reqmReq.pueCco,
      puenecveh      LIKE reqmReq.puenecveh, 
      puehorario     LIKE reqmReq.puehorario,
      
      jefeAreaF      LIKE reqmReq.jefeAreaF,
      jefeuniorg     LIKE reqmReq.jefeuniorg,
      jefepue        LIKE reqmReq.jefepue,
      jefenom        LIKE reqmReq.jefenom,

      reqautplatmp   LIKE reqmReq.reqautplatmp,

      reqDigitador   LIKE reqMReq.reqdigitador,
      reqUsuSol      LIKE reqMReq.reqususol,
      reqUsuAut      LIKE reqMReq.requsuaut,
      gerFunAut      LIKE reqMReq.gerfunaut,
      
      reqEstado      LIKE reqmReq.reqEstado,
      fecHorIngreso  LIKE reqmReq.fecHorIngreso    
      
   END RECORD

   TYPE t_doc RECORD
      reqId          LIKE reqDDoc.reqid,
      rdaId          LIKE reqDDoc.rdaid,
      rdaNom         LIKE reqDDoc.rdanom,
      rdaDoc         LIKE reqDDoc.rdadoc
   END RECORD 
   
   DEFINE reg_det DYNAMIC ARRAY OF tDet
   DEFINE g_reg   tDoc 

   DEFINE a_doc DYNAMIC ARRAY OF t_doc
   DEFINE g_doc    t_doc

   DEFINE numdocs    SMALLINT
   
END GLOBALS 