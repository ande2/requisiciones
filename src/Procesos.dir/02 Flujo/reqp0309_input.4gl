
GLOBALS "reqp0309_glob.4gl"
  
FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE flujos   SMALLINT 
DEFINE touchDet SMALLINT 
DEFINE infoadd  LIKE reqmeta.etaagregainfo

LET w = ui.Window.getCurrent()
LET f = w.getForm()

   LET resultado = FALSE 
   LET flujos = flujo_lista()
   IF flujos > 0 THEN
       LET u_reg.* = g_reg.*
       CALL combo_init_input()
       CALL combo_accion()
       IF operacion = 'I' OR operacion = 'R' THEN 
          
          INITIALIZE g_reg.* TO NULL
          LET g_reg.etaidini = gr_reg.reqetapa
          DISPLAY BY NAME g_reg.*
          --CALL cleanArrayItems()
          
       END IF

       INITIALIZE g_datos1.* TO NULL
       INITIALIZE g_datos2.* TO NULL
       
       CASE operacion
          WHEN 'I'
             CALL f.setElementHidden("gridInput",0) -- Motrar
             CALL f.setElementHidden("gridListReq",1) -- Ocultar
             CALL f.setElementHidden("group1",0)
             CALL f.setElementHidden("grpDatos1",1) -- Ocultar
             CALL f.setElementHidden("grpDatos2",1) -- Ocultar

          WHEN 'R'
             CALL f.setElementHidden("gridInput",0) -- Motrar
             CALL f.setElementHidden("gridListReq",1) -- Ocultar
             CALL f.setElementHidden("group1",1)
             CALL f.setElementHidden("grpDatos1",1) -- Ocultar
             CALL f.setElementHidden("grpDatos2",0) -- Ocultar
             
             SELECT reqanalista
               INTO g_datos2.reqanalista
               FROM reqmreq
              WHERE reqid = gr_reg.reqid
              
       END CASE

       DIALOG ATTRIBUTES(UNBUFFERED)
         INPUT BY NAME  
          g_reg.accid, g_reg.bitobserva, g_datos1.reqsalario, g_datos1.reqcondcomp, 
                       g_datos1.reqhrsext, g_datos1.reqobs, g_datos1.codposicion,
                       g_datos2.reqanalista
          ATTRIBUTES (WITHOUT DEFAULTS)
           
          BEFORE INPUT
             CALL DIALOG.setActionHidden("close",TRUE)


          ON CHANGE accid
             IF combo_etaidfin(g_reg.accid) THEN
                DISPLAY BY NAME g_reg.etaidfin

                LET infoadd = 0
                SELECT etaagregainfo INTO infoadd
                  FROM reqmeta
                 WHERE etaid = g_reg.etaidfin
                IF infoadd > 0 THEN
                    CASE infoadd
                      WHEN 1 
                         CALL f.setElementHidden("grpDatos1",0)
                         CALL f.setElementHidden("grpDatos2",1)
                      WHEN 2
                         CALL f.setElementHidden("grpDatos1",1)
                         CALL f.setElementHidden("grpDatos2",0)
                    END CASE
                ELSE
                    CALL f.setElementHidden("grpDatos1",1)
                    CALL f.setElementHidden("grpDatos2",1)
                END IF
             ELSE
                LET g_reg.etaidfin = NULL
                DISPLAY BY NAME g_reg.etaidfin
             END If
          
       END INPUT 
       
       ON ACTION ACCEPT

          IF operacion = 'M' AND g_reg.* = u_reg.* THEN
             IF touchDet <> 1 THEN 
                CALL msg("No se efectuaron cambios")
                EXIT DIALOG 
             END IF 
          END IF
          
          CASE box_gradato("Seguro de grabar")
             WHEN "Si"
                LET resultado = TRUE
                EXIT DIALOG
             WHEN "No"
                EXIT DIALOG 
             OTHERWISE
                CONTINUE DIALOG 
          END CASE 
          LET resultado = TRUE
          EXIT DIALOG 

         ON ACTION CANCEL
            EXIT DIALOG
            
       END DIALOG
       
       IF NOT resultado THEN
          LET g_reg.* = u_reg.*
          DISPLAY BY NAME g_reg.* 
       END IF 

       CALL f.setElementHidden("gridInput",1)
       CALL f.setElementHidden("gridListReq",0)

   ELSE
       CALL msgError(1,"No se puede procesar esta requisición. Es prosible que ya finalizó o no exista flujo siguiente.")
   END IF
   
RETURN resultado 
END FUNCTION

FUNCTION flujo_lista()
DEFINE id    LIKE reqmflu.fluid
DEFINE i, j  SMALLINT
DEFINE flujo DYNAMIC ARRAY OF RECORD
    id   LIKE reqmflu.fluid
END RECORD
    
    CALL flujo.clear()
    LET flujos = ' '
    
    LET i = 0

    DECLARE cur_flujo1 CURSOR FOR
        SELECT fluid
          FROM reqmflu
         WHERE etaid = gr_reg.reqetapa
           AND flutiporeq = '1'
           AND motid = gr_reg.reqmotivo
    FOREACH cur_flujo1 INTO id
       LET i = i + 1
       LET flujo[i].id = id
    END FOREACH

    DECLARE cur_flujo2 CURSOR FOR
        SELECT fluid
          FROM reqmflu
         WHERE etaid = gr_reg.reqetapa
           AND flutiporeq = '0'
           AND motid IS NULL
    FOREACH cur_flujo2 INTO id
       LET i = i + 1
       LET flujo[i].id = id
    END FOREACH

    FOR j = 1 TO i
        LET flujos = flujos, flujo[j].id
        IF j < i THEN
            LET flujos = flujos,','
        END IF
    END FOR
    
RETURN i
END FUNCTION

FUNCTION combo_init_input()
   CALL combo_din2("etaidini","SELECT etaid, etanombre FROM reqmeta ORDER BY etanombre ")
   CALL combo_din2("etaidfin","SELECT etaid, etanombre FROM reqmeta ORDER BY etanombre")
END FUNCTION 

FUNCTION combo_accion()
DEFINE consulta    STRING

   LET consulta = ' SELECT accid, accnombre FROM reqmacc ',
                  ' WHERE accid IN ( ',
                  ' SELECT DISTINCT accid FROM reqmflu WHERE fluid IN (',flujos,') ',
                  ' ) ',
                  ' ORDER BY accnombre'
   CALL combo_din2("accid",consulta)
END FUNCTION


FUNCTION combo_etaidfin(accion)
DEFINE accion    LIKE reqmacc.accid
DEFINE etapa     LIKE reqmeta.etaid
DEFINE resultado SMALLINT

    LET resultado = FALSE 
    
    SELECT flusigetapa INTO etapa
      FROM reqmflu
     WHERE etaid = g_reg.etaidini
       AND accid = accion
       AND flutiporeq = '1'
       AND motid = gr_reg.reqmotivo

    IF sqlca.sqlcode = NOTFOUND THEN
    
        SELECT flusigetapa INTO etapa
          FROM reqmflu
         WHERE etaid = g_reg.etaidini
           AND accid = accion
           AND flutiporeq = '0'
           AND motid IS NULL
           
        IF sqlca.sqlcode <> NOTFOUND THEN
            LET g_reg.etaidfin = etapa
            LET resultado = TRUE
        END IF
    ELSE
        LET g_reg.etaidfin = etapa
        LET resultado = TRUE
    END IF
    
RETURN resultado
END FUNCTION