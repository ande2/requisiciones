IMPORT FGL req_combos

GLOBALS "reqp0312_glob.4gl"

MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING

	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	LET n_param = num_args()
    IF n_param > 0 THEN
        LET usuario = ARG_VAL(1)
        IF n_param > 1 THEN
            LET requi = ARG_VAL(2)
        END IF

        LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
        
        CALL STARTLOG(prog_name2)

        CALL insert_init()
        CALL update_init()
        CALL delete_init()
        CALL combo_init()
        CALL tablas_temp()
        CALL main_init()
    END IF
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING

    CALL ui.Interface.loadActionDefaults("actiondefaults_rh1")

    CLOSE WINDOW SCREEN 

    LET nom_forma = "reqp0312_form"
	 OPEN WINDOW w1 WITH FORM nom_forma
    CALL fgl_settitle("ANDE - Candidatos")
    CALL ui.Window.getCurrent().setText("ANDE - Candidatos - "||nomUsuario(usuario))

	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()

    CALL main_menu()
END FUNCTION

FUNCTION main_menu()
{
 arg_val(1) = usuario - Id del usuario
 arg_val(2) = requi   - Número de requisición
}
DEFINE varg1 CHAR (1),
    cuantos     SMALLINT,
    contratar   SMALLINT,
    id, ids     SMALLINT,
    opcion      CHAR(1),
    salir       SMALLINT,
    txt_agregar STRING,
    txt_envio   STRING,
    runcmd      STRING ,
    solicitante LIKE reqmreq.reqususol,
    lcanfecprocita DATE
    
    
    LET varg1 = arg_val(1)

    LET salir = FALSE
    CALL f.setElementHidden("gridInput",1)

    IF requi > 0 THEN
        LET txt_agregar = 'Agregar'
        DISPLAY "REGISTRO DE CANDIDATOS" TO gtit_enc
    ELSE
        LET txt_agregar = 'Listado'
        DISPLAY "REGISTRO DE CANDIDATOS SIN REQUISICIÓN ASOCIADA" TO gtit_enc
    END IF
    LET txt_envio = 'Seguimiento'

    WHILE NOT salir

        LET lessolicita = '0'
        IF requi > 0 THEN
           LET solicitante = NULL
           
           SELECT reqususol INTO solicitante 
             FROM reqmreq
            WHERE reqid = requi
            
           IF usuario = solicitante  THEN 
              LET lessolicita = '1'
           END IF
        END IF

        DISPLAY ARRAY reg_det TO sLR.*
           ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
           {
           ESTADOS DEL CANDIDATO
           A = Ingresado - Automatico, al agregar al candidato
           TRANSITORIOS:
           B = En Investigación
           C = En Entrevista
           D = Aprobado por Entrevista
           E = En Evaluación Médica
           F = Aprobado en Clínica Médica
           G = Proceso de Contratación
           ESTADO FINAL SELECCIONADO PARA CONTRATAR
           S = Contratado
           ESTADO FINAL POR RECHAZO
           X = Rechazado Analista
           Y = Rechazado Clínica
           Z = Rechazado Entrevista
           }
          BEFORE DISPLAY
             CALL DIALOG.setActionActive("Propuesta",0) 
             IF disp_requi(requi) THEN --Despliega datos resumen de la requi
                 LET cuantos = consulta(FALSE) --Llena detalle de candidatos
                 IF cuantos = 0 THEN 
                    CALL DIALOG.setActionActive("Ver", 0)
                    CALL DIALOG.setActionActive("Modificar", 0)
                    CALL DIALOG.setActionActive("Eliminar", 0)
                 ELSE
                    LET contratar = btnContrata()
                    CALL DIALOG.setActionActive("Contratar",contratar)
                 END IF 
                 IF requi IS NULL THEN
                    CALL DIALOG.setActionActive("Agregar",1) 
                 END IF
                 CALL encabezado("")
             ELSE
                CALL DIALOG.setActionActive("Ver", 0)
                CALL DIALOG.setActionActive("Modificar", 0)
                CALL DIALOG.setActionActive("Eliminar", 0)
                CALL DIALOG.setActionActive("Agregar", 0)
                CALL DIALOG.setActionActive("Nuevo", 0)
             END IF

             SELECT escoorrhh, esrrhh INTO lescoorrhh, lesrrhh FROM commempl 
               WHERE id_commempl = usuario
             IF lescoorrhh = '1' OR lesrrhh = '1' THEN 

             ELSE --Si no es CooRRHH o RRHH, entonces es el Solicitante 
                  CALL DIALOG.setActionActive("Nuevo", 0)
                  CALL DIALOG.setActionActive("Agregar", 0)
                  CALL DIALOG.setActionActive("Modificar", 0)
                  CALL DIALOG.setActionActive("Eliminar", 0)
                  --CALL DIALOG.setActionActive("Enviar", 0)
             END IF   
             
    
          BEFORE ROW 
             CALL DIALOG.setActionActive("Propuesta", 0)
             LET id = arr_curr()
             
             IF id > 0 THEN 
                LET l_reg.* = reg_det[id].*
                SELECT escoorrhh, esrrhh INTO lescoorrhh, lesrrhh FROM commempl 
                  WHERE id_commempl = usuario
                IF lescoorrhh = '1' OR lesrrhh = '1' THEN 
                   IF reg_det[id].canestado = 'G' THEN 
                      CALL DIALOG.setActionActive("Propuesta", 1)
                   END IF    
                   IF reg_det[id].canestado = "C" OR reg_det[id].canestado = "E" THEN
                      IF lessolicita <> '1' THEN 
                        CALL DIALOG.setActionActive("Enviar", 0) --seguimiento
                      END IF 
                   ELSE
                      --CALL DIALOG.setActionActive("Propuesta", 0)
                      IF reg_det[id].canestado = "A" OR reg_det[id].canestado = "B" OR
                         reg_det[id].canestado = "D" OR reg_det[id].canestado = "F" OR
                         reg_det[id].canestado = "G"
                      THEN
                          CALL DIALOG.setActionActive("Enviar", 1)
                      ELSE
                          CALL DIALOG.setActionActive("Enviar", 0)
                      END IF
                   END IF
                ELSE
                   IF reg_det[id].canestado = "C" THEN
                      CALL DIALOG.setActionActive("Enviar", 1)
                      CALL DIALOG.setActionActive("Contratar",contratar) --seleccionar
                   ELSE
                      CALL DIALOG.setActionActive("Enviar", 0)
                      CALL DIALOG.setActionActive("Contratar",0)
                   END IF
                END IF
             END IF 
             SELECT canfecprocita INTO lcanfecprocita FROM reqmcan WHERE canid = reg_det[id].canid
             IF reg_det[id].canestado = 'C' AND lcanfecprocita IS NOT NULL THEN 
                CALL DIALOG.setActionActive("Contratar", 1)
             ELSE 
                CALL DIALOG.setActionActive("Contratar", 0)
             END IF
          ON ACTION Nuevo
               LET opcion = "I"
               EXIT DISPLAY

          ON ACTION Agregar  ATTRIBUTES(TEXT=txt_agregar)
               LET opcion = "A"
               EXIT DISPLAY

          ON ACTION Ver
           LET opcion = "V"
             LET id = arr_curr()
             LET ids = scr_line()
             EXIT DISPLAY
          
          ON ACTION Modificar
             LET opcion = "M"
             LET id = arr_curr()
             LET ids = scr_line()
             EXIT DISPLAY
    
          ON ACTION Eliminar
               LET opcion = "E"
               LET id = arr_curr()
               LET ids = scr_line()         
               EXIT DISPLAY

          ON ACTION Enviar ATTRIBUTES(TEXT=txt_envio)
             LET opcion = "P"
             LET id = arr_curr()
             LET ids = scr_line()
             EXIT DISPLAY

          ON ACTION Contratar
             LET opcion = "C"
             LET id = arr_curr()
             LET ids = scr_line()
             EXIT DISPLAY

           ON ACTION Propuesta
             LET opcion = "L"
             LET id = arr_curr()
             LET ids = scr_line()
             EXIT DISPLAY
             
           ON ACTION Salir
              LET opcion = 'S'
              EXIT DISPLAY 
        END DISPLAY 

        CASE opcion
          WHEN "I" --NUEVO
             IF ingreso(opcion) THEN          
                LET cuantos = consulta(FALSE)
                CALL fgl_set_arr_curr( arr_count() + 1 )
                --Refrescar Pantalla
                --DISPLAY ARRAY reg_det TO sDG.*
                --   BEFORE DISPLAY  EXIT DISPLAY 
                --END DISPLAY            
             END IF
             CALL encabezado("")

             
          WHEN "A" --AGREGAR DESDE LA BD DE CONTACTOS
             IF agregar(opcion) THEN          
                LET cuantos = consulta(FALSE)
                CALL fgl_set_arr_curr( arr_count() + 1 )        
             END IF
             CALL encabezado("")
    
          WHEN "M" --MODIFICAR
               DISPLAY "MODIFICA"
             IF id > 0 THEN 
                IF modifica(opcion) THEN
                   LET reg_det[id].* = l_reg.*
                END IF   
             END IF 
             CALL encabezado("")

          WHEN "P" --ENVIAR
             DISPLAY "MODIFICA"
             IF id > 0 THEN 
                IF modifica(opcion) THEN
                   LET reg_det[id].* = l_reg.*
                END IF   
             END IF 
             CALL encabezado("")

          WHEN "C" --CONTRATAR
             DISPLAY "MODIFICA"
             IF id > 0 THEN 
                IF modifica(opcion) THEN
                   LET reg_det[id].* = l_reg.*
                END IF   
             END IF 
             CALL encabezado("")
             
           WHEN "V" --VER
              IF captura_datos(opcion) THEN
                  CALL encabezado("")
              END IF
           
            WHEN "E" --ELIMINAR
                  IF id > 0 THEN 
                    IF eliminar() THEN
                       IF id = arr_count() THEN 
                          LET id = id - 1
                       END IF 
                       IF id > 0 THEN
                          LET l_reg.* = reg_det[id].*
                       ELSE 
                          INITIALIZE l_reg.* TO NULL
                       END IF 
                       DISPLAY BY NAME l_reg.*
        
                    END IF   
                 END IF 

            WHEN "L" --PROPUESTA
             DISPLAY "Reporte ", reg_det[id].canid
             LET runcmd = "fglrun Propuesta_laboral.42r ", reg_det[id].canid
             RUN runcmd 
             CALL encabezado("")
                 
            WHEN "S"
                LET salir = TRUE
        END CASE 
    END WHILE
END FUNCTION

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 

   DISPLAY BY NAME  gtit_enc
END FUNCTION 

FUNCTION combo_init()
   CALL combo_din2("trcid","SELECT trcid, trcnombre FROM reqmtrc ORDER BY trcnombre ")
   CALL combo_din2("trechid","SELECT trechid, trechnombre FROM reqmtrech ORDER BY trechnombre")
   CALL combo_din2("reqmcan.relgid",'SELECT relgid, relgnombre FROM reqrelig ORDER BY relgnombre')
   CALL combo_din2("cantdid",'SELECT cantdid, cantdnombre FROM reqmcantd ORDER BY cantdnombre')
   CALL combo_din2("trechid_h","SELECT trechid, trechnombre FROM reqmtrech ORDER BY trechnombre")
END FUNCTION 

FUNCTION combo_init_act()
   CALL combo_din2("trcid","SELECT trcid, trcnombre FROM reqmtrc WHERE trcactivo = 'S' ")
END FUNCTION 

FUNCTION disp_requi(num_req)
DEFINE num_req LIKE reqmreq.reqid
DEFINE req     RECORD LIKE reqmreq.*
DEFINE uor     RECORD LIKE glbuniorga.*
DEFINE afu     RECORD LIKE glbmareaf.*
DEFINE pue     RECORD LIKE glbmpue.*
DEFINE eta     RECORD LIKE reqmeta.*

     IF num_req > 0 THEN
         SELECT *
           INTO req.*
          FROM reqmreq
          WHERE reqid = num_req

        IF req.reqestado IS NOT NULL THEN

            LET reqetapa = req.reqetapa
            SELECT *
              INTO eta.*
              FROM reqmeta
             WHERE etaid = reqetapa

            IF eta.etaagregainfo = 2 THEN
                IF usuario = req.reqanalista THEN
                    LET usuanalis = TRUE
                ELSE
                    LET usuanalis = FALSE
                END IF
            END IF

            SELECT *
              INTO uor.*
              FROM glbuniorga
             WHERE uorid = req.pueuniorg

            SELECT * 
              INTO afu.*
              FROM glbmareaf
             WHERE afuid = uor.uorareaf

            SELECT *
              INTO pue.*
              FROM glbmpue
             WHERE pueid = req.pueposicion

            DISPLAY BY NAME req.reqid, req.reqfecrec, uor.uornombre,
                            afu.afunombre, pue.puenombre, req.reqestado,
                            req.jefenom
            RETURN TRUE
        ELSE
            LET uor.uornombre = '*** REQUISICION NO ENCONTRADA***'
             DISPLAY BY NAME req.reqid, uor.uornombre
             RETURN FALSE
        END IF
    ELSE
        LET requi = NULL
        LET w = ui.Window.getCurrent()
        LET f = w.getForm()

        CALL f.setElementHidden("groupRequi",1) -- 1 Ocultar  
        RETURN TRUE
    END IF
      
END FUNCTION

FUNCTION def_hora(fecha, hora)
DEFINE diahora  LIKE reqmcan.canfecprochora
DEFINE hora_str STRING
DEFINE max_str  SMALLINT
DEFINE dt2 DATETIME YEAR TO MINUTE
DEFINE dt3 CHAR(50)
DEFINE dt4 CHAR(16)
DEFINE fecha    LIKE reqmcan.canfecprocita
DEFINE hora     LIKE reqmcan.canfecprochora

    LET hora_str = hora
    LET max_str  = hora_str.getLength()
    LET dt3 = hora_str
    LET dt4 = fecha USING "YYYY-MM-DD", dt3[11,max_str]
    LET dt2 = dt4
    LET diahora = dt2

RETURN diahora
END FUNCTION


FUNCTION tablas_temp()

    -- TABLA PARA DOCTOS TEMPORALES
    CREATE TEMP TABLE tmp_reqmcandoc
    (
         cdocid SMALLINT,
         cdocfecing DATE,
         cantdid INTEGER,
         cdocobserv VARCHAR(255),
         cdocarchdir VARCHAR(255),
         cdocarchiv BYTE
    )
 
END FUNCTION


FUNCTION cmb_canestado_1(cb)
DEFINE cb     ui.ComboBox
DEFINE cmb_dat tComboTex
DEFINE i SMALLINT

   CALL cmb_canestado_init() RETURNING cmb_dat.*
    
   CALL cb.clear()
   FOR i = 1 TO cmb_dat.cantidad
       CALL cb.addItem(cmb_dat.datos[i].id,cmb_dat.datos[i].nombre)
   END FOR
END FUNCTION

FUNCTION cmb_reqestado(cb)
DEFINE cb     ui.ComboBox
DEFINE cmb_dat tComboTex
DEFINE i SMALLINT

   CALL cmb_reqestado_init() RETURNING cmb_dat.*
    
   CALL cb.clear()
   FOR i = 1 TO cmb_dat.cantidad
       CALL cb.addItem(cmb_dat.datos[i].id,cmb_dat.datos[i].nombre)
   END FOR
END FUNCTION

FUNCTION btnContrata()
DEFINE habiliar  SMALLINT
DEFINE cantCand    SMALLINT
DEFINE cantFinal   SMALLINT
DEFINE cantPendi   SMALLINT
DEFINE cantSelec   SMALLINT
DEFINE cantSinCita SMALLINT
DEFINE solicitante LIKE reqmreq.reqususol

    LET cantCand = 0
    LET cantFinal = 0
    LET cantPendi = 0
    LET cantSelec = 0
    LET cantSinCita = 0
    
    LET habiliar = FALSE

    SELECT reqususol INTO solicitante
      FROM reqmreq
     WHERE reqid = requi

    IF usuario = solicitante THEN

        SELECT COUNT(*) INTO cantCand
          FROM reqmcan
         WHERE reqid = requi

        --SELECT COUNT(*) INTO cantSinCita
        --      FROM reqmcan
        --     WHERE reqid = requi
        --       AND canestado IN ("C")
        --       AND canfecprochora IS NULL

        IF cantCand > 0 THEN
            SELECT COUNT(*) INTO cantFinal
              FROM reqmcan
             WHERE reqid = requi
               AND canestado IN ("C","X","Y","Z")

            SELECT COUNT(*) INTO cantPendi
              FROM reqmcan
             WHERE reqid = requi
               AND canestado IN ("A","B","C","D")

            SELECT COUNT(*) INTO cantSelec
              FROM reqmcan
             WHERE reqid = requi
               AND canestado IN ("F","S")

            IF cantFinal IS NULL THEN LET cantFinal = 0 END IF
            IF cantPendi IS NULL THEN LET cantPendi = 0 END IF
            IF cantSelec IS NULL THEN LET cantSelec = 0 END IF

            IF cantFinal > 0 THEN
                LET habiliar = TRUE
            END IF
        END IF
       
    END IF
RETURN habiliar
END FUNCTION