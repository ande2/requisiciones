IMPORT util
IMPORT FGL req_combos

GLOBALS "reqp0312_glob.4gl"
 
FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE 
   rDet tRL,
   rReg  RECORD LIKE reqmcan.*,
   cita  CHAR(25),
   x,i,vcon INTEGER,
   consulta STRING  

   IF flagConsulta THEN 
      CALL encabezado("Consulta")

      CONSTRUCT condicion 
         ON canid, canestado
         FROM canid, canestado

         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
             
      END CONSTRUCT 

   ELSE
      LET condicion = " 1=1 "
   END IF
   --Armar la consulta
   IF requi > 0 THEN
      LET consulta = 
          "SELECT  * ",
          " FROM reqmcan ",
          " WHERE reqid = ", requi,
          " AND canid > 0 AND ", condicion,
          " ORDER BY 1"
   ELSE
      LET consulta = 
          "SELECT  * ",
          " FROM reqmcan ",
          " WHERE reqid IS NULL ",
          " AND canid > 0 AND ", condicion,
          " ORDER BY 1"   
   END IF
   
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rReg.*
      LET cita = rReg.canfecprochora
      LET rDet.canid = rReg.canid
      LET rDet.nombre = rReg.cannombre1 CLIPPED, ' ', rReg.cannombre2 CLIPPED, ' ',
                        rReg.canapellido1 CLIPPED, ' ', rReg.canapellido2 CLIPPED
      LET rDet.cita   = rReg.canfecprocita USING "DD/MM/YYYY", cita[11,16]
      LET rDet.canestado = rReg.canestado
      LET rDet.canproccant = rReg.canproccant
      LET x = x + 1 
      LET reg_det [x].* = rDet.*  
   END FOREACH

   
   RETURN x --Cantidad de registros
END FUNCTION


FUNCTION consulta_d(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE usu          RECORD LIKE commempl.*
DEFINE condi        STRING
DEFINE 
   rDet_d tRL_d,
   x,i,vcon INTEGER,
   consulta STRING,
   cantdid_1  LIKE reqmcantd.cantdid
   
   LET x = 0

       IF flagConsulta THEN 
          CALL encabezado("Consulta")

          CONSTRUCT condicion 
             ON cdocid, cantdid, cdocobserv
             FROM cdocid, cantdid, cdocobserv

             ON ACTION ACCEPT
                EXIT CONSTRUCT 
             ON ACTION CANCEL 
                CALL reg_det_d.clear()
                RETURN 0
                 
          END CONSTRUCT 

       ELSE
          LET condicion = ' 1=1 '
       END IF
       --Armar la consulta

       SELECT * INTO usu.*
         FROM commempl
        WHERE id_commempl = usuario

        IF usu.esrrhh = '1' THEN
            LET condi = ' 1 = 1 '
        ELSE
           LET condi = ' cantdpriv = "N" '
        END IF

    --- FALTA poner como reconocer al usuario restringido
       IF g_reg.canid_1 IS NOT NULL THEN
           LET consulta = 
              "SELECT  cdocid, cdocfecing, cantdid, cdocobserv , cdocarchdir  ",
              " FROM reqmcandoc ",
              " WHERE canid = ", g_reg.canid_1,
              " AND cdocid > 0 AND ", condicion,
              " AND cantdid IN ( ",
                  " SELECT cantdid ",
                  "   FROM reqmcantd ",
                  "  WHERE ", condi, ") ",
              " ORDER BY 1"
       ELSE
           LET consulta = 
              "SELECT  cdocid, cdocfecing, cantdid, cdocobserv , cdocarchdir  ",
              " FROM tmp_reqmcandoc ",
              " WHERE cantdid IN ( ",
                  " SELECT cantdid ",
                  "   FROM reqmcantd ",
                  "  WHERE ", condi, ") ",
              " ORDER BY 1"
       END IF
       
       --definir cursor con consulta de BD
       DECLARE curDet_d CURSOR FROM consulta
       CALL reg_det_d.clear()
       --Llenar el arreglo con el resultado de la consulta
       FOREACH curDet_d INTO rDet_d.cdocid, rDet_d.cdocfecing, rDet_d.cantdid, 
                             rDet_d.cdocobserv, rDet_d.cdocarchdir
          LET x = x + 1 
          SELECT cantdnombre INTO rDet_d.cantdnombre
            FROM reqmcantd
           WHERE cantdid = rDet_d.cantdid
          LET reg_det_d[x].* = rDet_d.*  
       END FOREACH

   
   RETURN x --Cantidad de registros
END FUNCTION


FUNCTION consulta_h(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE fecha        LIKE reqdcan.rdccanfecoper
DEFINE 
   rDet_h tRL_h,
   x INTEGER,
   consulta STRING
   
   LET x = 0

   IF  g_reg.canid_1 > 0 THEN
       LET consulta = 
          "SELECT  null, canproccant, canestado, reqid, ",
                  " reqestado, trechid, rdccanfecoper  ",
          " FROM reqdcan ",
          " WHERE canid = ", g_reg.canid_1,
          " ORDER BY 1"
       
       --definir cursor con consulta de BD
       DECLARE curDet_h CURSOR FROM consulta
       CALL reg_det_d.clear()
       --Llenar el arreglo con el resultado de la consulta
       FOREACH curDet_h INTO rDet_h.*, fecha
          LET x = x + 1 
          LET rDet_h.rdccanfecoper = util.Datetime.format(fecha, "%d/%m/%Y %H:%M")
          LET reg_det_h[x].* = rDet_h.*  
       END FOREACH
   END IF
   
   RETURN x --Cantidad de registros
END FUNCTION

FUNCTION consulta_hc(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE fecha        LIKE reqdcan.rdccanfecoper
DEFINE usuanalista  LIKE commempl.id_commempl
DEFINE analista     CHAR(30)
DEFINE nom, ape     STRING
DEFINE ei, ef       CHAR(1)
DEFINE 
   rDetC_h tRLC_h,
   x INTEGER,
   consulta STRING
DEFINE cmb_dat tComboTex

   CALL cmb_canestado_init() RETURNING cmb_dat.*
   
   LET x = 0
    --"reqanalista","SELECT id_commempl, trim(nombre) ||' ' || trim(apellido) FROM commempl WHERE esRRHH = '1' ORDER BY 2 
   IF  g_reg.canid_1 > 0 THEN
       LET consulta = 
          "SELECT  null, null, null, reqid, ",
                  " null, cseg_analista, cseg_fecha, cseg_canestado_ini, cseg_canestado_fin  ",
          " FROM reqmcsg ",
          " WHERE canid = ", g_reg.canid_1,
          " ORDER BY cseg_fecha DESC"
       
       --definir cursor con consulta de BD
       DECLARE curDetC_h CURSOR FROM consulta
       CALL reg_det_d.clear()
       --Llenar el arreglo con el resultado de la consulta
       FOREACH curDetC_h INTO rDetC_h.*, usuanalista, fecha, ei, ef 
          LET x = x + 1 
          SELECT nombre, apellido 
             INTO nom, ape
            FROM commempl 
           WHERE id_commempl = usuanalista
          LET analista = nom.trim(), ' ', ape.trim()
          LET rDetC_h.cseg_fecha = util.Datetime.format(fecha, "%d/%m/%Y %H:%M")
          LET rDetC_h.analista = analista
          LET rDetC_h.einicio  = consulta_hc_estado(cmb_dat, ei)
          LET rDetC_h.efinal   = consulta_hc_estado(cmb_dat, ef)          
          LET reg_det_ch[x].* = rDetC_h.*  
       END FOREACH
   END IF
   
   RETURN x --Cantidad de registros
END FUNCTION

FUNCTION consulta_hc_estado(cmb_dat, estado)
DEFINE cmb_dat tComboTex
DEFINE i       SMALLINT
DEFINE estado  CHAR(1)
DEFINE nombre  STRING

   FOR i = 1 TO cmb_dat.cantidad
       IF cmb_dat.datos[i].id = estado THEN
          LET nombre = cmb_dat.datos[i].nombre
          EXIT FOR
       END IF
   END FOR

RETURN nombre
END FUNCTION