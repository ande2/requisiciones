GLOBALS "reqp0312_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE reqmcan ",
      "SET ",
      ---grupo1
      " canfecing = ?, ",
      " canobserva = ?,",
      ---grupo2
      " cannombre1   = ?, ",
      " cannombre2   = ?, ",
      " cannombre3   = ?, ",
      " canapellido1 = ?, ",
      " canapellido2 = ?, ",
      " canapellidoc = ?, ",
      ---grupo3
      " cantipiden = ?, ",
      " canidentif = ?, ",
      " cangenero = ?, ",
      " cantrato = ?, ",
      " canfecnac = ?, ",
      " canestadocivil = ?, ",
      ---grupo4
      " canemail = ?, ",
      " cantelefono = ?, ",
      " candireccion = ?, ",
      ---grupo5
      " cannivelacademico = ?, ",
      " cancarrera = ?, ",
      " relgid = ?, ",
      " cantrasporte = ?, ",
      " canfamilialab = ?, ",
      " caninfofam = ?, ",
      " canfamiliapub = ?, ",
      " canexperilab = ?, ",
      " cansalud = ?, ",
      ---grupo6
      " canturnorota = ?, ",
      " canfindesemana = ?, ",
      " canprocesant = ?, ",
      " trcid = ?, ",
      " canfecprocita = ?, ",
      " canfecprochora = ?, ",
      --
      " canusuoper = ?, ",
      " canfecoper = ? ",
      ---
      " WHERE canid = ? and reqid = ?"

   PREPARE st_modificar FROM strSql

   
END FUNCTION 

FUNCTION modifica(opcion)
DEFINE opcion   CHAR(1)

   CALL encabezado("Modificar")
   --CALL info_usuario()

   LET g_reg.seguimiento = NULL
   
   IF opcion = 'C' THEN
      IF captura_datos(opcion) THEN
          LET g_reg.seguimiento = 10
          RETURN actualizar_p()
      ELSE
         RETURN FALSE
      END IF
   ELSE
       IF captura_datos(opcion) THEN
          IF opcion = 'M' THEN
             RETURN actualizar()
          END IF
          IF opcion = 'P' THEN
             RETURN actualizar_p()
          END IF
       ELSE 
          RETURN FALSE 
       END IF 
   END IF
END FUNCTION 

FUNCTION actualizar()
   DEFINE cita LIKE reqmcan.canfecprochora
   DEFINE ahora LIKE reqmcan.canfecoper

   IF g_reg.canfecprocita >= TODAY THEN
      LET cita = def_hora(g_reg.canfecprocita, g_reg.hora)
   ELSE
      DISPLAY g_reg.canfecprocita
   END IF

   LET ahora = CURRENT YEAR TO MINUTE
   TRY
      EXECUTE st_modificar USING 
            g_reg.canfecing, g_reg.canobserva,
            g_reg.cannombre1, g_reg.cannombre2, g_reg.cannombre3, g_reg.canapellido1, g_reg.canapellido2, g_reg.canapellidoc,
            g_reg.cantipiden, g_reg.canidentif, g_reg.cangenero, g_reg.cantrato, g_reg.canfecnac, g_reg.canestadocivil,
            g_reg.canemail, g_reg.cantelefono, g_reg.candireccion,
            g_reg.cannivelacademico, g_reg.cancarrera, g_reg.relgid, g_reg.cantrasporte, g_reg.canfamilialab, g_reg.caninfofam, g_reg.canfamiliapub, g_reg.canexperilab, g_reg.cansalud,
            g_reg.canturnorota, g_reg.canfindesemana, g_reg.canprocesant, g_reg.trcid,
            g_reg.canfecprocita, cita,
            usuario, ahora,
            -- WHERE
            l_reg.canid, requi
   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

FUNCTION actualizar_p()
   DEFINE cita LIKE reqmcan.canfecprochora
   DEFINE ahora LIKE reqmcan.canfecoper
   DEFINE segui RECORD LIKE reqmcsg.*
   DEFINE otro   RECORD LIKE reqmcan.*
   DEFINE rechaotros  SMALLINT
   DEFINE estadoini LIKE reqmcan.canestado

         
   SELECT canestado INTO estadoini
     FROM reqmcan
    WHERE reqid = requi 
      AND canid = l_reg.canid

   IF estadoini = 'C' THEN LET segui.cseg_canestado_fin = "C" END IF

   IF g_reg.canfecprocita >= TODAY THEN
      LET cita = def_hora(g_reg.canfecprocita, g_reg.hora)
   ELSE
      DISPLAY g_reg.canfecprocita
   END IF

   LET ahora = CURRENT YEAR TO MINUTE

   LET rechaotros = FALSE

    -- "A" = "Ingresado"
    ------ PROCESO TRANSITORIOS DEL CANDIDATO
    -- "B" = "En Investigación"
    -- "C" = "En Entrevsita"
    -- "D" = "Aprobado Entrevista"
    -- "E" = "En Evaluación Médica"
    -- "F" = "Proceso de Contratación"
    ------ ESTADO FINAL SELECIONADO PARA CONTRATAR
    -- "S" = "Contratado"
    ------ ESTADOS FINALES POR RECHAZO
    -- "X" = "Rechazado Analista"
    -- "Y" = "Rechazado Clinica"
    -- "Z" = "Rechazado Entrevista"

    --   CALL cb.addItem(1,"Iniciar investigación")
    --   CALL cb.addItem(2,"Evaluación médica")           
    --   CALL cb.addItem(3,"Entevista")
    --   CALL cb.addItem(4,"Proceso de contratacion") 
    --   CALL cb.addItem(5,"Rechazar candidato")         
    --   CALL cb.addItem(6,"Finalizar proceso") 
         
  CASE g_reg.seguimiento
    WHEN 1 LET segui.cseg_canestado_fin = "B"
    WHEN 2 LET segui.cseg_canestado_fin = "C"
    WHEN 3 LET segui.cseg_canestado_fin = "E"
    WHEN 4 LET segui.cseg_canestado_fin = "G"
    WHEN 5 LET segui.cseg_canestado_fin = "X" -- FIANLIZAR PROCESO
    WHEN 6 LET segui.cseg_canestado_fin = "S" --
    WHEN 10 -- SELECCION DEL SOLICITANTE
       LET segui.cseg_canestado_fin = "D" -- Solicitante selecciona candidato
  END CASE
  
    IF g_reg.seguimiento = 6 OR g_reg.seguimiento = 10 THEN
       LET unicocan = FALSE
       CASE box_gradato("¿Desea que los otros candidatos sean rechazados a la plaza?")
          WHEN "Si"
             LET unicocan = TRUE
          WHEN "No"
             LET unicocan = FALSE                
       END CASE
    END IF

  
   TRY
      BEGIN WORK
      LET segui.reqid = requi
      LET segui.canid = l_reg.canid
      LET segui.cseg_analista = usuario
      LET segui.cseg_fecha = ahora
      LET segui.cseg_canestado_ini = g_reg.canestado_1

      IF  segui.cseg_canestado_ini <> segui.cseg_canestado_fin THEN

         INSERT INTO reqmcsg (reqid,canid,cseg_analista,cseg_fecha,
                              cseg_canestado_ini,cseg_canestado_fin)
            VALUES (segui.reqid, segui.canid, segui.cseg_analista, segui.cseg_fecha,
                    segui.cseg_canestado_ini, segui.cseg_canestado_fin)
         
         UPDATE reqmcan 
            SET canestado = segui.cseg_canestado_fin
            WHERE reqid = segui.reqid 
              AND canid = segui.canid

         IF segui.cseg_canestado_fin = 'C' THEN
            LET g_reg.canfecprocita = NULL
            LET cita = NULL 
            UPDATE reqmcan 
               SET canfecprocita = g_reg.canfecprocita,
                   canfecprochora = cita
             WHERE reqid = segui.reqid 
               AND canid = segui.canid         
         END IF
      ELSE 
         LET cita = def_hora(g_reg.canfecprocita, g_reg.hora)

         UPDATE reqmcan 
            SET canfecprocita = g_reg.canfecprocita,
                canfecprochora = cita
          WHERE reqid = segui.reqid 
            AND canid = segui.canid
      END IF

      CASE 
         WHEN (segui.cseg_canestado_fin = 'D' OR segui.cseg_canestado_fin = 'S')

            IF segui.cseg_canestado_fin = 'S' THEN 
               UPDATE reqmreq
                  SET fechaing = g_reg.fechaing
                WHERE reqid = segui.reqid
            END IF 
            
            IF unicocan THEN
            
               DECLARE cur_otros CURSOR FOR
                  SELECT * 
                    FROM reqmcan
                   WHERE reqid = segui.reqid
                     AND canestado IN ('A','B','C','D','E','F')
                     AND canid <> segui.canid
                     
               FOREACH cur_otros INTO otro.*
                   UPDATE reqmcan
                      SET canestado = 'Z',
                          canmotrechob = 'Otra persona fue seleccionada'    
                    WHERE canid = otro.canid
                      AND reqid = otro.reqid

                  LET segui.reqid = otro.reqid
                  LET segui.canid = otro.canid
                  LET segui.cseg_analista = usuario
                  LET segui.cseg_fecha = ahora
                  LET segui.cseg_canestado_ini = otro.canestado
                  LET segui.cseg_canestado_fin = "Z"
                  
                  INSERT INTO reqmcsg (reqid,canid,cseg_analista,cseg_fecha,
                                       cseg_canestado_ini,cseg_canestado_fin)
                     VALUES (segui.reqid, segui.canid, segui.cseg_analista, segui.cseg_fecha,
                             segui.cseg_canestado_ini, segui.cseg_canestado_fin)

                  CALL add_proceso_cand( segui.reqid, segui.canid)
               END FOREACH
            END IF
            
         WHEN segui.cseg_canestado_fin = 'X'
              UPDATE reqmcan 
                 SET trechid = g_reg.trechid,
                     canmotrechob = g_reg.canmotrechob
                 WHERE reqid = segui.reqid 
                   AND canid = segui.canid

               CALL add_proceso_cand( segui.reqid, segui.canid)
      END CASE
      COMMIT WORK
   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      ROLLBACK WORK
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION

FUNCTION add_proceso_cand(requisicion, candidato)
DEFINE candidato     LIKE reqmcan.canid
DEFINE requisicion   LIKE reqmreq.reqid
DEFINE canproccant_v LIKE reqmcan.canproccant
DEFINE entrevista    SMALLINT
   
    LET entrevista = 0
    SELECT COUNT(*) INTO entrevista
      FROM reqmcsg
     WHERE canid = candidato
       AND reqid = requisicion
       AND cseg_canestado_fin = 'C' -- estado entrevista

    IF entrevista > 0 THEN
       SELECT canproccant
         INTO canproccant_v
         FROM reqmcan
        WHERE canid = candidato
      
       IF canproccant_v IS NULL THEN LET canproccant_v = 0 END IF
     
       LET canproccant_v = canproccant_v + 1

       UPDATE reqmcan 
          SET canproccant = canproccant_v
        WHERE reqid = requisicion
          AND canid = candidato
       
    END IF
END FUNCTION

FUNCTION modifica_d()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos_d('M') THEN
      RETURN actualizar_d()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION actualizar_d()
DEFINE i SMALLINT 

   TRY
      IF g_reg.canid_1 IS NOT NULL THEN
          UPDATE reqmcandoc
             SET cantdid = g_reg_d.cantdid,
                 cdocobserv = g_reg_d.cdocobserv
           WHERE canid = g_reg.canid_1
             AND cdocid = g_reg_d.cdocid
       ELSE
          UPDATE tmp_reqmcandoc
             SET cantdid = g_reg_d.cantdid,
                 cdocobserv = g_reg_d.cdocobserv
           WHERE cdocid = g_reg_d.cdocid
       END IF
   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

