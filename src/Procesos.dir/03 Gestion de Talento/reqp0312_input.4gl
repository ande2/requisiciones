GLOBALS "reqp0312_glob.4gl"

FUNCTION captura_datos(operacion)
    DEFINE operacion CHAR(1)
    DEFINE resultado BOOLEAN
--DEFINE w ui.Window
--DEFINE f ui.Form
    DEFINE idx SMALLINT
    DEFINE touchDet SMALLINT
    DEFINE cita DATETIME YEAR TO SECOND
    DEFINE hora DATETIME HOUR TO MINUTE
    DEFINE cuantos SMALLINT
    DEFINE cuantosh SMALLINT
    DEFINE cuantoshc SMALLINT
    DEFINE canidadd LIKE reqmcan.canid
    DEFINE canidsig SMALLINT
    DEFINE canidenc SMALLINT
    DEFINE req_tmp LIKE reqmreq.reqid
    DEFINE contador  SMALLINT 
    DEFINE lvar STRING 

    LET w = ui.Window.getCurrent()
    LET f = w.getForm()

    CALL reg_det_d.clear()
    CALL reg_det_h.clear()

    CALL f.setElementHidden("gridListReg", 1) -- 1 Ocultar
    CALL f.setElementHidden("gridInput", 0) -- 0 Mostrar
    CALL f.setElementHidden("grupo11", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo9", 1) -- SEGUIMEINTO
    CALL f.setElementHidden("lbl_caninfofam", 1)
    CALL f.setElementHidden("reqmcan.caninfofam", 1)

    IF lescoorrhh = '1' OR lesrrhh = '1' THEN
        CALL f.setElementHidden("grupo7", 0)
    ELSE
        CALL f.setElementHidden("grupo7", 1)
    END IF

    CALL combo_init()
    LET resultado = FALSE
    LET u_reg.* = g_reg.*
    CASE
        WHEN operacion = 'I'

            DELETE FROM tmp_reqmcandoc

            INITIALIZE g_reg.* TO NULL
            LET g_reg.canfecing = TODAY
            LET g_reg.cantipiden = 'DPI'
            LET hora = "08:00"
            LET g_reg.hora = hora
            DISPLAY BY NAME g_reg.*
            CALL f.setElementHidden("grupo7", 1)
            CALL f.setElementHidden("grupoh", 1)
        WHEN (operacion = 'V' OR operacion = 'C')
            IF requi IS NOT NULL THEN
                SELECT canid,
                    canestado,
                    canfecing,
                    canproccant,
                    canobserva,
                    cannombre1,
                    cannombre2,
                    cannombre3,
                    canapellido1,
                    canapellido2,
                    canapellidoc,
                    cantipiden,
                    canidentif,
                    cangenero,
                    cantrato,
                    canfecnac,
                    canestadocivil,
                    canemail,
                    cantelefono,
                    candireccion,
                    cannivelacademico,
                    cancarrera,
                    relgid,
                    cantrasporte,
                    canfamilialab,
                    caninfofam,
                    canfamiliapub,
                    canexperilab,
                    cansalud,
                    canturnorota,
                    canfindesemana,
                    canprocesant,
                    trcid,
                    canfecprocita,
                    canfecprochora
                    INTO g_reg.*, cita
                    FROM reqmcan
                    WHERE canid = l_reg.canid
                        AND (reqid = requi OR reqid IS NULL)

                SELECT t.trechid, t.canmotrechob
                    INTO g_reg.trcid, g_reg.canmotrechob
                    FROM reqmcan t
                    WHERE t.canid = g_reg.canid_1
                INITIALIZE g_cli.* TO NULL
                SELECT cmed_citahora
                    INTO g_cli.cmed_citahora
                    FROM reqmccm
                    WHERE canid = g_reg.canid_1 AND cmed_result = 'C'

                SELECT cmed_result, cmed_observa
                    INTO g_cli.cmed_result, g_cli.cmed_observa
                    FROM reqmccm
                    WHERE canid = g_reg.canid_1 AND cmed_result IN ('1', '0')

                DISPLAY BY NAME g_cli.*

            ELSE
                SELECT reqid,
                    canid,
                    canestado,
                    canfecing,
                    canproccant,
                    canobserva,
                    cannombre1,
                    cannombre2,
                    cannombre3,
                    canapellido1,
                    canapellido2,
                    canapellidoc,
                    cantipiden,
                    canidentif,
                    cangenero,
                    cantrato,
                    canfecnac,
                    canestadocivil,
                    canemail,
                    cantelefono,
                    candireccion,
                    cannivelacademico,
                    cancarrera,
                    relgid,
                    cantrasporte,
                    canfamilialab,
                    caninfofam,
                    canfamiliapub,
                    canexperilab,
                    cansalud,
                    canturnorota,
                    canfindesemana,
                    canprocesant,
                    trcid,
                    canfecprocita,
                    canfecprochora
                    INTO req_tmp, g_reg.*, cita
                    FROM reqmcan
                    WHERE canid = l_reg.canid

                CALL f.setElementHidden("groupRequi", 0)
                IF NOT disp_requi(req_tmp) THEN
                    CALL f.setElementHidden("groupRequi", 1)
                END IF

            END IF

            --CALL f.setElementHidden("grupo7",0)
            DISPLAY BY NAME g_reg.*
            LET u_reg.* = g_reg.*

            IF g_reg.canfamilialab = 'S' THEN
                CALL f.setElementHidden("lbl_caninfofam", 0)
                CALL f.setElementHidden("reqmcan.caninfofam", 0)
            END IF

        OTHERWISE
            SELECT canid,
                canestado,
                canfecing,
                canproccant,
                canobserva,
                cannombre1,
                cannombre2,
                cannombre3,
                canapellido1,
                canapellido2,
                canapellidoc,
                cantipiden,
                canidentif,
                cangenero,
                cantrato,
                canfecnac,
                canestadocivil,
                canemail,
                cantelefono,
                candireccion,
                cannivelacademico,
                cancarrera,
                relgid,
                cantrasporte,
                canfamilialab,
                caninfofam,
                canfamiliapub,
                canexperilab,
                cansalud,
                canturnorota,
                canfindesemana,
                canprocesant,
                trcid,
                canfecprocita,
                canfecprochora
                INTO g_reg.*, cita
                FROM reqmcan
                WHERE canid = l_reg.canid AND (reqid = requi OR reqid IS NULL)

            CALL f.setElementHidden("grupo7", 0)
            DISPLAY BY NAME g_reg.*
            LET u_reg.* = g_reg.*

            IF g_reg.canfamilialab = 'S' THEN
                CALL f.setElementHidden("lbl_caninfofam", 0)
                CALL f.setElementHidden("reqmcan.caninfofam", 0)
            END IF

    END CASE

    --CALL ui.ComboBox.setDefaultInitializer("cmb_canestado_1")

    LET cuantos = consulta_d(FALSE)
    LET cuantosh = consulta_h(FALSE)
    LET cuantoshc = consulta_hc(FALSE)
    IF cuantosh > 0 THEN
        CALL f.setElementHidden("grupoh", 0)
        DISPLAY ARRAY reg_det_h TO sLR_h.*
            BEFORE DISPLAY
                EXIT DISPLAY
        END DISPLAY
    END IF
    IF cuantoshc > 0 THEN
        CALL f.setElementHidden("grupohc", 0)
        DISPLAY ARRAY reg_det_ch TO sLRC_h.*

            BEFORE DISPLAY
                DISPLAY BY NAME g_reg.canid_1
                EXIT DISPLAY
        END DISPLAY
    END IF

    CASE
        WHEN operacion = 'V'
            DISPLAY ARRAY reg_det_d TO sLR_d.*
                BEFORE DISPLAY
                    EXIT DISPLAY
            END DISPLAY

            DISPLAY ARRAY reg_det_d TO sLR_d.*

                BEFORE DISPLAY
                    IF cuantosh = 0 THEN
                        CALL DIALOG.setActionActive("Historial", 0)
                    END IF
                    IF cuantos = 0 THEN
                        CALL DIALOG.setActionActive("Documentos", 0)
                    END IF
                    DISPLAY BY NAME g_reg.canid_1

                ON ACTION Documentos
                    CALL main_menu_d(operacion)

                ON ACTION Historial
                    CALL main_menu_h(operacion)

            END DISPLAY

        WHEN operacion = 'C'
            DISPLAY ARRAY reg_det_d TO sLR_d.*
                BEFORE DISPLAY
                    EXIT DISPLAY
            END DISPLAY

            DIALOG ATTRIBUTES(UNBUFFERED)
                DISPLAY ARRAY reg_det_d TO sLR_d.*

                    BEFORE DISPLAY
                        IF cuantosh = 0 THEN
                            CALL DIALOG.setActionActive("Historial", 0)
                        END IF
                        IF cuantos = 0 THEN
                            CALL DIALOG.setActionActive("Documentos", 0)
                        END IF

                    ON ACTION Documentos
                        CALL main_menu_d(operacion)

                    ON ACTION Historial
                        CALL main_menu_h(operacion)

                END DISPLAY
                ON ACTION ACCEPT
                    CASE box_gradato(
                        "Selecionará a este candidato para el puesto. Desea continuar?")
                        WHEN "Si"
                            LET resultado = TRUE
                            EXIT DIALOG
                        WHEN "No"
                            EXIT DIALOG
                        OTHERWISE
                            CONTINUE DIALOG
                    END CASE
                    LET resultado = TRUE
                    EXIT DIALOG

                ON ACTION CLOSE 
                    EXIT DIALOG

            END DIALOG
        OTHERWISE
            DISPLAY ARRAY reg_det_d TO sLR_d.*
                BEFORE DISPLAY
                    EXIT DISPLAY
            END DISPLAY
            CALL f.setElementHidden("grupo12", 1)
            IF operacion = "I" OR operacion = "M" THEN
                DIALOG ATTRIBUTES(UNBUFFERED)
                    INPUT BY NAME g_reg.canfecing,
                        g_reg.canobserva,
                        g_reg.cannombre1,
                        g_reg.cannombre2,
                        g_reg.cannombre3,
                        g_reg.canapellido1,
                        g_reg.canapellido2,
                        g_reg.canapellidoc,
                        g_reg.cantipiden,
                        g_reg.canidentif,
                        g_reg.cangenero,
                        g_reg.cantrato,
                        g_reg.canfecnac,
                        g_reg.canestadocivil,
                        g_reg.canemail,
                        g_reg.cantelefono,
                        g_reg.candireccion,
                        g_reg.cannivelacademico,
                        g_reg.cancarrera,
                        g_reg.relgid,
                        g_reg.cantrasporte,
                        g_reg.canfamilialab,
                        g_reg.caninfofam,
                        g_reg.canfamiliapub,
                        g_reg.canexperilab,
                        g_reg.cansalud,
                        g_reg.canturnorota,
                        g_reg.canfindesemana,
                        g_reg.canprocesant,
                        g_reg.trcid,
                        g_reg.canfecprocita,
                        g_reg.hora
                        ATTRIBUTES(WITHOUT DEFAULTS)

                        BEFORE INPUT
                            DISPLAY BY NAME g_reg.canid_1, g_reg.canestado_1
                            CALL DIALOG.setActionHidden("close", TRUE)

                        AFTER FIELD canidentif
                           IF operacion = 'I' AND g_reg.cantipiden = 'DPI' AND g_reg.canidentif IS NOT NULL THEN
                              LET contador = 0
                              SELECT COUNT(*) INTO contador FROM reqmcan 
                                 WHERE @cantipiden = 'DPI'
                                 AND   @canidentif = g_reg.canidentif
                              IF sqlca.sqlcode = 0 THEN
                                 IF contador > 0 THEN
                                    CALL msg("DPI ya existe en la BD, verifique")
                                    NEXT FIELD canidentif
                                 END IF 
                              END IF 
                           END IF

                        BEFORE FIELD canidentif
                           IF operacion = 'M' AND g_reg.cantipiden = 'DPI' THEN
                              NEXT FIELD NEXT 
                           END IF 

                        ON ACTION Documentos
                            --IF operacion = 'I' OR operacion = 'A' THEN
                            --   CALL msg("Debe guardar información del candidato antes de ingresar documentos")
                            --ELSE
                            CALL main_menu_d(operacion)
                            --END IF

                        ON CHANGE canfamilialab
                            IF g_reg.canfamilialab = 'S' THEN
                                CALL f.setElementHidden("lbl_caninfofam", 0)
                                CALL f.setElementHidden("reqmcan.caninfofam", 0)
                            ELSE
                                CALL f.setElementHidden("lbl_caninfofam", 1)
                                CALL f.setElementHidden("reqmcan.caninfofam", 1)
                            END IF

                    END INPUT

                    ON ACTION ACCEPT

                        IF g_reg.caninfofam = 'N' THEN
                            LET g_reg.caninfofam = NULL
                        END IF

                        IF g_reg.cannombre1 IS NULL THEN
                            CALL msg("Debe ingresar primer nombre")
                            NEXT FIELD cannombre1
                        END IF

                        IF g_reg.canapellido1 IS NULL THEN
                            CALL msg("Debe ingresar primer apellido")
                            NEXT FIELD canapellido1
                        END IF

                        IF g_reg.cantipiden IS NULL THEN
                            CALL msg("Debe ingresar el tipo de identificación")
                            NEXT FIELD cantipiden
                        END IF

                        IF g_reg.cangenero IS NULL THEN
                            CALL msg("Debe ingresar el genero del candidato.")
                            NEXT FIELD cangenero
                        END IF

                        IF g_reg.canidentif IS NULL THEN
                            CALL msg("Debe ingresar la identificación")
                            NEXT FIELD canidentif
                        ELSE
                            LET canidenc = 0
                            SELECT COUNT(*)
                                INTO canidenc
                                FROM reqmcan
                                WHERE canidentif = g_reg.canidentif
                                    AND canid <> g_reg.canid_1
                            IF canidenc > 0 THEN
                                CALL msg(
                                    "Se encontró otra identificación a otro candidato. Verifique.")
                                LET g_reg.canidentif = u_reg.canidentif
                                NEXT FIELD canidentif
                            END IF
                        END IF

                        IF operacion = 'M' AND g_reg.* = u_reg.* THEN
                            IF touchDet <> 1 THEN
                                CALL msg("No se efectuaron cambios")
                                EXIT DIALOG
                            END IF
                        END IF

                        CASE box_gradato("Seguro de grabar")
                            WHEN "Si"
                                LET resultado = TRUE
                                EXIT DIALOG
                            WHEN "No"
                                EXIT DIALOG
                            OTHERWISE
                                CONTINUE DIALOG
                        END CASE
                        LET resultado = TRUE
                        EXIT DIALOG

                    ON ACTION CANCEL
                        EXIT DIALOG

                END DIALOG
            ELSE
                CALL f.setElementHidden("group1", 1)
                CALL f.setElementHidden("grupo3", 1)
                CALL f.setElementHidden("grupo4", 1)
                CALL f.setElementHidden("grupo5", 1)
                CALL f.setElementHidden("grupo6", 1)
                CALL f.setElementHidden("grupo7", 1)
                CALL f.setElementHidden("grupo8", 1)
                CALL f.setElementHidden("grupo10", 1)
                CALL f.setElementHidden("grupoh", 1)
                CALL f.setElementHidden("grupohc", 1)
                CALL f.setElementHidden("grupo9", 0)
                CALL f.setElementHidden("grupo12", 1)

                CALL cmb_seguimiento()

                IF lessolicita
                    = '1' THEN -- CUANDO ES EL ENTREVITADOR Y AGREGA CITA

                    CALL f.setElementHidden("grupo8", 0)
                    CALL f.setElementHidden("grupo9", 1)
                    LET g_reg.canfecprocita = NULL
                    LET g_reg.hora = NULL
                    LET g_reg.trechid = NULL
                    LET g_reg.canmotrechob = NULL

                    DIALOG ATTRIBUTES(UNBUFFERED)
                        INPUT BY NAME g_reg.seguimiento,
                            g_reg.canfecprocita,
                            g_reg.hora,
                            g_reg.trechid,
                            g_reg.canmotrechob
                            ATTRIBUTES(WITHOUT DEFAULTS)

                            BEFORE INPUT
                                DISPLAY BY NAME g_reg.canid_1, g_reg.canestado_1
                                CALL DIALOG.setActionHidden("close", TRUE)
                                
                                PREPARE ex_st1 FROM "SELECT canfecprocita, extract(hour FROM canfecprochora)||':'||extract(minute FROM canfecprochora) FROM reqmcan WHERE canid = " || g_reg.canid_1 
                                EXECUTE ex_st1 INTO g_reg.canfecprocita, g_reg.hora

                            ON CHANGE seguimiento
                                CASE g_reg.seguimiento
                                    WHEN 5
                                        CALL f.setElementHidden("grupo7", 0)
                                        CALL f.setElementHidden("grupo8", 1)
                                        CALL f.setElementHidden("grupo12", 1)
                                    WHEN 3
                                        CALL f.setElementHidden("grupo7", 1)
                                        CALL f.setElementHidden("grupo8", 0)
                                        CALL f.setElementHidden("grupo12", 1)
                                    OTHERWISE
                                        CALL f.setElementHidden("grupo7", 1)
                                        CALL f.setElementHidden("grupo8", 1)
                                        CALL f.setElementHidden("grupo12", 1)
                                END CASE
                        END INPUT

                        ON ACTION ACCEPT

                            IF operacion = 'P' AND g_reg.* = u_reg.* THEN
                                IF touchDet <> 1 THEN
                                    CALL msg("No se efectuaron cambios")
                                    EXIT DIALOG
                                END IF
                            END IF

                            CASE box_gradato("Seguro de grabar")
                                WHEN "Si"
                                    LET resultado = TRUE
                                    --IF g_reg.seguimiento <> 3 THEN
                                    IF g_reg.seguimiento NOT IN (3, 5) THEN
                                        LET g_reg.trechid = NULL
                                        LET g_reg.canmotrechob = NULL
                                    END IF
                                    IF g_reg.seguimiento <> 5 THEN
                                        LET g_reg.canfecprocita = NULL
                                        LET g_reg.hora = NULL
                                    END IF
                                    EXIT DIALOG
                                WHEN "No"
                                    EXIT DIALOG
                                OTHERWISE
                                    CONTINUE DIALOG
                            END CASE
                            LET resultado = TRUE
                            EXIT DIALOG

                        ON ACTION CANCEL
                            EXIT DIALOG

                    END DIALOG

                ELSE -- seguimiento cuando no es el esntrevistador

                    LET g_reg.canfecprocita = NULL
                    LET g_reg.hora = NULL
                    LET g_reg.trechid = NULL
                    LET g_reg.canmotrechob = NULL
                    LET g_reg.fechaing = NULL

                    DIALOG ATTRIBUTES(UNBUFFERED)
                        INPUT BY NAME g_reg.seguimiento,
                            g_reg.canfecprocita,
                            g_reg.hora,
                            g_reg.trechid,
                            g_reg.canmotrechob,
                            g_reg.fechaing
                            ATTRIBUTES(WITHOUT DEFAULTS)

                            BEFORE INPUT
                                DISPLAY BY NAME g_reg.canid_1, g_reg.canestado_1
                                CALL DIALOG.setActionHidden("close", TRUE)

                            ON CHANGE seguimiento
                                CASE g_reg.seguimiento
                                    WHEN 5
                                        CALL f.setElementHidden("grupo7", 0)
                                        CALL f.setElementHidden("grupo8", 1)
                                        CALL f.setElementHidden("grupo12", 1)
                                        CALL f.setElementHidden("group1", 0)
                                    WHEN 3
                                        CALL f.setElementHidden("grupo7", 1)
                                        CALL f.setElementHidden("group1", 0)
                                        CALL f.setElementHidden("grupo8", 0)
                                        CALL f.setElementHidden("grupo12", 1)
                                    WHEN 6
                                        CALL f.setElementHidden("grupo12", 0)
                                        CALL f.setElementHidden("group1", 1)
                                    OTHERWISE
                                        CALL f.setElementHidden("grupo7", 1)
                                        CALL f.setElementHidden("grupo8", 1)
                                        CALL f.setElementHidden("grupo12", 1)
                                        CALL f.setElementHidden("group1", 1)
                                END CASE
                        END INPUT

                        ON ACTION ACCEPT

                            IF g_reg.seguimiento > 0 THEN
                                DISPLAY "Continuar"
                            ELSE
                                CONTINUE DIALOG
                            END IF

                            IF g_reg.seguimiento = 6 THEN
                                IF g_reg.fechaing < TODAY THEN
                                    CALL msg(
                                        "Debe ingresar la fecha de ingreso mayor a la actual.")
                                    NEXT FIELD fechaing
                                END IF
                            END IF

                            IF operacion = 'P' AND g_reg.* = u_reg.* THEN
                                IF touchDet <> 1 THEN
                                    CALL msg("No se efectuaron cambios")
                                    EXIT DIALOG
                                END IF
                            END IF

                            CASE box_gradato("Seguro de grabar")
                                WHEN "Si"
                                    LET resultado = TRUE
                                    --IF g_reg.seguimiento <> 3 THEN
                                    IF g_reg.seguimiento NOT IN (3, 5) THEN
                                        LET g_reg.trechid = NULL
                                        LET g_reg.canmotrechob = NULL
                                    END IF
                                    IF g_reg.seguimiento <> 5 THEN
                                        LET g_reg.canfecprocita = NULL
                                        LET g_reg.hora = NULL
                                    END IF
                                    EXIT DIALOG
                                WHEN "No"
                                    EXIT DIALOG
                                OTHERWISE
                                    CONTINUE DIALOG
                            END CASE
                            LET resultado = TRUE
                            EXIT DIALOG

                        ON ACTION CANCEL
                            EXIT DIALOG

                    END DIALOG

                END IF
                CALL f.setElementHidden("grupo3", 0)
                CALL f.setElementHidden("grupo4", 0)
                CALL f.setElementHidden("grupo5", 0)
                CALL f.setElementHidden("grupo6", 0)
                CALL f.setElementHidden("grupo7", 0)
                CALL f.setElementHidden("grupo10", 0)
                CALL f.setElementHidden("grupoh", 0)
                CALL f.setElementHidden("grupohc", 0)
                CALL f.setElementHidden("grupo9", 1)
                CALL f.setElementHidden("grupo12", 1)
            END IF

            IF NOT resultado THEN
                LET g_reg.* = u_reg.*
                DISPLAY BY NAME g_reg.*
            END IF
    END CASE

    CALL f.setElementHidden("gridListReg", 0)
    CALL f.setElementHidden("gridInput", 1)
    RETURN resultado
END FUNCTION

FUNCTION cmb_seguimiento()
    DEFINE cb ui.ComboBox

    -- "A" = "Ingresado"
    ------ PROCESO TRANSITORIOS DEL CANDIDATO
    -- "B" = "En Investigación"
    -- "C" = "En Evaluación Médica"
    -- "D" = "Aprobado por Clínica"
    -- "E" = "En Entrevsita"
    -- "F" = "Proceso de Contratación"
    ------ ESTADO FINAL SELECIONADO PARA CONTRATAR
    -- "S" = "Contratado"
    ------ ESTADOS FINALES POR RECHAZO
    -- "X" = "Rechazado Analista"
    -- "Y" = "Rechazado Clinica"
    -- "Z" = "Rechazado Entrevista"

    LET cb = ui.ComboBox.forName("formonly.seguimiento")

    CALL cb.clear()
    CASE
        WHEN g_reg.canestado_1 = 'A'
            CALL cb.addItem(1, "Iniciar investigación")
            CALL cb.addItem(2, "Entrevista")
            CALL cb.addItem(5, "Rechazar candidato")
        WHEN g_reg.canestado_1 = 'B'
            CALL cb.addItem(2, "Entrevista")
            CALL cb.addItem(5, "Rechazar candidato")
        WHEN g_reg.canestado_1 = 'D'
            CALL cb.addItem(3, "Evaluación Médica")
            CALL cb.addItem(5, "Rechazar candidato")
        WHEN g_reg.canestado_1 = 'F'
            CALL cb.addItem(4, "Proceso de contratación")
            CALL cb.addItem(5, "Rechazar candidato")
        WHEN g_reg.canestado_1 = 'G'
            CALL cb.addItem(6, "Finalizar proceso")
            CALL cb.addItem(5, "Rechazar candidato")
    END CASE
END FUNCTION

FUNCTION main_menu_d(operacion)
    DEFINE
        operacion CHAR(1),
        cuantos SMALLINT,
        id, ids SMALLINT

    CALL f.setElementHidden("grupo3", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo4", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo5", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo6", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo7", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo8", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo11", 0) -- 1 Ocultar
    CALL f.setElementHidden("grupoH", 1) -- 1 Ocultar

    CALL delete_init_d()

    DISPLAY "REGISTRO DE DOCUMENTO DE CANDIDATOS" TO gtit_enc

    DISPLAY ARRAY reg_det_d
        TO sLR_d.*
        ATTRIBUTE(ACCEPT = FALSE, CANCEL = FALSE, UNBUFFERED)

        BEFORE DISPLAY
            IF operacion = 'V' THEN
                CALL DIALOG.setActionActive("agregar", 0)
                CALL DIALOG.setActionActive("modificar", 0)
                CALL DIALOG.setActionActive("Eliminar", 0)
            END IF
            LET cuantos = consulta_d(FALSE)
            IF cuantos > 0 THEN
                CALL dialog.setCurrentRow("sLR_d", 1)
                LET l_reg_d.* = reg_det_d[1].*
                DISPLAY BY NAME l_reg_d.*
            END IF
            CALL encabezado("")

        BEFORE ROW
            LET id = arr_curr()
            IF id > 0 THEN
                LET l_reg_d.* = reg_det_d[id].*
                DISPLAY BY NAME l_reg_d.*
            END IF

        ON ACTION agregar
            IF ingreso_d() THEN
                LET cuantos = consulta_d(FALSE)
                CALL fgl_set_arr_curr(arr_count() + 1)
            END IF
            CALL encabezado("")

        ON ACTION modificar
            LET id = arr_curr()
            LET ids = scr_line()
            IF id > 0 THEN
                IF modifica_d() THEN
                    LET reg_det_d[id].cdocobserv = g_reg_d.cdocobserv
                    LET reg_det_d[id].cantdid = g_reg_d.cantdid
                    SELECT cantdnombre
                        INTO reg_det_d[id].cantdnombre
                        FROM reqmcantd
                        WHERE cantdid = reg_det_d[id].cantdid
                    DISPLAY reg_det_d[id].* TO sLR_d[ids].*
                END IF
            END IF
            CALL encabezado("")

        ON ACTION mostrar
            CALL mostrar_d()

        ON ACTION Eliminar
            LET id = arr_curr()
            LET ids = scr_line()
            IF id > 0 THEN
                IF eliminar_d() THEN
                    CALL DIALOG.deleteRow("sLR_d", id)
                    IF id = arr_count() THEN
                        LET id = id - 1
                    END IF
                    IF id > 0 THEN
                        LET l_reg_d.* = reg_det_d[id].*
                    ELSE
                        INITIALIZE l_reg_d.* TO NULL
                    END IF
                    DISPLAY BY NAME l_reg_d.*
                END IF
            END IF

        ON ACTION salir
            EXIT DISPLAY
    END DISPLAY

    CALL f.setElementHidden("grupo11", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo3", 0) -- 1 Ocultar
    CALL f.setElementHidden("grupo4", 0) -- 1 Ocultar
    CALL f.setElementHidden("grupo5", 0) -- 1 Ocultar
    CALL f.setElementHidden("grupo6", 0) -- 1 Ocultar
    CALL f.setElementHidden("grupo7", 0) -- 1 Ocultar
    CALL f.setElementHidden("grupo8", 0) -- 1 Ocultar
    CALL f.setElementHidden("grupoH", 0)

END FUNCTION

FUNCTION main_menu_h(operacion)
    DEFINE
        operacion CHAR(1),
        cuantos SMALLINT,
        id, ids SMALLINT

    CALL f.setElementHidden("grupo3", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo4", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo5", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo6", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo7", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo8", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo10", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo11", 1) -- 1 Ocultar

    DISPLAY "HISTORIAL DE CANDIDATOS" TO gtit_enc

    DISPLAY ARRAY reg_det_h
        TO sLR_h.*
        ATTRIBUTE(ACCEPT = FALSE, CANCEL = FALSE, UNBUFFERED)

        BEFORE ROW
            LET id = arr_curr()

        ON ACTION salir
            EXIT DISPLAY
    END DISPLAY

    CALL f.setElementHidden("grupo10", 0) -- 1 Ocultar
    CALL f.setElementHidden("grupo11", 1) -- 1 Ocultar
    CALL f.setElementHidden("grupo3", 0) -- 1 Ocultar
    CALL f.setElementHidden("grupo4", 0) -- 1 Ocultar
    CALL f.setElementHidden("grupo5", 0) -- 1 Ocultar
    CALL f.setElementHidden("grupo6", 0) -- 1 Ocultar
    CALL f.setElementHidden("grupo7", 0) -- 1 Ocultar
    CALL f.setElementHidden("grupo8", 0) -- 1 Ocultar
END FUNCTION

FUNCTION captura_datos_d(operacion)
    DEFINE operacion CHAR(1)
    DEFINE resultado BOOLEAN
    DEFINE touchDet SMALLINT
    DEFINE seguir SMALLINT
    DEFINE seguir2 SMALLINT
    DEFINE nombre STRING
    DEFINE rec RECORD
        path STRING,
        name STRING,
        wildcards STRING,
        caption STRING
    END RECORD

    CALL f.setElementHidden("gridInput", 0) -- 0 Mostrar

    LET resultado = FALSE
    LET seguir = TRUE
    LET seguir2 = TRUE
    LET u_reg_d.* = g_reg_d.*
    IF operacion = 'I' THEN

        INITIALIZE g_reg_d.* TO NULL
        LET g_reg_d.cdocfecing = TODAY
        DISPLAY BY NAME g_reg_d.cdocid,
            g_reg_d.cdocid,
            g_reg_d.cdocfecing,
            g_reg_d.cantdid,
            g_reg_d.cdocobserv,
            g_reg_d.cdocarchdir

        LET rec.path = ""
        LET rec.name = "Archivos"
        LET rec.wildcards = ""
        LET rec.caption = "Open file"
        WHILE seguir
            CALL ui.Interface.frontCall(
                "standard", "openFile", [rec.*], [g_reg_d.cdocarchdir])

            IF g_reg_d.cdocarchdir IS NULL THEN
                IF NOT box_confirma(
                    'El archivo no fue seleccionado. ¿Desea volver a intentar?') THEN
                    LET seguir = FALSE
                    LET seguir2 = FALSE
                END IF
            ELSE
                DISPLAY BY NAME g_reg_d.cdocarchdir
                LET seguir = FALSE
            END IF
        END WHILE
    ELSE
        LET g_reg_d.cdocid = l_reg_d.cdocid
        LET g_reg_d.cdocarchdir = l_reg_d.cdocarchdir
        LET g_reg_d.cantdid = l_reg_d.cantdid
        LET g_reg_d.cdocfecing = l_reg_d.cdocfecing
        LET g_reg_d.cdocobserv = l_reg_d.cdocobserv

        LET u_reg_d.* = g_reg_d.*
    END IF

    IF seguir2 THEN
        DIALOG ATTRIBUTES(UNBUFFERED)
            INPUT BY NAME g_reg_d.cantdid, g_reg_d.cdocobserv
                ATTRIBUTES(WITHOUT DEFAULTS)

                BEFORE INPUT
                    CALL DIALOG.setActionHidden("close", TRUE)

            END INPUT

            ON ACTION ACCEPT

                IF g_reg_d.cantdid IS NULL THEN
                    CALL msg("Debe ingresar documento")
                    NEXT FIELD CURRENT
                END IF

                IF operacion = 'M' AND g_reg_d.* = u_reg_d.* THEN
                    IF touchDet <> 1 THEN
                        CALL msg("No se efectuaron cambios")
                        EXIT DIALOG
                    END IF
                END IF

                CASE box_gradato("Seguro de grabar")
                    WHEN "Si"
                        LET resultado = TRUE
                        EXIT DIALOG
                    WHEN "No"
                        EXIT DIALOG
                    OTHERWISE
                        CONTINUE DIALOG
                END CASE
                LET resultado = TRUE
                EXIT DIALOG

            ON ACTION CANCEL
                EXIT DIALOG

        END DIALOG
    END IF

    IF NOT resultado THEN
        LET g_reg_d.* = u_reg_d.*
        DISPLAY BY NAME g_reg_d.*
    END IF

    RETURN resultado
END FUNCTION
