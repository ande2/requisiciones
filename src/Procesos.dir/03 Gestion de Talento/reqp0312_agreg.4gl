IMPORT FGL fgl_zoom

GLOBALS "reqp0312_glob.4gl"

FUNCTION agregar(opcion)
DEFINE opcion CHAR(1)
DEFINE seguir SMALLINT
DEFINE result SMALLINT
DEFINE mensaje STRING
DEFINE canadd RECORD LIKE reqmcan.*
DEFINE reqant RECORD LIKE reqmreq.*
DEFINE ahora  LIKE reqmcan.canfecoper
DEFINE maxint SMALLINT
DEFINE cantmp tRL

    LET result = FALSE
    LET maxint = 3

    IF opcion = 'A' THEN
        LET seguir = TRUE

        WHILE seguir
            INITIALIZE canadd.* TO NULL
            INITIALIZE reqant.* TO NULL
            --INITIALIZE vista.* TO NULL
        
            LET canadd.canid = zoom_canid(FGL_DIALOG_GETBUFFER())

            IF canadd.canid > 0 THEN
                IF requi > 0 THEN

                SELECT * INTO canadd.*
                  FROM reqmcan
                 WHERE canid = canadd.canid

                 IF canadd.canproccant IS NULL THEN LET canadd.canproccant = 0 END IF

                 IF canadd.canproccant < maxint THEN

                    SELECT * INTO reqant.*
                      FROM reqmreq
                     WHERE reqid = canadd.reqid

                   IF box_confirma('¿Desea cambiar al candidato a la actual requisición?') THEN
                       BEGIN WORK
                       TRY
                           LET ahora = CURRENT YEAR TO MINUTE

                           IF canadd.reqid > 0 THEN
                               INSERT INTO reqdcan (canid,reqid,canestado,
                                                    reqestado,trechid,canmotrechfec,
                                                    canmotrechob,rdcusuoper,rdccanfecoper,
                                                    canproccant)
                                    VALUES (canadd.canid, canadd.reqid, canadd.canestado,
                                            reqant.reqestado, canadd.trechid,canadd.canmotrechfec,
                                            canadd.canmotrechob, usuario, ahora,
                                            canadd.canproccant)
                           END IF

                           LET canadd.canproccant = canadd.canproccant + 1
                           UPDATE reqmcan
                              SET reqid         = requi,
                                  canproccant   = canadd.canproccant,
                                  canestado     = 'A',
                                  trechid       = NULL,
                                  canmotrechfec = NULL,
                                  canmotrechob  = NULL,
                                  canusuoper    = usuario,
                                  canfecoper    = ahora
                            WHERE canid = canadd.canid

                            COMMIT WORK
                            CALL box_valdato ("Registro agregado")

                            LET seguir = FALSE
                            LET result = TRUE
                        CATCH
                            ROLLBACK WORK
                            CALL msgError(sqlca.sqlcode,"Grabar Registro")
                            LET seguir = FALSE
                            LET result = FALSE                        
                        END TRY
                    ELSE
                       LET mensaje = 'El candidato ID ', canadd.canid USING "######",
                                     ' ', canadd.cannombre1 CLIPPED,' ', canadd.canapellido1 CLIPPED,
                                     ' ya tiene más de ', maxint USING "#",' procesos realizados, no podrá agregarlo a la requisición actual. ',
                                     '¿Desea seguir buscando?'
                       IF NOT box_confirma(mensaje) THEN
                          LET seguir = FALSE
                          LET result = FALSE
                       END IF
                    END IF
               ELSE
                   IF NOT box_confirma('¿Desea seguir buscando?') THEN
                      LET seguir = FALSE
                      LET result = FALSE
                   END IF
               END IF
               ELSE
                  LET cantmp.* = l_reg.*
                  LET l_reg.canid = canadd.canid
                  LET seguir = captura_datos("V")
                  LET l_reg.* = cantmp.*
                  CALL f.setElementHidden("groupRequi",1)
                  LET seguir = FALSE
                  LET result = FALSE
               END IF
            ELSE
            --   IF NOT box_confirma('¿Desea seguir buscando?') THEN
                  LET seguir = FALSE
                  LET result = FALSE
               --END IF
            END IF
        END WHILE
    END IF

RETURN result
END FUNCTION


PRIVATE FUNCTION zoom_canid(l_current_value STRING)
DEFINE zoom_canid   fgl_zoom.zoomType
DEFINE sql_c        STRING

    IF requi > 0 THEN
        LET sql_c = ' SELECT %2 ',
                    ' FROM vw_reqcand ',
                    ' WHERE (reqid IS NULL OR (reqid > 0 AND reqid <> ', requi,')) ',
                    ' AND %1 ORDER BY canid'
    ELSE
        LET sql_c = ' SELECT %2 ',
                    ' FROM vw_reqcand ',
                    ' WHERE %1 ORDER BY canid'
    END IF

    CALL zoom_canid.init()
    
    # define the SQL used in the zoom.  This SQL should have %1 where the where clause will be 
    # substituted with the result of the QBE.  Optionally you can also include a %2 in place 
    # of the columns e.g. SELECT %2, and the columns will be populated from the additional 
    # column definition functions.
    LET zoom_canid.sql = sql_c
    # set the title of the zoom window
    LET zoom_canid.title = "Selección de Candidato"
    # the value to return if user cancels 
    LET zoom_canid.cancelvalue = l_current_value
    
    # set to true if you dont want the QBE window to appear
    LET zoom_canid.noqbe = FALSE     

    # set to true if you dont want the List of results to appear    
    LET zoom_canid.nolist = FALSE      

    # set to true to dislay the results first without initially doing a QBE
    --LET zoom_canid.gotolist = BOOLEAN       

    # set to true if you want the window to return immediately if only row is found
    --LET zoom_canid.autoselect = BOOLEAN     

    # set to true to allow the user to select multiple ROW
    LET zoom_canid.multiplerow = FALSE
    LET zoom_canid.freezeright = TRUE
    
    # values are (c)har, (i)nteger, (f)loat, (d)ate
    CALL zoom_canid.column[1].quick_set("canid", TRUE, "i", 8, "ID")
    CALL zoom_canid.column[2].quick_set("cannombre1", FALSE, "c", 25, "Primer Nombre")
    CALL zoom_canid.column[3].quick_set("canapellido1", FALSE, "c", 25, "Primer Apellido")
    CALL zoom_canid.column[4].quick_set("cangenero", FALSE, "c", 25, "Genero")
    CALL zoom_canid.column[5].quick_set("canemail", FALSE, "c", 100, "Email")
    CALL zoom_canid.column[6].quick_set("reqid", FALSE, "c", 40, "Requisición")        
    CALL zoom_canid.column[7].quick_set("uornombre", FALSE, "c", 40, "Area funcional")
    CALL zoom_canid.column[8].quick_set("afunombre", FALSE, "c", 40, "Unidad Organizativa")
    CALL zoom_canid.column[9].quick_set("puenombre", FALSE, "c", 40, "Posición")

    --u.uornombre, a.afunombre, p.puenombre

    RETURN zoom_canid.call()
END FUNCTION
