DATABASE rh

TYPE t_reg RECORD
   notiid      LIKE reqdnot.notiid,
   etaid       LIKE reqdnot.etaid,
   accid       LIKE reqdnot.accid,
   etaidnew    LIKE reqdnot.etaidnew,
   notidigi    LIKE reqdnot.notidigi,
   notisoli    LIKE reqdnot.notisoli,
   notiaut     LIKE reqdnot.notiaut,
   notiautgf   LIKE reqdnot.notiautgf,
   noticompe   LIKE reqdnot.noticompe,
   noticoord   LIKE reqdnot.noticoord,
   notianal    LIKE reqdnot.notianal,
   noticli     LIKE reqdnot.noticli
END RECORD 

DEFINE ga_reg  DYNAMIC ARRAY OF   t_reg
DEFINE ug_reg                    t_reg
DEFINE cc      SMALLINT 

MAIN

   CALL startlog('reqnot.log')
   
   OPEN FORM notc_form FROM "notc_form"
   DISPLAY FORM notc_form

   CALL combo_init()

   LET cc = 0
   WHILE TRUE 
      CALL notc_query()

      IF cc = 1 THEN 
         EXIT WHILE  
      END IF 
      
   END WHILE   
   
END MAIN

FUNCTION notc_query()
   DEFINE i, pos, pan SMALLINT 
   DEFINE errvar      STRING 
   
   DECLARE curc CURSOR FOR 
      SELECT notiid, etaid, accid, etaidnew, 
         coalesce(notidigi,'N'), coalesce(notisoli,'N'), coalesce(notiaut,'N'), coalesce(notiautgf,'N'),
         coalesce(noticompe,'N'), coalesce(noticoord,'N'), coalesce(notianal,'N'), coalesce(noticli,'N')
         FROM reqdnot
         ORDER BY 1

   LET i = 1
   FOREACH curc INTO ga_reg[i].*
      LET i = i + 1
   END FOREACH 
   CALL ga_reg.deleteElement(i)

   DISPLAY ARRAY ga_reg TO sa_reg.* ATTRIBUTES (ACCEPT = FALSE, CANCEL = FALSE  )
      --BEFORE DISPLAY 
         --ERROR "Hay ", ga_reg.getLength()

      ON ACTION agregar ATTRIBUTES (TEXT = "Agregar")
         LET pos = ga_reg.getLength() + 1
         CALL ga_reg.insertElement(pos)
         CALL DIALOG.setArrayLength("sa_reg", pos)
         CALL DIALOG.setCurrentRow("sa_reg", pos)
         --NEXT FIELD etaid
         LET pan = pos --scr_line()
         
         LET int_flag = FALSE 
         INPUT ga_reg[pos].etaid,     ga_reg[pos].accid,     ga_reg[pos].etaidnew,
               ga_reg[pos].notidigi,  ga_reg[pos].notisoli,  ga_reg[pos].notiaut,   ga_reg[pos].notiautgf,
               ga_reg[pos].noticompe, ga_reg[pos].noticoord, ga_reg[pos].notianal,  ga_reg[pos].noticli
          FROM sa_reg[pan].etaid,     sa_reg[pan].accid,     sa_reg[pan].etaidnew,
               sa_reg[pan].notidigi,  sa_reg[pan].notisoli,  sa_reg[pan].notiaut,   sa_reg[pan].notiautgf,
               sa_reg[pan].noticompe, sa_reg[pan].noticoord, sa_reg[pan].notianal,  sa_reg[pan].noticli

            BEFORE INPUT 
               
         END INPUT

         IF int_flag THEN 
            LET int_flag = FALSE
            CALL ga_reg.deleteElement(pos)
            EXIT DISPLAY 
         END IF

         WHENEVER ERROR CONTINUE 
            INSERT INTO reqdnot (etaid, accid, etaidnew, 
               notidigi, notisoli, notiaut, notiautgf,
               noticompe, noticoord, notianal, noticli) VALUES (
               ga_reg[pos].etaid,     ga_reg[pos].accid,     ga_reg[pos].etaidnew,
               ga_reg[pos].notidigi,  ga_reg[pos].notisoli,  ga_reg[pos].notiaut,   ga_reg[pos].notiautgf,
               ga_reg[pos].noticompe, ga_reg[pos].noticoord, ga_reg[pos].notianal,  ga_reg[pos].noticli)
         WHENEVER ERROR STOP 
         IF sqlca.sqlcode <> 0 THEN
            LET errvar = err_get(sqlca.sqlcode)
            CALL errorlog(errvar CLIPPED)
            ERROR "Error al agregar ", sqlca.sqlerrd[2], ' ', errvar
         ELSE 
            EXIT DISPLAY 
         END IF
         
      ON ACTION modificar ATTRIBUTES (TEXT = "Modificar")
         LET pos = arr_curr()
         LET pan = scr_line()
         LET int_flag = FALSE
         LET ug_reg.* = ga_reg[pos].* 
         INPUT ga_reg[pos].etaid,     ga_reg[pos].accid,     ga_reg[pos].etaidnew,
               ga_reg[pos].notidigi,  ga_reg[pos].notisoli,  ga_reg[pos].notiaut,   ga_reg[pos].notiautgf,
               ga_reg[pos].noticompe, ga_reg[pos].noticoord, ga_reg[pos].notianal,  ga_reg[pos].noticli
          WITHOUT DEFAULTS 
          FROM sa_reg[pan].etaid,     sa_reg[pan].accid,     sa_reg[pan].etaidnew,
               sa_reg[pan].notidigi,  sa_reg[pan].notisoli,  sa_reg[pan].notiaut,   sa_reg[pan].notiautgf,
               sa_reg[pan].noticompe, sa_reg[pan].noticoord, sa_reg[pan].notianal,  sa_reg[pan].noticli
            BEFORE INPUT 
         END INPUT       
         IF int_flag THEN 
            LET int_flag = FALSE
            LET ga_reg[pos].* = ug_reg.*
            DISPLAY ga_reg[pos].* TO sa_reg[pan].*
         END IF 

         WHENEVER ERROR CONTINUE 
            UPDATE reqdnot SET ( etaid, accid, etaidnew, 
               notidigi, notisoli, notiaut, notiautgf,
               noticompe, noticoord, notianal, noticli) = 
               (ga_reg[pos].etaid,    ga_reg[pos].accid,     ga_reg[pos].etaidnew,
               ga_reg[pos].notidigi,  ga_reg[pos].notisoli,  ga_reg[pos].notiaut,   ga_reg[pos].notiautgf,
               ga_reg[pos].noticompe, ga_reg[pos].noticoord, ga_reg[pos].notianal,  ga_reg[pos].noticli)
               WHERE notiid = ga_reg[pos].notiid
         WHENEVER ERROR STOP 
         IF sqlca.sqlcode <> 0 THEN
            ERROR "Error al actualizar ", sqlca.sqlcode 
         END IF 

      ON ACTION eliminar ATTRIBUTES (TEXT = "Eliminar")
         IF box_elifila() THEN 
            LET pos = arr_curr()
            WHENEVER ERROR CONTINUE 
               DELETE FROM reqdnot
                  WHERE notiid = ga_reg[pos].notiid
            WHENEVER ERROR STOP 
            IF sqlca.sqlcode <> 0 THEN
               LET errvar = err_get(sqlca.sqlcode)
               CALL errorlog(errvar CLIPPED)
               ERROR "Error al eliminar ", sqlca.sqlerrd[2], ' ', errvar
            ELSE 
               EXIT DISPLAY 
            END IF
         END IF 
         
      ON ACTION regresar ATTRIBUTES (TEXT = "Regresar")
         LET cc = 1
         EXIT DISPLAY 
         
   END DISPLAY

END FUNCTION 

FUNCTION notc_upd()  
   DEFINE pos, pan SMALLINT
   
   INPUT ARRAY ga_reg WITHOUT DEFAULTS FROM sa_reg.*
   
      BEFORE FIELD etaid  
         LET pos = arr_curr()
         LET pan = scr_line()
         IF ga_reg[pos].etaidnew IS NULL THEN  
            ERROR "after insert"
            LET ga_reg[pos].notidigi  = 'N'
            LET ga_reg[pos].notisoli  = 'N'
            LET ga_reg[pos].notiaut   = 'N'
            LET ga_reg[pos].notiautgf = 'N'
            LET ga_reg[pos].noticompe = 'N'
            LET ga_reg[pos].noticoord = 'N'
            LET ga_reg[pos].notianal  = 'N'
            LET ga_reg[pos].noticli   = 'N'
            DISPLAY BY NAME ga_reg[pos].notidigi
         END IF 

   END INPUT 
   
END FUNCTION 

FUNCTION combo_init()
  
   CALL combo_din2("etaid","select etaId, trim(etaNombre) from reqmeta ORDER BY 2")
   CALL combo_din2("accid","select accId, trim(accNombre) from reqmacc ORDER BY 2")
   CALL combo_din2("etaidnew","select etaId, trim(etaNombre) from reqmeta ORDER BY 2")

END FUNCTION