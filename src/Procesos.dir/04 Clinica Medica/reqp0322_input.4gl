
GLOBALS "reqp0322_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE touchDet SMALLINT 
DEFINE cita     DATETIME YEAR TO SECOND
DEFINE requi    LIKE reqmreq.reqid
DEFINE citmed   RECORD LIKE reqmccm.*
DEFINE subarch  SMALLINT 
    
   LET w = ui.Window.getCurrent()
   LET f = w.getForm()

   CALL f.setElementHidden("gridListReg",1) -- 1 Ocultar           
   CALL f.setElementHidden("gridInput",0)   -- 0 Mostrar
   CALL f.setElementHidden("grupo11",1) -- 1 Ocultar  
   CALL f.setElementHidden("grupo8",1) -- Cita
   CALL f.setElementHidden("grupo9",1) -- Result Evaluacion
   CALL f.setElementHidden("grupo11",1)
   CALL f.setElementHidden("lbl_caninfofam",1)
   CALL f.setElementHidden("reqmcan.caninfofam",1)  

   CALL combo_init()
   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   LET subarch = FALSE
   
  SELECT reqid, canid, canestado, canfecing, canproccant, canobserva,
         cannombre1, cannombre2, cannombre3, canapellido1, canapellido2, canapellidoc,
         cantipiden, canidentif, cangenero, cantrato, canfecnac, canestadocivil,
         canemail, cantelefono, candireccion,
         null, NULL
    INTO requi, g_reg.*, cita
    FROM reqmcan
   WHERE canid = l_reg.canid

  INITIALIZE citmed.* TO NULL
  SELECT * INTO citmed.*
    FROM reqmccm
   WHERE reqid = requi
     AND canid = g_reg.canid_1
  IF sqlca.sqlcode <> NOTFOUND THEN
     LET g_reg.canfecprocita = citmed.cmed_citafec
     LET g_reg.hora = citmed.cmed_citahora
  END IF

  IF disp_requi(requi) THEN
     CALL f.setElementHidden("groupRequi",0)
  END IF
     
  CALL f.setElementHidden("grupo7",0)

  LET g_reg.cli_result = NULL 
  LET  g_reg.cli_observa = NULL
  
  DISPLAY BY NAME g_reg.*
  LET u_reg.* = g_reg.*

   CALL ui.ComboBox.setDefaultInitializer("cmb_canestado_1")


   CALL f.setElementHidden("grupo9",0)

   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT BY NAME g_reg.cli_result, g_reg.cli_observa 
      ATTRIBUTES (WITHOUT DEFAULTS)

      BEFORE INPUT
         DISPLAY BY NAME g_reg.canid_1, g_reg.canestado_1
         CALL DIALOG.setActionHidden("close",TRUE)

      ON CHANGE cli_result
         IF g_reg.cli_result = '1' THEN
            CALL f.setElementHidden("grupo11",0)
            IF captura_docto_resultado() THEN
                LET docto.cdocfecing = TODAY
                DISPLAY BY NAME docto.cdocarchdir, docto.cdocfecing
                LET subarch = TRUE
                NEXT FIELD NEXT
            ELSE
                LET subarch = FALSE
                LET docto.cdocfecing = NULL
                LET docto.cdocarchdir = NULL
                DISPLAY BY NAME docto.cdocarchdir, docto.cdocfecing
            END IF
         ELSE
            LET subarch = FALSE
            LET docto.cdocfecing = NULL
            LET docto.cdocarchdir = NULL
            DISPLAY BY NAME docto.cdocarchdir, docto.cdocfecing
            CALL f.setElementHidden("grupo11",1)
         END IF

   END INPUT 
   
   ON ACTION ACCEPT

      IF g_reg.cli_observa IS NULL THEN
         CALL msg("Debe ingresar las obsevaciones de la evaluación.")
         NEXT FIELD cli_observa
      END IF 
        
      IF operacion = 'P' AND g_reg.* = u_reg.* THEN
         IF touchDet <> 1 THEN 
            CALL msg("No se efectuaron cambios")
            EXIT DIALOG 
         END IF 
      END IF
      
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            IF  g_reg.cli_result = '1' THEN
                IF NOT subarch THEN
                    IF NOT captura_docto_resultado() THEN
                        LET resultado = FALSE
                    ELSE
                        LET resultado = TRUE
                    END IF
                ELSE
                    LET resultado = TRUE
                END IF
            ELSE
                LET docto.cdocfecing = NULL
                LET docto.cdocarchdir = NULL
                LET resultado = TRUE
            END IF
            EXIT DIALOG
         WHEN "No"
            LET resultado = FALSE
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 

      EXIT DIALOG 

     ON ACTION CANCEL
        EXIT DIALOG
        
   END DIALOG

   CALL f.setElementHidden("grupo9",1)
       
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      DISPLAY BY NAME g_reg.* 
   END IF 

    CALL f.setElementHidden("groupRequi",1)
    CALL f.setElementHidden("gridListReg",0)            
    CALL f.setElementHidden("gridInput",1)  
    CALL f.setElementHidden("grupo11",1) 
RETURN resultado 
END FUNCTION

FUNCTION captura_docto_resultado()
   DEFINE seguir, resultado SMALLINT
   DEFINE rec RECORD
                path STRING,
                name STRING,
                wildcards STRING,
                caption STRING
           END RECORD

    LET seguir = TRUE
    LET resultado = FALSE
       
    INITIALIZE docto.* TO NULL
    LET rec.path = ""
    LET rec.name = "Archivos"
    LET rec.wildcards = ""
    LET rec.caption = "Open file"
    WHILE seguir
        CALL ui.Interface.frontCall("standard","openFile",[rec.*],[docto.cdocarchdir])
        
        IF docto.cdocarchdir IS NULL THEN
            IF NOT box_confirma('El archivo no fue seleccionado. ¿Desea volver a intentar?') THEN
                LET seguir = FALSE
            END IF
        ELSE 
            LET docto.canid = l_reg.canid
            LET seguir = FALSE
            LET resultado = TRUE
        END IF
    END WHILE
        
RETURN resultado
END FUNCTION



FUNCTION captura_datos_cita(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE touchDet SMALLINT 
DEFINE cita     DATETIME YEAR TO SECOND
DEFINE requi    LIKE reqmreq.reqid
DEFINE citmed   RECORD LIKE reqmccm.*
    
   LET w = ui.Window.getCurrent()
   LET f = w.getForm()

   CALL f.setElementHidden("gridListReg",1) -- 1 Ocultar           
   CALL f.setElementHidden("gridInput",0)   -- 0 Mostrar
   CALL f.setElementHidden("grupo11",1) -- 1 Ocultar  
   CALL f.setElementHidden("grupo8",0) -- Cita
   CALL f.setElementHidden("grupo9",1) -- Result Evaluacion
   CALL f.setElementHidden("lbl_caninfofam",1)
   CALL f.setElementHidden("reqmcan.caninfofam",1)  


   CALL combo_init()
   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   
  SELECT reqid, canid, canestado, canfecing, canproccant, canobserva,
         cannombre1, cannombre2, cannombre3, canapellido1, canapellido2, canapellidoc,
         cantipiden, canidentif, cangenero, cantrato, canfecnac, canestadocivil,
         canemail, cantelefono, candireccion,
         null, null
    INTO requi, g_reg.*, cita
    FROM reqmcan
   WHERE canid = l_reg.canid

  INITIALIZE citmed.* TO NULL
  SELECT * INTO citmed.*
    FROM reqmccm
   WHERE reqid = requi
     AND canid = g_reg.canid_1
  IF sqlca.sqlcode <> NOTFOUND THEN
     LET g_reg.canfecprocita = citmed.cmed_citafec
     LET g_reg.hora = citmed.cmed_citahora
  END IF

  IF disp_requi(requi) THEN
     CALL f.setElementHidden("groupRequi",0)
  END IF
     
  CALL f.setElementHidden("grupo7",0)
  DISPLAY BY NAME g_reg.*
  LET u_reg.* = g_reg.*

   CALL ui.ComboBox.setDefaultInitializer("cmb_canestado_1")

   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT BY NAME g_reg.canfecprocita, g_reg.hora
      ATTRIBUTES (WITHOUT DEFAULTS)

      BEFORE INPUT
         DISPLAY BY NAME g_reg.canid_1, g_reg.canestado_1
         CALL DIALOG.setActionHidden("close",TRUE)

   END INPUT 
   
   ON ACTION ACCEPT
        
      IF operacion = 'C' AND g_reg.* = u_reg.* THEN
         IF touchDet <> 1 THEN 
            CALL msg("No se efectuaron cambios")
            EXIT DIALOG 
         END IF 
      END IF
      
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 

     ON ACTION CANCEL
        EXIT DIALOG
        
   END DIALOG

   CALL f.setElementHidden("grupo9",1)
       
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      DISPLAY BY NAME g_reg.* 
   END IF 

    CALL f.setElementHidden("groupRequi",1)
    CALL f.setElementHidden("gridListReg",0)            
    CALL f.setElementHidden("gridInput",1)   
RETURN resultado 
END FUNCTION

FUNCTION disp_requi(num_req)
DEFINE num_req LIKE reqmreq.reqid
DEFINE req     RECORD LIKE reqmreq.*
DEFINE uor     RECORD LIKE glbuniorga.*
DEFINE afu     RECORD LIKE glbmareaf.*
DEFINE pue     RECORD LIKE glbmpue.*
DEFINE eta     RECORD LIKE reqmeta.*

     IF num_req > 0 THEN
         SELECT *
           INTO req.*
          FROM reqmreq
          WHERE reqid = num_req

        IF req.reqestado IS NOT NULL THEN

            LET reqetapa = req.reqetapa
            SELECT *
              INTO eta.*
              FROM reqmeta
             WHERE etaid = reqetapa

            IF eta.etaagregainfo = 2 THEN
                IF usuario = req.reqanalista THEN
                    LET usuanalis = TRUE
                ELSE
                    LET usuanalis = FALSE
                END IF
            END IF

            SELECT *
              INTO uor.*
              FROM glbuniorga
             WHERE uorid = req.pueuniorg

            SELECT * 
              INTO afu.*
              FROM glbmareaf
             WHERE afuid = uor.uorareaf

            SELECT *
              INTO pue.*
              FROM glbmpue
             WHERE pueid = req.pueposicion

            DISPLAY BY NAME req.reqid, req.reqfecrec, uor.uornombre,
                            afu.afunombre, pue.puenombre, req.reqestado,
                            req.jefenom
            RETURN TRUE
        ELSE
            LET uor.uornombre = '*** REQUISICION NO ENCONTRADA***'
             DISPLAY BY NAME req.reqid, uor.uornombre
             RETURN FALSE
        END IF
    ELSE
        LET requi = NULL
        LET w = ui.Window.getCurrent()
        LET f = w.getForm()

        CALL f.setElementHidden("groupRequi",1) -- 1 Ocultar  
        RETURN TRUE
    END IF
      
END FUNCTION