IMPORT util

GLOBALS "reqp0322_glob.4gl"
 
FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE 
   rDet tRL,
   rReg  RECORD LIKE reqmcan.*,
   climed RECORD LIKE reqmccm.*,
   cita  CHAR(25),
   x,i,vcon INTEGER,
   consulta STRING  

   IF flagConsulta THEN 
      CALL encabezado("Consulta")

      CONSTRUCT condicion 
         ON canid, canestado
         FROM canid, canestado

         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
             
      END CONSTRUCT 

   ELSE
      LET condicion = " 1=1 "
   END IF
   --Armar la consulta
   IF requi > 0 THEN
      LET consulta = 
          "SELECT  * ",
          " FROM reqmcan ",
          " WHERE reqid = ", requi,
          " AND canid > 0 AND ", condicion,
          " ORDER BY 1"
   ELSE
      LET consulta = 
          "SELECT * ",
          " FROM reqmcan ",
          " WHERE canid > 0 AND ", condicion,
          " AND canestado = 'E' ",  --- Estado: En clinica medica
          " ORDER BY canfecprocita, canfecprochora"   
   END IF
   
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rReg.*
      LET rDet.canid = rReg.canid
      LET rDet.nombre = rReg.cannombre1 CLIPPED, ' ', rReg.cannombre2 CLIPPED, ' ',
                        rReg.canapellido1 CLIPPED, ' ', rReg.canapellido2 CLIPPED
      INITIALIZE climed.* TO NULL
      LET rDet.cita = NULL
      SELECT FIRST 1 * INTO climed.*
        FROM reqmccm
       WHERE @reqid = rReg.reqid
         AND @canid = rReg.canid
      IF climed.cmedid > 0 THEN
          LET cita = climed.cmed_citahora
          LET rDet.cita   = climed.cmed_citafec USING "DD/MM/YYYY", cita[11,16]
      END IF
      LET rDet.canestado = rReg.canestado
      LET x = x + 1 
      LET reg_det [x].* = rDet.*  
   END FOREACH

   
   RETURN x --Cantidad de registros
END FUNCTION
