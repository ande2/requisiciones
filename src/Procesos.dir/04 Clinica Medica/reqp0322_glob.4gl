DATABASE rh

GLOBALS
TYPE tDG RECORD
--Grupo1
    canid_1 LIKE reqmcan.canid,
    canestado_1 LIKE reqmcan.canestado,
    canfecing LIKE reqmcan.canfecing,
    canproccant_1 LIKE reqmcan.canproccant,
    canobserva LIKE reqmcan.canobserva,
--Grupo2
    cannombre1 LIKE reqmcan.cannombre1,
    cannombre2 LIKE reqmcan.cannombre2,
    cannombre3 LIKE reqmcan.cannombre3,
    canapellido1 LIKE reqmcan.canapellido1,
    canapellido2 LIKE reqmcan.canapellido2,
    canapellidoc LIKE reqmcan.canapellidoc,
--Grupo3
    cantipiden LIKE reqmcan.cantipiden,
    canidentif LIKE reqmcan.canidentif,
    cangenero LIKE reqmcan.cangenero,
    cantrato LIKE reqmcan.cantrato,
    canfecnac LIKE reqmcan.canfecnac,
    canestadocivil LIKE reqmcan.canestadocivil,
--Grupo4
    canemail LIKE reqmcan.canemail,
    cantelefono LIKE reqmcan.cantelefono,
    candireccion LIKE reqmcan.candireccion,
---grupo8
    canfecprocita LIKE reqmcan.canfecprocita,
    hora DATETIME HOUR TO MINUTE,
-- SIGUIMIENTO
    cli_result    LIKE reqmccm.cmed_result,
    cli_observa   VARCHAR(200)
END RECORD

TYPE tRL RECORD
    canid LIKE reqmcan.canid,
    nombre VARCHAR(100),
    canestado LIKE reqmcan.canestado,
    cita   VARCHAR(20)
END RECORD

TYPE tDG_d RECORD
    cdocid      LIKE reqmcandoc.cdocid,
    cdocfecing  LIKE reqmcandoc.cdocfecing,
    cantdid     LIKE reqmcandoc.cantdid,
    cdocobserv  LIKE reqmcandoc.cdocobserv,
    cdocarchdir LIKE reqmcandoc.cdocarchdir
END RECORD

    DEFINE dbname STRING,
    w           ui.Window,
    f           ui.Form
    
    CONSTANT    prog_name = "reqp0322"

    ----  CANDIATOS
    DEFINE reg_det DYNAMIC ARRAY OF tRL
    DEFINE l_reg   tRL
    DEFINE u_reg, g_reg             tDG

    ---- DOCTOS
    DEFINE docto     RECORD LIKE reqmcandoc.*
    --- DOCUMENTOS
    DEFINE u_reg_d, g_reg_d             tDG_d
    
    --- OTROS
    DEFINE condicion STRING
    DEFINE usuario   LIKE commempl.id_commempl
    DEFINE requi     LIKE reqmreq.reqid
    DEFINE reqetapa  LIKE reqmreq.reqetapa
    DEFINE usuanalis SMALLINT

END GLOBALS