IMPORT security 
IMPORT util
IMPORT os

GLOBALS "reqp0322_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE reqmcan ",
      "SET ",
      ---grupo1
      " canfecing = ?, ",
      " canobserva = ?,",
      ---grupo2
      " cannombre1   = ?, ",
      " cannombre2   = ?, ",
      " cannombre3   = ?, ",
      " canapellido1 = ?, ",
      " canapellido2 = ?, ",
      " canapellidoc = ?, ",
      ---grupo3
      " cantipiden = ?, ",
      " canidentif = ?, ",
      " cangenero = ?, ",
      " cantrato = ?, ",
      " canfecnac = ?, ",
      " canestadocivil = ?, ",
      ---grupo4
      " canemail = ?, ",
      " cantelefono = ?, ",
      " candireccion = ?, ",
      ---grupo5
      " cannivelacademico = ?, ",
      " cancarrera = ?, ",
      " relgid = ?, ",
      " cantrasporte = ?, ",
      " canfamilialab = ?, ",
      " caninfofam = ?, ",
      " canfamiliapub = ?, ",
      " canexperilab = ?, ",
      " cansalud = ?, ",
      ---grupo6
      " canturnorota = ?, ",
      " canfindesemana = ?, ",
      " canprocesant = ?, ",
      " trcid = ?, ",
      " canfecprocita = ?, ",
      " canfecprochora = ?, ",
      --
      " canusuoper = ?, ",
      " canfecoper = ? ",
      ---
      " WHERE canid = ? and reqid = ?"

   PREPARE st_modificar FROM strSql

   
END FUNCTION 

FUNCTION modifica(opcion)
DEFINE opcion   CHAR(1)

   CALL encabezado("Modificar")
   --CALL info_usuario()

   IF opcion = 'P' THEN
       IF captura_datos(opcion) THEN
          IF opcion = 'M' THEN
             RETURN actualizar()
          END IF
          IF opcion = 'P' THEN
             RETURN actualizar_p()
          END IF
       ELSE 
          RETURN FALSE 
       END IF 
   END IF

   IF opcion = 'C' THEN
       IF captura_datos_cita(opcion) THEN
          IF opcion = 'M' THEN
             RETURN actualizar()
          END IF
          IF opcion = 'C' THEN
             RETURN actualizar_c()
          END IF
       ELSE 
          RETURN FALSE 
       END IF 
   END IF

   
END FUNCTION 

FUNCTION actualizar()
   DEFINE cita LIKE reqmcan.canfecprochora
   DEFINE ahora LIKE reqmcan.canfecoper

   IF g_reg.canfecprocita >= TODAY THEN
      LET cita = def_hora(g_reg.canfecprocita, g_reg.hora)
   ELSE
      DISPLAY g_reg.canfecprocita
   END IF

   LET ahora = CURRENT YEAR TO MINUTE
   TRY
      EXECUTE st_modificar USING 
            g_reg.canfecing, g_reg.canobserva,
            g_reg.cannombre1, g_reg.cannombre2, g_reg.cannombre3, g_reg.canapellido1, g_reg.canapellido2, g_reg.canapellidoc,
            g_reg.cantipiden, g_reg.canidentif, g_reg.cangenero, g_reg.cantrato, g_reg.canfecnac, g_reg.canestadocivil,
            g_reg.canfecprocita, cita,
            usuario, ahora,
            -- WHERE
            l_reg.canid, requi
   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

FUNCTION actualizar_p()
   DEFINE cita LIKE reqmcan.canfecprochora
   DEFINE ahora LIKE reqmcan.canfecoper
   DEFINE segui RECORD LIKE reqmcsg.*
   DEFINE climed RECORD LIKE reqmccm.*
   DEFINE canproccant_v LIKE reqmcan.canproccant
   DEFINE entrevista  SMALLINT

   IF g_reg.canfecprocita >= TODAY THEN
      LET cita = def_hora(g_reg.canfecprocita, g_reg.hora)
   ELSE
      DISPLAY g_reg.canfecprocita
   END IF

   LET ahora = CURRENT YEAR TO MINUTE
   TRY

      SELECT reqid INTO segui.reqid
        FROM reqmcan
      WHERE canid = l_reg.canid 
      
      LET segui.canid = l_reg.canid
      LET segui.cseg_analista = usuario
      LET segui.cseg_fecha = ahora
      LET segui.cseg_canestado_ini = g_reg.canestado_1

      LET climed.canid = segui.canid
      LET climed.reqid = segui.reqid
      LET climed.cmed_fecha = TODAY
      LET climed.cmed_observa = g_reg.cli_observa
      LET climed.cmed_result = g_reg.cli_result
      LET climed.cmed_usuario = usuario
      LET climed.cseg_fecha = ahora
      
      CASE climed.cmed_result
       WHEN '1' LET segui.cseg_canestado_fin = "F"  --- APPROBADO EN CLINICA
       WHEN '0' LET segui.cseg_canestado_fin = "Y"  --- RECHAZADO EN CLINICA     
      END CASE

      BEGIN WORK
      
      INSERT INTO reqmcsg (reqid,canid,cseg_analista,cseg_fecha,
                           cseg_canestado_ini,cseg_canestado_fin)
         VALUES (segui.reqid, segui.canid, segui.cseg_analista, segui.cseg_fecha,
                 segui.cseg_canestado_ini, segui.cseg_canestado_fin)

      INSERT INTO reqmccm ( canid, reqid, cmed_fecha, cmed_observa, 
                            cmed_usuario, cseg_fecha, cmed_result)
         VALUES ( climed.canid, climed.reqid, climed.cmed_fecha, climed.cmed_observa, 
                  climed.cmed_usuario, climed.cseg_fecha, climed.cmed_result)
      
      UPDATE reqmcan 
         SET canestado = segui.cseg_canestado_fin
         WHERE reqid = segui.reqid 
           AND canid = segui.canid

      IF climed.cmed_result = '1' AND docto.cdocarchdir IS NOT NULL THEN
         CALL grabar_docto()
      END IF

      COMMIT WORK
   CATCH 
      ROLLBACK WORK
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 

END FUNCTION

FUNCTION grabar_docto()

   DEFINE accion CHAR(1)
   DEFINE filename BIGINT 
   DEFINE mensaje STRING 
   DEFINE destino STRING
   DEFINE cdocarchiv LIKE reqmcandoc.cdocarchiv
   DEFINE maximo  SMALLINT
   DEFINE tipDoc  LIKE reqmcantd.cantdid

   LET g_reg_d.cdocfecing = TODAY
   ---- CARGA EL ARCHIVO


    SELECT cantdid INTO tipDoc
      FROM reqmcantd
     WHERE cantdingcli = 'S'

    LET docto.cdocobserv = g_reg.cli_observa
     
    TRY
       LET filename = util.Integer.abs(security.RandomGenerator.CreateRandomNumber())

       LET destino = destino_archivo(filename)
       CALL fgl_getfile(docto.cdocarchdir, destino)

       LOCATE cdocarchiv IN FILE destino

        IF g_reg.canid_1 IS NOT NULL THEN
              LET docto.cdocarchdir = nospace(nombre_archivo(docto.cdocarchdir) CLIPPED)
              INSERT 
                INTO reqmcandoc (canid, cantdid, cdocfecing,   cdocobserv, cdocarchdir,   cdocarchiv)
              VALUES (g_reg.canid_1, tipDoc , docto.cdocfecing, docto.cdocobserv, docto.cdocarchdir,
                          cdocarchiv)
        END IF

    CATCH
       LET mensaje = sqlca.sqlcode, " ", sqlca.sqlerrm  # Catch runtime execution errors from the SQLCA diagnostic record
       ERROR mensaje
       CALL msgError(mensaje,"Grabar Registro")
       RETURN FALSE 
    END TRY

END FUNCTION 

FUNCTION destino_archivo(archivo)
DEFINE archivo, ruta STRING

    LET ruta = '.',os.Path.separator(),'files',os.Path.separator(),archivo

RETURN ruta
END FUNCTION

FUNCTION nombre_archivo(archivo)
DEFINE archivo  STRING
DEFINE result STRING

    IF isGDC() THEN
        LET result = os.Path.basename(archivo)
    ELSE
        LET result = archivo 
    END IF
    
RETURN result
END FUNCTION

FUNCTION actualizar_c()
   DEFINE cita LIKE reqmcan.canfecprochora
   DEFINE ahora LIKE reqmcan.canfecoper
   DEFINE segui RECORD LIKE reqmcsg.*
   DEFINE climed RECORD LIKE reqmccm.*
   IF g_reg.canfecprocita >= TODAY THEN
      LET cita = def_hora(g_reg.canfecprocita, g_reg.hora)
   ELSE
      DISPLAY g_reg.canfecprocita
   END IF

   LET ahora = CURRENT YEAR TO MINUTE
   TRY

      SELECT reqid INTO segui.reqid
        FROM reqmcan
      WHERE canid = l_reg.canid 

      LET climed.canid = l_reg.canid
      LET climed.reqid = segui.reqid
      LET climed.cmed_fecha = TODAY
      LET climed.cmed_usuario = usuario
      LET climed.cseg_fecha = ahora
      LET climed.cmed_citafec = g_reg.canfecprocita
      LET climed.cmed_citahora = cita
      LET climed.cmed_result = 'E'
      

      INSERT INTO reqmccm ( canid, reqid, cmed_fecha, cmed_observa, 
                            cmed_usuario, cseg_fecha, cmed_result,
                            cmed_citafec, cmed_citahora)
         VALUES ( climed.canid, climed.reqid, climed.cmed_fecha, climed.cmed_observa, 
                  climed.cmed_usuario, climed.cseg_fecha, climed.cmed_result,
                  climed.cmed_citafec, climed.cmed_citahora)

   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION

