################################################################################
# Funcion     : %M%
# id_commdep  : Catalogo de Valvulas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        id_commdep de la modificacion
#
################################################################################
DATABASE rh

GLOBALS 
TYPE 
   tDet RECORD 
      reqId          LIKE reqmReq.reqId,
      reqFecRec      LIKE reqmReq.reqFecRec,
      reqEmpCon      LIKE reqmReq.reqEmpCon,
      pueLoc         LIKE reqmReq.pueLoc,
      reqMotivo      LIKE reqmReq.reqMotivo,
      reqTmpMes      LIKE reqmReq.reqTmpMes,
      reqVacAnt      LIKE reqmReq.reqVacAnt,
      reqTipo        LIKE reqmReq.reqTipo,
      
      pueUniOrg      LIKE reqmReq.pueUniOrg,
      puePosicion    LIKE reqmReq.puePosicion,
      pueCco         LIKE reqmReq.pueCco,
      puenecveh      LIKE reqmReq.puenecveh, 
      puehorario     LIKE reqmReq.puehorario,
      reqautplatmp   LIKE reqmReq.reqautplatmp,
      
      jefeuniorg     LIKE reqmReq.jefeuniorg,
      jefepue        LIKE reqmReq.jefepue,
      jefenom        LIKE reqmReq.jefenom,
      
      nomSolicita    LIKE reqmReq.nomSolicita,
      nomAutoriza    LIKE reqmReq.nomAutoriza,
      reqEstado      LIKE reqmReq.reqEstado,
      fecHorIngreso  LIKE reqmReq.fecHorIngreso    
      
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet

{TYPE 
   tDetArr RECORD
      idBoletaIng   LIKE pemDBoleta.idBoletaIng,
      idLinea       LIKE pemDBoleta.idLinea,
      cantCajas     LIKE pemDBoleta.cantCajas,
      pesoBruto     LIKE pemDBoleta.pesoBruto,
      pesoTara      LIKE pemDBoleta.pesoTara,
      pesoNeto      LIKE pemDBoleta.pesoNeto
   END RECORD }

--DEFINE reg_detArr, reg_detArr2 DYNAMIC ARRAY OF tDetArr

--DEFINE g_regArr, u_regArr tDetArr
   
DEFINE vDbname     STRING,   
       condicion   STRING --Condicion de la clausula Where 
CONSTANT    prog_name = "reqp0307"

{DEFINE d_cajaEmp DYNAMIC ARRAY OF RECORD
  idCatEmpaque  LIKE    pemDCajEmp.idcatempaque,
  prestamo      LIKE    pemDCajEmp.prestamo,
  devolucion    LIKE    pemDCajEmp.devolucion
  
END RECORD }
END GLOBALS