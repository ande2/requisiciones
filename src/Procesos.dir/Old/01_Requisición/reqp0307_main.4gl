################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "reqp0307_glob.4gl"
MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING

    DEFINE f ui.Form
		
	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	LET n_param = num_args()

	{IF n_param = 0 THEN
		RETURN
	ELSE
        --LET vDbname = arg_val(2)
        --DISPLAY "vDbname ", vDbname
		--CONNECT TO vDbname
        CONNECT TO "db0001"
	END IF}

	LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
    
	CALL STARTLOG(prog_name2)
	CALL main_init()
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form
	  
	INITIALIZE u_reg.* TO NULL
	LET g_reg.* = u_reg.*

   --CALL trae_usuario() returning g_usuario.*
   --CALL trae_empresa(dbname) returning g_empresa.*
   --CALL trae_programa(prog_name) returning g_programa.*
   --IF g_programa.prog_id = 0 OR 
      --g_programa.prog_id IS NULL THEN
      --CALL box_valdato("Programa no existente, contacte a sistemas")
      --RETURN 
   --END IF 
    CALL ui.Interface.loadActionDefaults("actiondefaults")
    --CALL ui.Interface.loadStyles("styles_sc")
   --CALL ui.Interface.loadToolbar("toolbar")

	LET nom_forma = prog_name CLIPPED, "_form"
    --LET nom_forma = "catm0202_form"
    CLOSE WINDOW SCREEN 
	OPEN WINDOW w1 WITH FORM nom_forma
    CALL fgl_settitle("ANDE - Ingreso de Requisiciones")
    --CALL setEnvForm()
	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()
    LET w = ui.Window.getCurrent()
    --CALL f.setFieldHidden("festado",1)
    CALL f.setElementHidden("Label1",1)
	--CALL fgl_settitle(g_programa.prog_des)
    
	CALL insert_init()
   CALL update_init()
   CALL delete_init()
   CALL combo_init()
	CALL main_menu()
END FUNCTION                                                                    

FUNCTION main_menu()                               
DEFINE  
    cuantos, 
    id, ids	    SMALLINT,
    vopciones   CHAR(255), 
    cnt 		SMALLINT,  
    tol ARRAY[15] OF RECORD
      acc       CHAR(15),
      des_cod   SMALLINT
    END RECORD,
    aui, tb, tbi, tbs om.DomNode

    --DEFINE w ui.Window
    --DEFINE f ui.Form
--
    --LET w = ui.Window.getCurrent()
    --LET f = w.getForm()
    
    LET cnt 		= 1
    DISPLAY "INGRESO DE REQUISICIONES" TO gtit_enc

    MENU --"Pesaje"
       BEFORE MENU
         HIDE OPTION "modificar"
         
       ON ACTION agregar
         IF ingreso() THEN 
            LET cuantos = consulta(FALSE)
            CALL fgl_set_arr_curr( arr_count() + 1 )
            --Refrescar Pantalla
            DISPLAY ARRAY reg_det TO sDet.*
               BEFORE DISPLAY  EXIT DISPLAY 
            END DISPLAY 
         END IF
         CALL encabezado("")
         --CALL info_usuario()

        {ON ACTION actualizapesos
           CALL actualizapeso()}
           
        ON ACTION buscar
         CLEAR FORM 
         LET cuantos = consulta(true)
         IF cuantos > 0 THEN 
            LET g_reg.* = reg_det[1].*
            CALL dispReg()
            SHOW OPTION "modificar"
         ELSE
            HIDE OPTION "modificar"    
         END IF 
         CALL encabezado("") 

        ON ACTION modificar
         LET id = 1 --arr_curr()
         LET ids = scr_line()
         IF id > 0 THEN 
            IF modifica() THEN
               LET reg_det[id].* = g_reg.*
               DISPLAY reg_det[id].* TO sDet[ids].*
            END IF   
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
      ON ACTION anular
         --LET id = arr_curr()
         --LET ids = scr_line()
         IF g_reg.reqId IS NOT NULL THEN 
         --IF id > 0 THEN 
            IF anular() THEN
               --Si es anulacion
               --LET reg_det[id].* = g_reg.*
               --DISPLAY reg_det[id].* TO sDet[ids].*
               --Si es eliminacion
               --CALL DIALOG.deleteRow("sdet", id)
               IF id = arr_count() THEN 
                  LET id = id - 1
               END IF 
               IF id > 0 THEN 
                  LET g_reg.* = reg_det[id].*
               ELSE 
                  INITIALIZE g_reg.* TO NULL
               END IF 
               CALL dispReg()
            END IF  
         ELSE 
             CALL msg("Debe primero Buscar")   
         END IF 
         
      --ON KEY (CONTROL-W) 
      ON ACTION reporte
         CALL mainReport1(g_reg.reqId)
      ON ACTION salir
         EXIT MENU  
    END MENU 
    
	DISPLAY ARRAY reg_det TO sDet.*
      ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
   
      BEFORE DISPLAY
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            CALL dialog.setCurrentRow("sdet",1)
            LET g_reg.* = reg_det[1].*
            --DISPLAY BY NAME g_reg.*
            CALL dispReg()
            
            
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
         
      BEFORE ROW 
         LET id = arr_curr()
         IF id > 0 THEN 
            LET g_reg.* = reg_det[id].*
            CALL dispReg()
         END IF 
         
      ON ACTION agregar
         IF ingreso() THEN 
            LET cuantos = consulta(FALSE)
            CALL fgl_set_arr_curr( arr_count() + 1 )
            --Refrescar Pantalla
            DISPLAY ARRAY reg_det TO sDet.*
               BEFORE DISPLAY  EXIT DISPLAY 
            END DISPLAY 
         END IF
         CALL encabezado("")
         --CALL info_usuario()

      --ON ACTION esconder
        -- CALL f.setElementHidden("festado",1)
         
      ON ACTION buscar
         
         LET cuantos = consulta(true)
         IF cuantos > 0 THEN 
            LET g_reg.* = reg_det[1].*
            CALL dispReg()
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
         
      --ON ACTION primero
         --CALL fgl_set_arr_curr( 1 )
      --ON ACTION ultimo
         --CALL fgl_set_arr_curr( arr_count() )
      --ON ACTION anterior 
         --CALL fgl_set_arr_curr( arr_curr() - 1 )
      --ON ACTION siguiente 
         --CALL fgl_set_arr_curr( arr_curr() + 1 )
      
      ON ACTION modificar
         LET id = arr_curr()
         LET ids = scr_line()
         IF id > 0 THEN 
            IF modifica() THEN
               LET reg_det[id].* = g_reg.*
               DISPLAY reg_det[id].* TO sDet[ids].*
            END IF   
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
      ON ACTION anular
         LET id = arr_curr()
         LET ids = scr_line()         
         IF id > 0 THEN 
            IF anular() THEN
               --Si es anulacion
               --LET reg_det[id].* = g_reg.*
               --DISPLAY reg_det[id].* TO sDet[ids].*
               --Si es eliminacion
               CALL DIALOG.deleteRow("sdet", id)
               IF id = arr_count() THEN 
                  LET id = id - 1
               END IF 
               IF id > 0 THEN 
                  LET g_reg.* = reg_det[id].*
               ELSE 
                  INITIALIZE g_reg.* TO NULL
               END IF 
               CALL dispReg()
            END IF   
         END IF 
         
      --ON KEY (CONTROL-W) 
      ON ACTION reporte
         CALL mainReport1(g_reg.reqId)
      ON ACTION salir
         EXIT DISPLAY 
   END DISPLAY 
END FUNCTION

FUNCTION combo_init()
DEFINE sql_stmt STRING 
   {CALL combo_din2("fidordpro","SELECT idordpro, trim(numordpro)||'-'||trim(nomordpro) FROM pemmordpro WHERE fecfinal + 30 > TODAY AND estado = 1")

   --Para Pilotos
   LET sql_stmt = "SELECT id_commempl, trim(nombre) || '-' || trim(apellido) ",
                  " FROM comMEmpl ",
                  " WHERE id_comMPue = ", getValParam("RECEPCION DE FRUTA - ID PUESTO PILOTO")
   CALL combo_din2("femplentrego",sql_stmt)

   LET sql_stmt = ' SELECT a.idItem, trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg ', 
                  ' FROM glbMItems a, glbmfam b, glbmtip c ',
                  ' WHERE a.idfamilia = b.idfamilia ', 
                  ' AND a.idtipo = c.idtipo '
   CALL combo_din2("fiditem", "SELECT idItem, desItemLg FROM glbMItems")
   
   CALL combo_din2("femplrecibio", "SELECT id_commempl, trim(nombre)||'-'||trim(apellido) FROM commempl")
   CALL combo_din2("femplrecibio1", "SELECT id_commempl, trim(nombre)||'-'||trim(apellido) FROM commempl")

   --Para tipo caja
   LET sql_stmt = "SELECT idCatEmpaque, nomCatEmpaque FROM pemMCatEmp WHERE IdTipEmpaque = ", 
                   getValParam("RECEPCION DE FRUTA - ID CATEGORIA CAJAS")
   CALL combo_din2("ftipocaja",sql_stmt)
   CALL combo_din2("ftipocaja1",sql_stmt)
   
   CALL combo_din2("fproductor","SELECT id_commemp, trim(nombre) FROM commemp WHERE espropia = 'f'")
   --CALL combo_din2("estado","SELECT idEstructura, nomEstructura FROM mestructura ")
   --CALL combo_din2("cmbvalvula", "SELECT idValvula, nomValvula FROM pemMValvula")
   CALL combo_din2("cmbitem", "SELECT idItem, desItemLg FROM glbMItems")
   --CALL combo_din2("cmbjefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --CALL combo_din2("cmbpuesto","SELECT * FROM commpue WHERE id_commpue > 0")
   --CALL combo_din2("cmbtipempaque","SELECT idtipempaque, nomtipempaque FROM pemmtipemp ")
   CALL combo_din2("cmbidordpro","SELECT idordpro, trim(numordpro)||'-'||trim(nomordpro) FROM pemmordpro WHERE fecfinal + 30 > TODAY")
   CALL combo_din2("cmbproductor","SELECT id_commemp, trim(nombre) FROM commemp WHERE espropia = 'f'")
   --CALL combo_din2("cmbitem","SELECT iditem, desItemMd FROM commemp WHERE espropia = 'f'")
}   
END FUNCTION 

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 

   DISPLAY BY NAME  gtit_enc
END FUNCTION 

FUNCTION dispReg()
DEFINE sql_stmt STRING 
   CLEAR FORM  
   DISPLAY g_reg.reqId --, g_reg.fecha, g_reg.esDirecto,
               --g_reg.tipoBoleta, 
              { g_reg.estado, g_reg.idOrdPro,
               g_reg.idDOrdPro, g_reg.emplEntrego, 
               g_reg.docPropioSerie, g_reg.docPropio, 
               g_reg.productor, g_reg.idItem, g_reg.nomEntrego, g_reg.docProductor
               }
               --g_reg.tieneTarima, g_reg.tipoCaja, g_reg.pesoTotal
            TO fidBoletaIng {, ffecha, fesdirecto, --ftipoBoleta, 
               festado, 
               fidordpro, fidDOrdPro, femplEntrego, 
               fdocpropioserie, fdocpropio,
               fproductor, fiditem, fnomEntrego, fdocproductor}
               --ftieneTarima, ftipoCaja, fpesoTotal
               
   {IF g_reg.idOrdPro IS NOT NULL THEN 
      LET sql_stmt = 
             "SELECT idDOrdPro, fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg ",
             " FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e ",
             " WHERE a.idfinca = b.idfinca ",
             " AND a.idestructura = c.idEstructura ",
             " AND a.idValvula = d.idValvula ",
             " AND a.idItem = e.idItem ",
             " AND a.idOrdPro = ", g_reg.idOrdPro
             --DISPLAY sql_stmt
       CALL combo_din2("fiddordpro", sql_stmt)
       --Display al tab de productor
       DISPLAY g_reg.tieneTarima, g_reg.tipoCaja, g_reg.pesoTotal,
               g_reg.emplRecibio, g_reg.fecTran
           TO ftieneTarima, ftipoCaja, fpesoTotal,
              femplRecibio, ffecTran
       --Display al tab de finca
       DISPLAY NULL, NULL 
            TO femplrecibio1, ffecTran1
      CALL displayDetArr()
   ELSE 
      --Display al tab de productor
      DISPLAY NULL, NULL
           TO femplRecibio, ffecTran
      --Display al tab de finca
      DISPLAY g_reg.tieneTarima, g_reg.tipoCaja, g_reg.pesoTotal,
              g_reg.emplRecibio, g_reg.fecTran
           TO ftieneTarima1, ftipoCaja1, fpesoTotal1,
              femplRecibio1, ffecTran1}
       {CALL combo_din2("fiditem", "SELECT idItem, desItemLg FROM glbMItems")}
    {  CALL displayDetArr2()
   END IF }
   
    
   {DECLARE cCursor CURSOR FOR 
   SELECT idBoletaIng, idLinea, cantCajas, 
          pesoBruto, peso}
               
END FUNCTION

{FUNCTION displayDetArr()
DEFINE i SMALLINT 
  CALL reg_detArr.clear()
  CALL reg_detArr2.clear()
  DECLARE detArrCur CURSOR FOR
  SELECT idBoletaIng, idLinea, cantCajas,
         pesoBruto, pesoTara, pesoNeto
  FROM pemDBoleta
  WHERE idBoletaIng = g_reg.idBoletaIng
  ORDER BY idLinea
  LET i = 1
  FOREACH detArrCur INTO reg_detArr[i].*
     LET i = i + 1
  END FOREACH 
  CALL reg_detArr.deleteElement(reg_detArr.getLength())
  DISPLAY ARRAY reg_detArr TO sDet2.*
     BEFORE DISPLAY  EXIT DISPLAY 
  END DISPLAY 
END FUNCTION }

{FUNCTION displayDetArr2()
DEFINE i SMALLINT 
  CALL reg_detArr2.clear()
  DECLARE detArrCur2 CURSOR FOR
  SELECT idBoletaIng, idLinea, cantCajas,
         pesoBruto, pesoTara, pesoNeto
  FROM pemDBoleta
  WHERE idBoletaIng = g_reg.idBoletaIng
  ORDER BY idLinea
  LET i = 1
  FOREACH detArrCur2 INTO reg_detArr2[i].*
     LET i = i + 1
  END FOREACH 
  CALL reg_detArr2.deleteElement(reg_detArr2.getLength())
  DISPLAY ARRAY reg_detArr2 TO sDet3.*
     BEFORE DISPLAY  EXIT DISPLAY 
  END DISPLAY 
END FUNCTION }


{FUNCTION actualizapeso()
  DEFINE vidbol LIKE pemmboleta.idboletaing
  DEFINE vpesoa,peso2 LIKE pemmboleta.pesototal
  DEFINE i SMALLINT 

  LET i = 0
  --BEGIN WORK 
  DECLARE cccur CURSOR FOR
    select t1.idboletaing, sum(t2.pesoneto)
      from pemmboleta t1, pemdboleta t2
      where t1.idboletaing = t2.idboletaing
      group by 1
      order by 1

    FOREACH cccur INTO vidbol, vpesoa
       SELECT pesototal INTO peso2 FROM pemmboleta WHERE idboletaing = vidbol
       IF vpesoa <> peso2 THEN 
          UPDATE pemmboleta
          SET pesototal = vpesoa
          WHERE idboletaing = vidbol
          IF sqlca.sqlcode = 0 THEN 
             DISPLAY "actualizando ", vidbol
             LET i = i + 1
          ELSE 
             DISPLAY "Error ", sqlca.sqlcode , "al actualizar ", vidbol    
          END IF 
       ELSE
          DISPLAY "boleta ", vidbol, " OK"   
       END IF    
    END FOREACH 
    DISPLAY "total boletas actualizadas ", i  
    --ROLLBACK WORK 
END FUNCTION  }
{FUNCTION setEnvForm()
DEFINE vUniMed VARCHAR(50) 

SELECT  valchr 
INTO    vUniMed
FROM    glb_paramtrs 
WHERE nompar = 'NOMBRE UNIDAD DE MEDIDA POR DEFECTO EN BASCULA'

DISPLAY vUniMed TO unidadMedida
END FUNCTION} 