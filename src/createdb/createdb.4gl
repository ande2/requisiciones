DATABASE rh

MAIN

WHENEVER ERROR CONTINUE 

DROP TABLE grupo;
create table grupo
  (
    grpid serial not null ,
    grpnombre varchar(255),
    primary key (grpid)  
  );
  IF sqlca.sqlcode = 0 THEN 
      DISPLAY "Tabla grupo creada"
   ELSE
      DISPLAY "Error en grupo: ",sqlca.sqlcode
   END IF 

   DROP TABLE menu;
   create table menu
  (
    menid serial not null ,
    mennombre varchar(255),
    mencmd char(1000),
    mentipo smallint,
    menpadre integer,
    primary key (menid),
    FOREIGN KEY ( menpadre ) REFERENCES menu  
  );

  IF sqlca.sqlcode = 0 THEN 
      DISPLAY "Tabla menu creada"
   ELSE
      DISPLAY "Error en menu: ",sqlca.sqlcode
   END IF 

   DROP TABLE permiso;
  create table permiso
  (
    pergrpid integer not null ,
    permenid integer not null ,
    primary key (pergrpid,permenid),
    FOREIGN KEY ( pergrpid ) REFERENCES grupo,
    FOREIGN KEY ( permenid ) REFERENCES menu
  );

  IF sqlca.sqlcode = 0 THEN 
      DISPLAY "Tabla permiso creada"
   ELSE
      DISPLAY "Error en permiso: ",sqlca.sqlcode
   END IF

   DROP TABLE usuario;
  create table usuario
  (
    usuid serial not null ,
    usulogin varchar(255),
    usupwd varchar(255),
    usunombre varchar(255),
    usugrpid integer,
    primary key (usuid) ,
    FOREIGN KEY ( usugrpid ) REFERENCES grupo
  );

   IF sqlca.sqlcode = 0 THEN 
      DISPLAY "Tabla usuario creada"
   ELSE
      DISPLAY "Error en usuario: ",sqlca.sqlcode
   END IF

   INSERT INTO grupo VALUES(2,"SUPERUSUARIOS");
   INSERT INTO grupo VALUES(3,"ADMINISTRADORES");
   INSERT INTO grupo VALUES(4,"SUPERVISORES");
   INSERT INTO grupo VALUES(6,"USUARIOS");
   INSERT INTO grupo VALUES(7,"CONTA1");

   INSERT INTO MENU VALUES (-1,"RAIZ",          NULL,                     0,-1);
   INSERT INTO MENU VALUES (40,"Administracion",NULL,                     0,-1);
   INSERT INTO MENU VALUES (41,"Seguridad",     NULL,                     0,40);
   INSERT INTO MENU VALUES (42,"Catalogos",     "fglrun menuCatalogo.42r",1,41);

   INSERT INTO permiso VALUES (2,-1);
   INSERT INTO permiso VALUES (2,37);
   INSERT INTO permiso VALUES (2,38);
   INSERT INTO permiso VALUES (2,39);
   INSERT INTO permiso VALUES (2,40);
   INSERT INTO permiso VALUES (2,41);
   INSERT INTO permiso VALUES (2,42);
   INSERT INTO permiso VALUES (2,43);
   INSERT INTO permiso VALUES (2,44);
   INSERT INTO permiso VALUES (2,45);
   INSERT INTO permiso VALUES (2,46);
   INSERT INTO permiso VALUES (2,47);
   INSERT INTO permiso VALUES (2,48);
   INSERT INTO permiso VALUES (2,49);
   INSERT INTO permiso VALUES (2,50);
   INSERT INTO permiso VALUES (2,51);
   INSERT INTO permiso VALUES (2,52);
   INSERT INTO permiso VALUES (2,53);
   INSERT INTO permiso VALUES (2,54);
   INSERT INTO permiso VALUES (2,55);
   INSERT INTO permiso VALUES (2,56);
   INSERT INTO permiso VALUES (2,57);
   
   INSERT INTO usuario VALUES(4,"root",  "root",  "Superusuario",    2);
   INSERT INTO usuario VALUES(2,"admin", "admin", "Administrador",   3);
   INSERT INTO usuario VALUES(5,"carlos","carlos","Carlos Santizo",  4);
   INSERT INTO usuario VALUES(3,"usr1",  "usr1",  "José LOPEZ TELLO",6);
   INSERT INTO usuario VALUES(6,"jlopez","jlopez","Juan Lopez",      7);
   
END MAIN 