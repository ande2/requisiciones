DATABASE rh 

DEFINE grDep RECORD
  id_commdep  LIKE commdep.id_commdep,
  descripcion LIKE commdep.descripcion,
  hijo_de     LIKE commdep.hijo_de
END RECORD
DEFINE grPue RECORD
  id_commpue    LIKE commpue.id_commpue,
  descripcion   LIKE commpue.descripcion,
  id_commdep    LIKE commpue.id_commdep
END RECORD  
DEFINE grEmpl RECORD 
    id_commempl LIKE commempl.id_commempl,
    nombre      LIKE commempl.nombre,
    apellido    LIKE commempl.apellido
END RECORD 

{MAIN

  MENU "Reporte"
    COMMAND "Generar"
      CALL genera()
    COMMAND "Salir"
      EXIT MENU 
  END MENU 

END MAIN} 

FUNCTION printReport()
  DEFINE pos SMALLINT
  DEFINE ant LIKE commdep.hijo_de
  DEFINE pLine VARCHAR(100)
  DEFINE pLineBca VARCHAR(1)
  DEFINE sql_stmt STRING 
  DEFINE posPue SMALLINT 

--cs
   { DEFINE handler om.SaxDocumentHandler
    IF fgl_report_loadCurrentSettings(NULL) THEN
       CALL fgl_report_selectDevice("SVG")
       --CALL fgl_report_setAutoformatType ("COMPATIBILITY")
       CALL fgl_report_setAutoformatType ("FLAT LIST")
                  --CALL fgl_report_configureCompatibilityOutput(NULL,"courier",NULL,NULL, NULL,NULL)
                  --DISPLAY "Pase"
                  CALL fgl_report_configureAutoformatOutput ("courier", 10, NULL, null, NULL, null) --"TRUE",
                      --"ORGANIZACIÓN Y ASIGNACIONES", "*", "/tmp")
                  --CALL fgl_report_configureAutoformatOutput(NULL,15,NULL,NULL,NULL,NULL)
       CALL fgl_report_selectPreview(TRUE)
       LET handler  = fgl_report_commitCurrentSettings()
    END IF}

--cs
  
  START REPORT rRep
  --Deptos
  DECLARE curDep CURSOR FOR 
      SELECT id_commdep, descripcion, hijo_de 
        FROM commdep 
       WHERE estado = 1 
         AND id_commdep > 0 
    ORDER BY hijo_de

    --Puestos
    LET sql_stmt = "SELECT id_commpue, descripcion, id_commdep FROM commpue ",
                   " WHERE id_commdep = ? AND estado = 1 ORDER BY id_commpue "
    PREPARE ex_curPue FROM sql_stmt 
    DECLARE curPue CURSOR FOR ex_curPue 

    --Empleados
    LET sql_stmt = "SELECT id_commempl, nombre, apellido ",
                   " FROM commempl WHERE id_commdep = ? AND id_commpue = ?  ", 
                   " AND estado = 1 ", 
                   " AND id_commempl > 99 ",
                   " ORDER BY id_commpue "
    PREPARE ex_curEmpl FROM sql_stmt 
    DECLARE curEmpl CURSOR FOR ex_curEmpl 
    
    LET pos = 12
    LET ant = 0  
    FOREACH curDep INTO grDep.*
      
      LET pLine = grDep.id_commdep USING "######"
      
      IF grDep.hijo_de <> ant THEN
         LET ant = grDep.hijo_de
         LET pos = pos + 3
         LET pLine[pos,100] = grDep.descripcion
      ELSE 
         LET pLine[pos,100] = grDep.descripcion
      END IF 
      OUTPUT TO REPORT rRep(pLineBca)
      OUTPUT TO REPORT rRep(pLine)
      --Para puestos
      OPEN curPue USING grDep.id_commdep
      WHILE TRUE
        FETCH NEXT curPue INTO grPue.*
        IF sqlca.sqlcode = 100 THEN
           EXIT WHILE 
        ELSE 
           --LET pLine = ""
           LET pLine = grPue.id_commpue USING "######"
           LET posPue = pos + 3
           LET pLine[posPue,100] = "-", grPue.descripcion 
           OUTPUT TO REPORT rRep(pLine)
           --Para empleados
            OPEN curEmpl USING grPue.id_commdep, grPue.id_commpue
            WHILE TRUE
                FETCH NEXT curEmpl INTO grEmpl.*
                IF sqlca.sqlcode = 100 THEN
                    EXIT WHILE 
                ELSE 
                    --LET pLine = ""
                    LET pLine = grEmpl.id_commempl USING "######"
                    LET posPue = pos + 6
                    LET pLine[posPue,100] = ">", grEmpl.nombre, " ",grEmpl.apellido 
                    OUTPUT TO REPORT rRep(pLine)
                END IF 
            END WHILE    
        END IF 
      END WHILE    
    END FOREACH

    FINISH REPORT rRep
    
END FUNCTION 


REPORT rRep(lLine)
 
DEFINE lLine VARCHAR(100)

DEFINE vEmpresa LIKE commemp.nombre
DEFINE vUsuario VARCHAR(50)

FORMAT 

  PAGE HEADER 
    --Para empresa
    LET vEmpresa = "SELECT nombre FROM commemp WHERE id_commemp = 1"
    PREPARE ex_vEmpresa FROM vEmpresa
    EXECUTE ex_vEmpresa INTO vEmpresa
    PRINT COLUMN 001, vEmpresa,
          COLUMN 075, "FECHA   : ", TODAY USING "dd/mm/yyyy"

    PRINT COLUMN 025, "REPORTE DE ORGANIZACIÓN - EMPLEADOS"

    --Para usuario
    LET vUsuario = "SELECT UNIQUE USER FROM systables WHERE tabid = 99"
    PREPARE ex_vUsuario FROM vUsuario 
    EXECUTE ex_vUsuario INTO vUsuario 
    PRINT COLUMN 001, "USUARIO: ", vUsuario,
          COLUMN 075, "HOJA No.: ", PAGENO USING "&&"
    PRINT COLUMN 000, "=============================================================================================="
    SKIP 2 LINES 

    PRINT COLUMN 001, " CODIGO", 
          COLUMN 015, "DEPTO/PUESTO/EMPLEADO"
    PRINT COLUMN 001, "--------",
          COLUMN 015, "----------------------------------------------"

  ON EVERY ROW 
    PRINT COLUMN 001, lLine

END REPORT 