################################################################################
# Funcion     : %M%
# param_id : Funcion para consulta de umdores  
# Funciones   : umd_qry_init()    -> Prepara la seleccion
#               query_umd()       -> Ingreso de criterios de busqueda
#               fetch_umd()       -> Navega sobre seleccion de datos
#               clean_up()        -> Limpia el cursor de trabajo
#               display_umd()     -> Despliega el registro en pantalla
#               inicia_null()     -> inicia param_ides a nulos
#               display_estado()  -> Despliega el estado del umdor
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        param_id de la modificacion
#
################################################################################

GLOBALS "catm0101_glob.4gl"

FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE 
   rDet tDet,
   x INTEGER,
   consulta STRING 

   IF flagConsulta THEN 
      CALL encabezado("Consulta")
      --CALL info_usuario()
      --Creación de la consulta
      CONSTRUCT condicion 
         ON id_commempl, nombre, apellido, email, telefono,  
            usuGrpId, usuLogin, usuPwd,  
            codigo, pueId, 
            jefe, 
            esSolicitante, esAutorizante, esGerAreaF, 
            esNomina, esCooRRHH, esRRHH, esCliMed,
            estado
         FROM id_commempl, nombre, apellido, email, telefono,  
            usuGrpId, usuLogin, usuPwd,
            codigo, pueId, -- id_commdep, id_commpue, 
            jefe, 
            esSolicitante, esAutorizante, esGerAreaF, 
            esNomina, esCooRRHH, esRRHH, esCliMed,
            estado

         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
         
      END CONSTRUCT 
   ELSE
      LET condicion = " 1=1 "
   END IF
   --Armar la consulta
   LET consulta = 
      "SELECT id_commempl, nombre, apellido, ",
            " email, telefono,  ",
            " usuGrpId, usuLogin, usuPwd, ", 
            " codigo, ",
            " afuId, uorId, pueId, ",
            " jefe, ",
            " esSolicitante, esAutorizante, esGerAreaF, ", 
            " esNomina, esCooRRHH, esRRHH, esCliMed, ", 
            " estado  ",
      " FROM commempl ",
      --"WHERE id_commempl > 99 AND ", condicion,
      " WHERE ", condicion,
      " ORDER BY 1"
   
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rDet.id_commempl, rDet.nombre, rDet.apellido,
      rDet.email, rDet.telefono, 
      rDet.usugrpid, rDet.usuLogin, rDet.usuPwd,
      rDet.codigo, 
      rDet.afuId, rDet.uorId, rDet.pueId, 
      rDet.jefe, 
      rDet.esSolicitante, rDet.esAutorizante, rDet.esGerAreaF,
      rDet.esNomina, rDet.esCooRRHH, rDet.esRRHH,
      rDet.esCliMed, rDet.estado
      
      LET x = x + 1 
      LET reg_det [x].* = rDet.*  
   END FOREACH
   RETURN x --Cantidad de registros
END FUNCTION
