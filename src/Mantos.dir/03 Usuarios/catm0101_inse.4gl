################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "catm0101_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO commempl ( ",
        --" id_commempl, nombre, apellido, ",
        " nombre, apellido, ",
        " email, telefono,  ",
        " usuGrpid, usuLogin, usuPwd, ",
        " codigo, ",
        " afuId, uorId, pueId, ",
        " jefe, ",
        " esSolicitante, esAutorizante, ",
        " esGerAreaF, esNomina, esCooRRHH, ",
        " esRRHH, esCliMed, ",
        " estado ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   
   LET g_reg.id_commempl = 0
   LET g_reg.estado = 1
   TRY 
      EXECUTE st_insertar USING 
            --g_reg.id_commempl, g_reg.nombre, g_reg.apellido, 
            g_reg.nombre, g_reg.apellido, 
            g_reg.email, g_reg.telefono, 
            g_reg.usuGrpId, g_reg.usuLogin, g_reg.usuPwd,
            g_reg.codigo, 
            g_reg.afuId, g_reg.uorId, g_reg.pueId, 
            g_reg.jefe,
            g_reg.esSolicitante, g_reg.esAutorizante,
            g_reg.esGerAreaF, g_reg.esNomina, 
            g_reg.esCooRRHH, g_reg.esRRHH, g_reg.esCliMed,
            g_reg.estado

      --CS agregarlo cuando se agregue el ID 
      LET g_reg.id_commempl = SQLCA.sqlerrd[2]
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("jefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   RETURN TRUE 
END FUNCTION 