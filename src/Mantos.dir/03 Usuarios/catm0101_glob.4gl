################################################################################
# Funcion     : %M%
# Descripcion : Catalogo de Empresas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
DATABASE rh

GLOBALS 
TYPE 
   tDet RECORD 
      id_commempl       LIKE commempl.id_commempl,
      nombre            LIKE commempl.nombre,
      apellido          LIKE commempl.apellido,
      email             LIKE commempl.email,
      telefono          LIKE commempl.telefono,
      
      usugrpid          LIKE commempl.usugrpid,
      usuLogin          LIKE commempl.usulogin,
      usuPwd            LIKE commempl.usupwd,
      
      codigo            LIKE commempl.codigo,

      afuId             LIKE commempl.afuid, 
      uorId             LIKE commempl.uorid, 
      pueId             LIKE commempl.pueId,
      
      jefe              LIKE commempl.jefe,

      esSolicitante     LIKE commempl.essolicitante,
      esAutorizante     LIKE commempl.esautorizante,
      esGerAreaF        LIKE commempl.esgerareaf,
      esNomina          LIKE commempl.esnomina,
      esCooRRHH         LIKE commempl.escoorrhh,
      esRRHH            LIKE commempl.esrrhh,
      esCliMed          LIKE commempl.esclimed,
      
      estado            LIKE commempl.estado
      
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   usuario     INTEGER,
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "catm0101"
END GLOBALS