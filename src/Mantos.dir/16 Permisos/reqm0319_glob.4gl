DATABASE rh

GLOBALS
TYPE tDG RECORD
    uapid      LIKE reqmuap.uapid,
    etaid      LIKE reqmuap.etaid,
    uapusuario LIKE reqmuap.uapusuario,
    uaptipo    LIKE reqmuap.uaptipo
END RECORD

    DEFINE dbname STRING
    CONSTANT    prog_name = "reqm0319"

    DEFINE reg_det DYNAMIC ARRAY OF tDG
    DEFINE u_reg, g_reg             tDG
    DEFINE condicion STRING

    DEFINE etapa LIKE reqmeta.etaid

DEFINE
    w           ui.Window,
    f           ui.Form
END GLOBALS