GLOBALS "reqm0317_glob.4gl"


FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM reqmeta ",
      "WHERE etaid = ? "
      
   PREPARE st_delete FROM strSql

   LET strSql =
      "UPDATE reqmeta SET estado = 0 ",
        " WHERE etaid = ? "        

   PREPARE st_anular FROM strSql

   LET strSql =
      "DELETE FROM reqmflu ",
      "WHERE etaid = ? "
      
   PREPARE st_delete_f FROM strSql


END FUNCTION 

FUNCTION anular()
DEFINE vresult SMALLINT
DEFINE oper CHAR(1) 
DEFINE mensaje STRING


   LET oper = 'E'
   LET mensaje = 'Al anular, eliminará el registro'
   
   IF NOT box_confirma(mensaje) THEN
      RETURN FALSE
   END IF
   
   TRY
      BEGIN WORK
      IF oper='A' THEN 
         EXECUTE st_anular USING g_reg.etaid
      ELSE 
         EXECUTE st_delete_f USING g_reg.etaid
         EXECUTE st_delete USING g_reg.etaid
      END IF 
      COMMIT WORK
   CATCH 
      ROLLBACK WORK
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   
   CALL msg("Registro eliminado")
   
   RETURN TRUE 
END FUNCTION 
