GLOBALS "reqm0317_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE reqmeta ",
      "SET ",
      " etanombre = ?, ",
      " etaorden = ?, ",
      " etaestadoreq = ?,",
      " etaabccandid = ?, ",
      " etaagregainfo = ?, ",
      " etatipo = ?, ",
      " etatiempo = ? ",
      " WHERE etaid = ?"

   PREPARE st_modificar FROM strSql

   LET strSql =
      "DELETE FROM reqmflu ",
      "WHERE etaid = ? "
      
   PREPARE st_mdelete_f FROM strSql

   LET strSql =
      "INSERT INTO reqmflu ( ",
        " etaid, accid, flusigetapa, flutiporeq, flurestinusu ",
        "   ) ",
      " VALUES (?,?,?,?,?)"

   PREPARE st_minsertar_f FROM strSql
   
END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION actualizar()
DEFINE i, j SMALLINT 
DEFINE tiempo LIKE reqmeta.etatiempo
DEFINE txtiempo CHAR(10)
DEFINE txthoras CHAR(10)

   LET txtiempo = '0 0'
   IF g_reg.dias > 0 THEN
      LET txtiempo = g_reg.dias USING "##&"
   ELSE
      LET txtiempo = ' 0'
   END IF
   IF g_reg.horas > 0 THEN
      LET txthoras =  g_reg.horas USING "#&&"
      LET txtiempo = txtiempo CLIPPED, txthoras
   ELSE
      LET txtiempo = txtiempo CLIPPED,' 0'
   END IF
   DISPLAY txtiempo
   LET tiempo = txtiempo
   TRY
      BEGIN WORK
      EXECUTE st_mdelete_f USING g_reg.etaid
      CALL grabar_f()      
      EXECUTE st_modificar USING 
      g_reg.etanombre, g_reg.etaorden, g_reg.etaestadoreq,
      g_reg.etaabccandid, g_reg.etaagregainfo, g_reg.etatipo, tiempo,
      g_reg.etaid
      COMMIT WORK
   CATCH 
      ROLLBACK WORK
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 
