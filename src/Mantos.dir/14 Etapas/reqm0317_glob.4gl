DATABASE rh

GLOBALS
TYPE tDG RECORD
    etaid         LIKE reqmeta.etaid,
    etanombre     LIKE reqmeta.etanombre,
    etaorden      LIKE reqmeta.etaorden,
    etaestadoreq  LIKE reqmeta.etaestadoreq,
    etaabccandid  LIKE reqmeta.etaabccandid,
    etaagregainfo LIKE reqmeta.etaagregainfo,
    etatipo       LIKE reqmeta.etatipo,
    dias          SMALLINT,
    horas         SMALLINT
END RECORD

TYPE tDGF RECORD
    accid        LIKE reqmflu.accid,
    flusigetapa  LIKE reqmflu.flusigetapa,
    flutiporeq   LIKE reqmflu.flutiporeq,
    motid        LIKE reqmflu.motid,
    flurestinusu LIKE reqmflu.flurestinusu,
    fluid        LIKE reqmflu.fluid
END RECORD 

    DEFINE dbname STRING
    CONSTANT    prog_name = "reqm0317"

    DEFINE reg_det DYNAMIC ARRAY OF tDG
    DEFINE u_reg, g_reg             tDG
    DEFINE condicion STRING

    DEFINE reg_flu DYNAMIC ARRAY OF tDGF
    DEFINE fu_reg, fg_reg           tDGF
    
END GLOBALS