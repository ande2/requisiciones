################################################################################
# Funcion     : %M%
# Descripcion : Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
DATABASE rh 
 
GLOBALS 
TYPE 
   tDet RECORD 
      afuId       LIKE glbmareaf.afuId,
      afuNombre   LIKE glbmareaf.afuNombre,
      afuGerente  LIKE glbmareaf.afuGerente,
      afuEstado   LIKE glbmareaf.afuEstado
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion      STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "catm0301"
END GLOBALS