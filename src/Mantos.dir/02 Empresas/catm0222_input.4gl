
GLOBALS "catm0222_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form
DEFINE idx SMALLINT 
DEFINE touchDet SMALLINT 

LET w = ui.Window.getCurrent()
LET f = w.getForm()

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I' THEN 
      
      INITIALIZE g_reg.* TO NULL
      DISPLAY BY NAME g_reg.*
      --CALL cleanArrayItems()
      
   END IF
   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT BY NAME  
      g_reg.id_commemp, g_reg.codigo, g_reg.nit, g_reg.nombre, g_reg.iniciales, g_reg.direccion, 
      g_reg.telefono, g_reg.espropia, g_reg.estado, g_reg.dbname
      ATTRIBUTES (WITHOUT DEFAULTS)

      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",true)

      ON CHANGE nit
        SELECT * FROM commemp 
        WHERE nit = g_reg.nit
        IF sqlca.sqlcode = 0 THEN
           CALL msg("NIT ya existe, ingrese de nuevo")
           NEXT FIELD CURRENT
        END IF 
          
      AFTER FIELD nombre
        LET g_reg.nombre = g_reg.nombre CLIPPED  
        SELECT nombre FROM commemp
        WHERE nombre = g_reg.nombre 
        IF sqlca.sqlcode = 0 THEN
           CALL msg("Nombre de empresa ya existe")
           NEXT FIELD CURRENT 
        END IF 
        

      AFTER FIELD dbname 
        IF g_reg.nombre IS NOT NULL THEN
           SELECT id_commemp FROM commemp WHERE commemp.dbname = g_reg.dbname
           IF sqlca.sqlcode = 0 THEN 
              CALL msg("Nombre de base de datos ya existe")
              NEXT FIELD CURRENT
           END IF 
        END IF
 
 
            --IF (operacion = 'I') OR (g_reg.emNomCt <> u_reg.emNomCt) THEN 
            --IF existe_cat_cod(g_reg.idBautizo) THEN 
           
               --LET g_reg.idBautizo = NULL 
            
            --END IF   
         --END IF
       

      {AFTER FIELD librob
      IF g_reg.librob IS NOT NULL THEN 
         IF (operacion = 'I') OR (g_reg.librob <> u_reg.librob) THEN 
            IF existe_cat_nom(g_reg.librob) THEN 
               CALL msg("Este nombre ya esta siendo utilizado")
               LET g_reg.librob = NULL 
               NEXT FIELD CURRENT 
            END IF   
         END IF
      END IF} 
      
   END INPUT 

   {INPUT ARRAY gr_item FROM sDet2.* --ATTRIBUTES (APPEND ROW  = FALSE )
      --BEFORE INPUT 
        --CALL f. ("append", "goodbye") 

      AFTER FIELD idItem
         LET idx = arr_curr()
         IF valida_rep(idx) THEN 
            CALL msg("Item ya esta en la lista, ingrese de nuevo")
            NEXT FIELD idItem
         END IF 
         
         SELECT trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg 
           INTO gr_item[idx].desItem 
           FROM db0001:glbmitems a, db0001:glbmfam b, db0001:glbmtip c
           WHERE a.idfamilia = b.idfamilia
             AND a.idtipo = c.idtipo
             AND idItem = gr_item[idx].idItem
         IF sqlca.sqlcode = 0 THEN 
            DISPLAY gr_item[idx].desItem TO sDet2[scr_line()].desItemLg
         ELSE 
            CALL msg("No existe código de item, ingrese de nuevo, presione <Buscar> para ayuda")
            NEXT FIELD idItem
         END IF 
         
      ON CHANGE idItem
         LET idx = arr_curr()
         IF valida_rep(idx) THEN 
            CALL msg("Item ya esta en la lista, ingrese de nuevo")
            NEXT FIELD idItem
         END IF 
         SELECT trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg 
           INTO gr_item[idx].desItem 
           FROM db0001:glbmitems a, db0001:glbmfam b, db0001:glbmtip c
           WHERE a.idfamilia = b.idfamilia
             AND a.idtipo = c.idtipo
             AND idItem = gr_item[idx].idItem
         IF sqlca.sqlcode = 0 THEN 
            DISPLAY gr_item[idx].desItem TO sDet2[scr_line()].desItemLg
         ELSE 
            CALL msg("No existe c?digo de item, ingrese de nuevo, presione <Buscar> para ayuda")
            NEXT FIELD idItem
         END IF 
         
      ON ACTION buscar --ON KEY (control-e)
         LET idx = arr_curr()
         CASE 
         WHEN INFIELD (idItem)

            --CALL picklist_2('Items', 'Codigo', 'Descripcion',
              --  'idItem', 'desItemLg', 'glbmitems',
              -- " 1=1", 1, 1)
            CALL picklist_2('Items', 'Codigo', 'Descripcion',
                'a.idItem', 'trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg', 'db0001:glbmitems a, db0001:glbmfam b, db0001:glbmtip c',
               " a.idfamilia = b.idfamilia AND a.idtipo = c.idtipo ", 1, 1)   
            RETURNING gr_item[idx].idItem , gr_item[idx].desItem, INT_FLAG
            IF NOT INT_FLAG THEN 
               LET idx = arr_curr()
               IF valida_rep(idx) THEN 
                  CALL msg("Item ya esta en la lista, ingrese de nuevo")
                 NEXT FIELD idItem
               ELSE 
                  NEXT FIELD NEXT
               END IF 
            END IF 
         END CASE
   END INPUT} 
   
   ON ACTION ACCEPT
      
        {IF g_reg.nit IS NULL THEN
           CALL msg("Debe ingresar NIT")
           NEXT FIELD CURRENT
        END IF  }

        IF g_reg.nombre IS NULL THEN
           CALL msg("Debe ingresar nombre")
           NEXT FIELD CURRENT
        END IF  

        {IF g_reg.espropia IS NULL THEN
           CALL msg("Debe especificar si es empresa propia")
           NEXT FIELD esPropia
        END IF }

        --VALIDACIONES DE ITEMSXEMPRESA
        { IF gr_item.getLength() > 0 THEN 
          
         LET idx = arr_curr()
         IF valida_rep(idx) THEN 
            CALL msg("Item ya esta en la lista, ingrese de nuevo")
            NEXT FIELD idItem
         END IF 
         IF gr_item[gr_item.getLength()].idItem IS NOT NULL THEN 
            SELECT desItemLg INTO gr_item[idx].desItem FROM db0001:glbmitems
              WHERE idItem = gr_item[idx].idItem
            IF sqlca.sqlcode = 0 THEN 
               DISPLAY gr_item[idx].desItem TO sDet2[scr_line()].desItemLg
            ELSE 
               CALL msg("No existe c?digo de item, ingrese de nuevo, presione <Buscar> para ayuda")
               NEXT FIELD idItem
           END IF 
         ELSE 
            IF gr_item.getLength() > 0 THEN 
               CALL gr_item.deleteElement(gr_item.getLength())
               LET touchDet = 1
            END IF 
         END IF
         END IF }
      --IF g_reg.cat_id IS NULL THEN
         --CALL msg("Debe ingresar Id.")
         --NEXT FIELD cat_id
      --END IF
      
      --IF g_reg.librob IS NULL THEN
         --CALL msg("Debe ingresar nombre")
         --NEXT FIELD librob
      --END IF
      --IF (operacion = 'I') OR (g_reg.cat_id <> u_reg.cat_id) THEN 
         --IF existe_cat_id(g_reg.cat_id) THEN 
            --CALL msg("Este Id. ya esta siendo utilizado")
            --LET g_reg.cat_id = NULL 
            --NEXT FIELD cat_id 
         --END IF   
      --END IF

      --CS
      --IF (operacion = 'I') OR (g_reg.idBautizo <> u_reg.idBautizo) THEN 
         --IF existe_cat_cod(g_reg.idBautizo) THEN 
            --CALL msg("Este codigo ya esta siendo utilizado")
            --LET g_reg.idBautizo = NULL 
            --NEXT FIELD idBautizo 
         --END IF   
      --END IF
      --CS
      {IF (operacion = 'I') OR (g_reg.librob <> u_reg.librob) THEN 
         IF existe_cat_nom(g_reg.librob) THEN 
            CALL msg("Este nombre ya esta siendo utilizado")
            LET g_reg.librob = NULL 
            NEXT FIELD librob 
         END IF   
      END IF}
      IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         IF touchDet <> 1 THEN 
            CALL msg("No se efectuaron cambios")
            EXIT DIALOG 
         END IF 
      END IF
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 

   
      
     ON ACTION CANCEL
        EXIT dialog
   END DIALOG
   
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      DISPLAY BY NAME g_reg.* 
   END IF 
   RETURN resultado 
END FUNCTION

{FUNCTION valida_rep(i)
DEFINE i,j SMALLINT 


FOR j = 1 TO gr_item.getLength()
  IF i <> j THEN 
     IF gr_item[i].idItem = gr_item[j].idItem THEN
        RETURN TRUE  
     END IF 
  END IF 
    
END FOR 
RETURN FALSE
END FUNCTION 

FUNCTION cleanArrayItems()

  CALL gr_item.clear()
  
END FUNCTION }