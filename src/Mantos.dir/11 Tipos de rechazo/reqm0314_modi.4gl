GLOBALS "reqm0314_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE reqmtrech ",
      "SET ",
      " trechnombre = ?, ",
      " trechobserva = ? ",
      " WHERE trechid = ?"

   PREPARE st_modificar FROM strSql

   
END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION actualizar()
DEFINE i SMALLINT 

   TRY
      EXECUTE st_modificar USING 
      g_reg.trechnombre, g_reg.trechobserva, g_reg.trechid
   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

{FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM commemp ",
      "WHERE id_commemp = ? "
      
   PREPARE st_delete FROM strSql

   LET strSql =
      "UPDATE commemp SET estado = 0 ",
        " WHERE id_commemp = ? "        

   PREPARE st_anular FROM strSql

END FUNCTION 

FUNCTION anular()
DEFINE vresult SMALLINT
DEFINE oper CHAR(1) 

   LET vresult = 0 
   
   SELECT count(*) INTO vresult FROM reqMReq WHERE reqEmpCon = g_reg.id_commemp
   
   IF vresult > 0 THEN
      LET oper = 'A'
   ELSE 
      LET oper = 'E'
   END IF
   
   IF NOT box_confirma("Esta seguro de anular el registro") THEN
      RETURN FALSE
   END IF
   
   TRY
      IF oper='A' THEN 
         EXECUTE st_anular USING g_reg.id_commemp
      ELSE 
         EXECUTE st_delete USING g_reg.id_commemp
      END IF 
   CATCH 
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   
   CALL msg("Registro eliminado")
   
   RETURN TRUE 
END FUNCTION }

