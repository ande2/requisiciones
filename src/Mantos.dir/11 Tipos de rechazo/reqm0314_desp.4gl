
GLOBALS "reqm0314_glob.4gl"
 
FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE 
   rDet tDG,
   x,i,vcon INTEGER,
   consulta STRING  

   IF flagConsulta THEN 
      CALL encabezado("Consulta")

      CONSTRUCT condicion 
         ON trechid, trechnombre, trechobserva
         FROM trechid, trechnombre, trechobserva

         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
             
      END CONSTRUCT 

   ELSE
      LET condicion = " 1=1 "
   END IF
   --Armar la consulta
   LET consulta = 
      "SELECT  trechid, trechnombre, trechobserva ",
      " FROM reqmtrech ",
      " WHERE trechid > 0 AND ", condicion,
      " ORDER BY 1"
   
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rDet.*
      LET x = x + 1 
      LET reg_det [x].* = rDet.*  
   END FOREACH

   
   RETURN x --Cantidad de registros
END FUNCTION
