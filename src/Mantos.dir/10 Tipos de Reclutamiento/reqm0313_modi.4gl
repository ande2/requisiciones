GLOBALS "reqm0313_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE reqmtrc ",
      "SET ",
      " trcnombre = ?, ",
      " trcactivo = ? ",
      " WHERE trcid = ?"

   PREPARE st_modificar FROM strSql

   
END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION actualizar()
DEFINE i SMALLINT 

   TRY
      EXECUTE st_modificar USING 
         g_reg.trcnombre, g_reg.trcactivo, g_reg.trcid
   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 
