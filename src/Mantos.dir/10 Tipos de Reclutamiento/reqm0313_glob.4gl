DATABASE rh

GLOBALS
TYPE tDG RECORD
    trcid     LIKE reqmtrc.trcid,
    trcnombre LIKE reqmtrc.trcnombre,
    trcactivo LIKE reqmtrc.trcactivo
END RECORD

    DEFINE dbname STRING
    CONSTANT    prog_name = "reqm0313"

    DEFINE reg_det DYNAMIC ARRAY OF tDG
    DEFINE u_reg, g_reg             tDG
    DEFINE condicion STRING
    
END GLOBALS