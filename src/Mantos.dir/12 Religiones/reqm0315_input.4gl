
GLOBALS "reqm0315_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form
DEFINE idx SMALLINT 
DEFINE touchDet SMALLINT 

LET w = ui.Window.getCurrent()
LET f = w.getForm()

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I' THEN 
      
      INITIALIZE g_reg.* TO NULL
      DISPLAY BY NAME g_reg.*
      --CALL cleanArrayItems()
      
   END IF
   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT BY NAME  g_reg.relgnombre, g_reg.relgobserba
      ATTRIBUTES (WITHOUT DEFAULTS)

      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",true)
      
   END INPUT 
   
   ON ACTION ACCEPT
      
        IF g_reg.relgnombre IS NULL THEN
           CALL msg("Debe ingresar nombre")
           NEXT FIELD CURRENT
        END IF  

      IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         IF touchDet <> 1 THEN 
            CALL msg("No se efectuaron cambios")
            EXIT DIALOG 
         END IF 
      END IF
      
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 

     ON ACTION CANCEL
        EXIT DIALOG
        
   END DIALOG
   
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      DISPLAY BY NAME g_reg.* 
   END IF 
   
RETURN resultado 
END FUNCTION