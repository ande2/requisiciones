GLOBALS "reqm0315_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO reqrelig ( ",
        " relgnombre, relgobserba ",
        "   ) ",
      " VALUES (?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   DEFINE i SMALLINT 
   
   TRY 
      EXECUTE st_insertar USING 
            g_reg.relgnombre, g_reg.relgobserba
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")

   RETURN TRUE 
END FUNCTION 