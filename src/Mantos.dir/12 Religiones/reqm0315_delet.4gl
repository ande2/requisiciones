GLOBALS "reqm0315_glob.4gl"


FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM reqrelig ",
      "WHERE relgid = ? "
      
   PREPARE st_delete FROM strSql

   LET strSql =
      "UPDATE reqrelig SET estado = 0 ",
        " WHERE relgid = ? "        

   PREPARE st_anular FROM strSql

END FUNCTION 

FUNCTION anular()
DEFINE vresult SMALLINT
DEFINE oper CHAR(1) 
DEFINE mensaje STRING

   --LET vresult = 0 
   --
   --SELECT count(*) INTO vresult FROM reqMReq WHERE reqEmpCon = g_reg.id_commemp
   --
   --IF vresult > 0 THEN
   --   LET oper = 'A'
   --   LET mensaje = 'Esta seguro de anular el registro'
   --ELSE 
      LET oper = 'E'
      LET mensaje = 'Al anular, eliminará el registro'
   --END IF
   
   IF NOT box_confirma(mensaje) THEN
      RETURN FALSE
   END IF
   
   TRY
      IF oper='A' THEN 
         EXECUTE st_anular USING g_reg.relgid
      ELSE 
         EXECUTE st_delete USING g_reg.relgid
      END IF 
   CATCH 
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   
   CALL msg("Registro eliminado")
   
   RETURN TRUE 
END FUNCTION 
