
GLOBALS "catm0303_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form
DEFINE sql_stmt STRING 

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I'THEN 
      
      INITIALIZE g_reg.* TO NULL
      DISPLAY BY NAME g_reg.*
   END IF
   DIALOG ATTRIBUTES(UNBUFFERED)
   INPUT BY NAME  
      g_reg.pueId, g_reg.uorareaf, g_reg.pueUniOrga, g_reg.pueNombre, g_reg.pueEstado
      ATTRIBUTES (WITHOUT DEFAULTS)
      
      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",TRUE)
         IF (operacion = 'I') THEN
            LET g_reg.pueEstado = 1
            DISPLAY BY NAME g_reg.pueEstado
         END IF  

      ON CHANGE uorAreaF
         LET sql_stmt = "select uorId, trim(uorNombre) from glbUniOrga WHERE uorAreaF = ", g_reg.uorareaf, " ORDER BY 2"
         DISPLAY sql_stmt 
         CALL combo_din2("pueUniOrga",sql_stmt)
         
      AFTER FIELD pueNombre
      IF g_reg.pueNombre IS NULL THEN
         ERROR "Nombre no puede ser nulo"
         NEXT FIELD pueNombre
      END IF 
      
      
   END INPUT 

   ON ACTION ACCEPT
   
      IF g_reg.pueNombre IS NULL THEN
         CALL msg("Nombre no puede ser nulo")
         NEXT FIELD pueNombre
      END IF
      
      IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         CALL msg("No se efectuaron cambios")
         EXIT DIALOG 
      END IF
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 
      
   ON ACTION CANCEL
      EXIT dialog
   END DIALOG
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      DISPLAY BY NAME g_reg.* 
   END IF 
   RETURN resultado 
END FUNCTION

