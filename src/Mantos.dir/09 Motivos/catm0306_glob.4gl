################################################################################
# Funcion     : %M%
# Descripcion : Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
DATABASE rh 
 
GLOBALS 
TYPE 
   tDet RECORD 
      motId       LIKE glbMMot.motId,
      motNombre   LIKE glbMMot.motNombre,
      motReqAut   LIKE glbMMot.motReqAut,
      flagTemp    LIKE glbMMot.flagTemp,
      flagVac     LIKE glbMMot.flagVac,
      motEstado   LIKE glbMMot.motEstado
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion      STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "catm0306"
END GLOBALS