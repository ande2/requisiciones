DATABASE rh

DEFINE g_reg RECORD LIKE reqmcan.*

MAIN

   CLOSE WINDOW SCREEN 

   OPEN WINDOW w1 WITH FORM "reqp0412_form"

   CALL ui.Interface.loadActionDefaults("actiondefaults_rh1")
   
   MENU
      ON ACTION agregar
         CALL fagrega()
         
      ON ACTION salir 
         EXIT MENU 
   END MENU 

END MAIN

FUNCTION fagrega()
   INPUT BY NAME g_reg.*
         
END FUNCTION 