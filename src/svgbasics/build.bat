@rem FOURJS_START_COPYRIGHT(P,2017)
@rem Property of Four Js*
@rem (c) Copyright Four Js 2017, 2022. All Rights Reserved.
@rem * Trademark of Four Js Development Tools Europe Ltd
@rem   in the United States and elsewhere
@rem FOURJS_END_COPYRIGHT
fglform -M svgbasics.per
fglcomp -M svgbasics.4gl
